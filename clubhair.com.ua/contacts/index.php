<?
require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/header.php");
$APPLICATION->SetPageProperty("description", "CLUBHAIR: інтернет-магазин професійної косметики і професійних засобів для догляду за волоссям");
$APPLICATION->SetPageProperty("keywords", "CLUBHAIR: інтернет-магазин професійної косметики і професійних засобів для догляду за волоссям");
$APPLICATION->SetPageProperty("title", "Контакти - CLUBHAIR: інтернет-магазин професійної косметики і професійних засобів для догляду за волоссям");
$APPLICATION->SetTitle("Контакти");
?>    <div class="contacts_wrapper">
    <div class="title_box title-contacts">
        <h1 class="title">Контакти</h1>
    </div>
    <div class="contacts_content row">
        <div class="contacts__adress col-lg-3 col-sm-12 col-md-3">
            <?php $APPLICATION->IncludeFile(SITE_DIR . 'contacts/include/contacts.php') ?>
        </div>
        <div class="contacts__form col-lg-9 col-sm-9 col-9 map-relative">
            <? $APPLICATION->IncludeComponent(
                "bitrix:catalog.store",
                "stores",
                array(
                    "CACHE_TIME" => "3600",
                    "CACHE_TYPE" => "A",
                    "MAP_TYPE" => "0",
                    "PHONE" => "Y",
                    "SCHEDULE" => "Y",
                    "SEF_MODE" => "N",
                    "SET_TITLE" => "N",
                    "TITLE" => "",
                    "COMPONENT_TEMPLATE" => "stores"
                ),
                false,
                array(
                    "ACTIVE_COMPONENT" => "Y"
                )
            ); ?>

        </div>
    </div>
    </div>
    <!-- end wrapper -->


<? require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/footer.php"); ?>