<?
$MESS["AUTH_LOGIN_BUTTON"] = "Войти";
$MESS["AUTH_LOGIN"] = "Логин";
$MESS["AUTH_PASSWORD"] = "Пароль";
$MESS["AUTH_REMEMBER_ME"] = "Запомнить меня на этом компьютере";
$MESS["AUTH_FORGOT_PASSWORD_2"] = "Забыли свой пароль?";
$MESS["AUTH_REGISTER"] = "Регистрация";
$MESS["AUTH_LOGOUT_BUTTON"] = "Выйти";
$MESS["AUTH_PROFILE"] = "Мой профиль";
$MESS["AUTH_CABINET"] = "Личный кабинет";
$MESS["AUTH_A_INTERNAL"] = "Встроенная авторизация";
$MESS["AUTH_A_OPENID"] = "OpenID";
$MESS["AUTH_OPENID"] = "OpenID";
$MESS["AUTH_A_LIVEID"] = "LiveID";
$MESS["AUTH_LIVEID_LOGIN"] = "Log In";
$MESS["AUTH_CAPTCHA_PROMT"] = "Введите слово на картинке";
$MESS["AUTH_REMEMBER_SHORT"] = "Запомнить меня";
$MESS["socserv_as_user_form"] = "Войти как пользователь:";
$MESS["AUTH_SECURE_NOTE"]="Перед отправкой формы авторизации пароль будет зашифрован в браузере. Это позволит избежать передачи пароля в открытом виде.";
$MESS["AUTH_NONSECURE_NOTE"]="Пароль будет отправлен в открытом виде. Включите JavaScript в браузере, чтобы зашифровать пароль перед отправкой.";
$MESS["auth_form_comp_otp"] = "Одноразовый пароль:";
$MESS["auth_form_comp_otp_remember_title"] = "Запомнить код на этом компьютере";
$MESS["auth_form_comp_otp_remember"] = "Запомнить код";
$MESS["auth_form_comp_auth"] = "Авторизация";

// AUTH
$MESS["AUTH_TITLE"] = "Авторизация";
$MESS["AUTH_PLEASE_AUTH"] = "Пожалуйста, авторизуйтесь для входа на сайт";
$MESS["AUTH_LOGIN"] = "Логин";
$MESS["AUTH_PASSWORD"] = "Пароль";
$MESS["AUTH_REMEMBER_ME"] = "Запомнить меня на этом компьютере";
$MESS["AUTH_AUTHORIZE"] = "Войти";
$MESS["AUTH_REGISTER"] = "Зарегистрироваться";
$MESS["AUTH_FIRST_ONE"] = "Если вы впервые на сайте, заполните, пожалуйста, регистрационную форму.";
$MESS["AUTH_FORGOT_PASSWORD_2"] = "Забыли пароль?";
$MESS["AUTH_CAPTCHA_PROMT"] = "Введите слово на картинке";
$MESS["AUTH_TITLE"] = "Авторизация";
$MESS["AUTH_SECURE_NOTE"] = "Перед отправкой формы авторизации пароль будет зашифрован в браузере. Это позволит избежать передачи пароля в открытом виде.";
$MESS["AUTH_NONSECURE_NOTE"] = "Пароль будет отправлен в открытом виде. Включите JavaScript в браузере, чтобы зашифровать пароль перед отправкой.";

// REGISTER
$MESS["AUTH_REGISTER"] = "Регистрация";
$MESS["AUTH_NAME"] = "Имя";
$MESS["AUTH_LAST_NAME"] = "Фамилия";
$MESS["AUTH_LOGIN_MIN"] = "Логин (минимум 3 символа)";
$MESS["AUTH_CONFIRM"] = "Подтверждение пароля";
$MESS["CAPTCHA_REGF_PROMT"] = "Введите слово на картинке";
$MESS["AUTH_REQ"] = "Обязательные поля";
$MESS["AUTH_AUTH"] = "Авторизация";
$MESS["AUTH_PASSWORD_REQ"] = "Пароль";
$MESS["AUTH_EMAIL_WILL_BE_SENT"] = "На указанный в форме e-mail придет запрос на подтверждение регистрации.";
$MESS["AUTH_EMAIL_SENT"] = "На указанный в форме e-mail было выслано письмо с информацией о подтверждении регистрации.";
$MESS["AUTH_EMAIL"] = "Email";
$MESS["AUTH_SECURE_NOTE"] = "Перед отправкой формы пароль будет зашифрован в браузере. Это позволит избежать передачи пароля в открытом виде.";

// FORGOT
$MESS ['AUTH_FORGOT_PASSWORD_1'] = "Если вы забыли пароль, введите логин или Email. Контрольная строка для смены пароля, а также ваши регистрационные данные, будут высланы вам по E-Mail.";
$MESS ['AUTH_GET_CHECK_STRING'] = "Восстановление пароля";
$MESS ['AUTH_SEND'] = "Отправить";
$MESS ['AUTH_AUTH'] = "Авторизация";
$MESS ['AUTH_FORGOT'] = "Вспомнили пароль?";
$MESS["AUTH_LOGIN_EMAIL"] = "Логин или Email";
$MESS["system_auth_captcha"] = "Введите символы с картинки";
?>