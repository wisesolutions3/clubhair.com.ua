<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();                        
if(\Bitrix\Main\Loader::includeSharewareModule("krayt.perfumery") == \Bitrix\Main\Loader::MODULE_DEMO_EXPIRED || 
   \Bitrix\Main\Loader::includeSharewareModule("krayt.perfumery") ==  \Bitrix\Main\Loader::MODULE_NOT_FOUND
    )
{ return false;}



use Bitrix\Main\Loader;
use Bitrix\Main\ModuleManager;
use Bitrix\Main\Web\Json;

if (!Loader::includeModule('iblock'))
    return;

$aRiBlock = [];

$dbIBlock = CIBlock::GetList(array('SORT' => 'ASC', 'ID' => 'ASC'), ['ACTIVE' => "Y"]);
while($arIBlock = $dbIBlock->GetNext())
{
    $aRiBlock[$arIBlock['ID']] = $arIBlock['NAME'];
}
$arTemplateParameters = array(
	"IBLOCK_ID"=>array(
		"NAME" => GetMessage("MENU_IBLOCK_ID"),
		"TYPE" => "LIST",
		"VALUES" => $aRiBlock,
		"PARENT" => "BASE",
	)
);
?>
