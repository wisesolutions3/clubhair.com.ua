<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Кошик");
?>
    <div class="title_box">
        <h1><?=$APPLICATION->ShowTitle(false);?></h1>
    </div>

<?$APPLICATION->IncludeComponent(
    "bitrix:sale.basket.basket",
    "basket",
    array(
        "ACTION_VARIABLE" => "basketAction",
        "AUTO_CALCULATION" => "Y",
        "COLUMNS_LIST" => array(
            0 => "NAME",
            1 => "DISCOUNT",
            2 => "DELETE",
            3 => "DELAY",
            4 => "TYPE",
            5 => "PRICE",
            6 => "QUANTITY",
            7 => "SUM",
            8 => "PROPERTY_PRODUCT_IMAGE",
        ),
        "CORRECT_RATIO" => "N",
        "GIFTS_BLOCK_TITLE" => "Выберите один из подарков",
        "GIFTS_CONVERT_CURRENCY" => "N",
        "GIFTS_HIDE_BLOCK_TITLE" => "N",
        "GIFTS_HIDE_NOT_AVAILABLE" => "N",
        "GIFTS_MESS_BTN_BUY" => "Выбрать",
        "GIFTS_MESS_BTN_DETAIL" => "Подробнее",
        "GIFTS_PAGE_ELEMENT_COUNT" => "4",
        "GIFTS_PLACE" => "BOTTOM",
        "GIFTS_PRODUCT_PROPS_VARIABLE" => "prop",
        "GIFTS_PRODUCT_QUANTITY_VARIABLE" => "quantity",
        "GIFTS_SHOW_DISCOUNT_PERCENT" => "Y",
        "GIFTS_SHOW_IMAGE" => "Y",
        "GIFTS_SHOW_NAME" => "Y",
        "GIFTS_SHOW_OLD_PRICE" => "N",
        "GIFTS_TEXT_LABEL_GIFT" => "Подарок",
        "HIDE_COUPON" => "N",
        "OFFERS_PROPS" => array(
        ),
        "PATH_TO_ORDER" => SITE_DIR."personal/order/make/",
        "PRICE_VAT_SHOW_VALUE" => "N",
        "QUANTITY_FLOAT" => "N",
        "SET_TITLE" => "Y",
        "TEMPLATE_THEME" => "blue",
        "USE_ENHANCED_ECOMMERCE" => "N",
        "USE_GIFTS" => "N",
        "USE_PREPAYMENT" => "N",
        "COMPONENT_TEMPLATE" => "basket",
        "DEFERRED_REFRESH" => "N",
        "USE_DYNAMIC_SCROLL" => "Y",
        "SHOW_FILTER" => "Y",
        "SHOW_RESTORE" => "Y",
        "COLUMNS_LIST_EXT" => array(
            0 => "PREVIEW_PICTURE",
//			1 => "PROPS",
            2 => "DELETE",
            3 => "DELAY",
            4 => "SUM",
        ),
        "COLUMNS_LIST_MOBILE" => array(
            0 => "PREVIEW_PICTURE",
//			1 => "PROPS",
            2 => "DELETE",
            3 => "DELAY",
            4 => "SUM",
        ),
        "TOTAL_BLOCK_DISPLAY" => array(
            0 => "bottom",
        ),
        "DISPLAY_MODE" => "extended",
        "PRICE_DISPLAY_MODE" => "Y",
        "SHOW_DISCOUNT_PERCENT" => "Y",
        "DISCOUNT_PERCENT_POSITION" => "top-left",
        "PRODUCT_BLOCKS_ORDER" => "props,sku,columns",
        "USE_PRICE_ANIMATION" => "Y",
        "LABEL_PROP" => array(
        ),
        "COMPATIBLE_MODE" => "Y",
        "LABEL_PROP_MOBILE" => "",
        "LABEL_PROP_POSITION" => "",
        "ADDITIONAL_PICT_PROP_3" => "-",
        "ADDITIONAL_PICT_PROP_4" => "-",
        "BASKET_IMAGES_SCALING" => "adaptive",
        "EMPTY_BASKET_HINT_PATH" => SITE_DIR."catalog/"
    ),
    false
);?><?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>