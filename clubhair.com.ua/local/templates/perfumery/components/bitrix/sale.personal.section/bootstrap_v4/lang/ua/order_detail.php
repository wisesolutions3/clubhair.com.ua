<?
$MESS["AUTH_AUTHORIZE"] = "Увійти";
$MESS["AUTH_CAPTCHA_PROMT"] = "Введіть слово на картинці";
$MESS["AUTH_FIRST_ONE"] = "Якщо ви вперше на сайті, заповніть, будь ласка, реєстраційну форму.";
$MESS["AUTH_FORGOT_PASSWORD_2"] = "Забули свій пароль?";
$MESS["AUTH_LOGIN"] = "Логін";
$MESS["AUTH_NONSECURE_NOTE"] = "Пароль буде відправлений у відкритому вигляді. Увімкніть JavaScript в браузері, щоб зашифрувати пароль перед відправкою.";
$MESS["AUTH_PASSWORD"] = "Пароль";
$MESS["AUTH_PLEASE_AUTH"] = "Будь ласка, увійдіть для входу на сайт";
$MESS["AUTH_REGISTER"] = "Реєстрація";
$MESS["AUTH_REMEMBER_ME"] = "Запам'ятати мене на цьому комп'ютері";
$MESS["AUTH_SECURE_NOTE"] = "Перед відправкою форми авторизації пароль буде зашифрований в браузері. Це дозволить уникнути передачі пароля у відкритому вигляді.";
$MESS["AUTH_TITLE"] = "Авторизація";
$MESS["SOPC_TPL_BILL"] = "Рахунок";
$MESS["SOPC_TPL_FROM_DATE"] = "від";
$MESS["SOPC_TPL_NOTPAID"] = "Не оплачено";
$MESS["SOPC_TPL_NUMBER_SIGN"] = "№";
$MESS["SOPC_TPL_PAID"] = "Сплачено";
$MESS["SOPC_TPL_RESTRICTED_PAID"] = "На перевірці менеджером";
$MESS["SOPC_TPL_RESTRICTED_PAID_MESSAGE"] = "<b> Зверніть увагу: </b> оплата замовлення буде доступна після підтвердження менеджером";
$MESS["SOPC_TPL_SUM_TO_PAID"] = "Сума до оплати за рахунком";
$MESS["SPS_CHAIN_MAIN"] = "Мій кабінет";
$MESS["SPS_CHAIN_ORDERS"] = "Мої замовлення";
$MESS["SPS_CHAIN_ORDER_DETAIL"] = "Інформація про замовлення № #ID#";
?>