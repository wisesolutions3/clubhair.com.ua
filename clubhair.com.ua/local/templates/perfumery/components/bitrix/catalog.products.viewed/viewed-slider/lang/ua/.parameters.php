<?
$MESS["ADD_PICT_PROP_TIP"] = "Властивість додаткових картинок товару";
$MESS["ADD_TO_BASKET_ACTION_ADD"] = "додавання в корзину";
$MESS["ADD_TO_BASKET_ACTION_BUY"] = "покупки";
$MESS["CP_CPV_TPL_ADD_PICT_PROP"] = "Додаткова картинка основного товару";
$MESS["CP_CPV_TPL_ADD_TO_BASKET_ACTION"] = "Показувати кнопку додавання в корзину або покупки";
$MESS["CP_CPV_TPL_COMPARE_NAME"] = "Унікальне ім'я для списку порівняння";
$MESS["CP_CPV_TPL_CUSTOM_FILTER"] = "Фільтр товарів";
$MESS["CP_CPV_TPL_DATA_LAYER_NAME"] = "Ім'я контейнера даних";
$MESS["CP_CPV_TPL_DISCOUNT_PERCENT_POSITION"] = "Розташування відсотка знижки";
$MESS["CP_CPV_TPL_DML_EXT"] = "розширений";
$MESS["CP_CPV_TPL_DML_SIMPLE"] = "простий режим";
$MESS["CP_CPV_TPL_ENLARGE_PRODUCT"] = "Виділяти товари в списку";
$MESS["CP_CPV_TPL_ENLARGE_PRODUCT_PROP"] = "по властивості, підганяти по шаблону (можлива зміна сортування елементів)";
$MESS["CP_CPV_TPL_ENLARGE_PRODUCT_STRICT"] = "за обраним шаблоном (строго)";
$MESS["CP_CPV_TPL_ENLARGE_PROP"] = "Виділяти за обраним властивості";
$MESS["CP_CPV_TPL_LABEL_PROP"] = "Властивості міток товару";
$MESS["CP_CPV_TPL_LABEL_PROP_MOBILE"] = "Властивості міток товару, які відображаються на мобільних пристроях";
$MESS["CP_CPV_TPL_LABEL_PROP_POSITION"] = "Розташування міток товару";
$MESS["CP_CPV_TPL_LAZY_LOAD"] = "Показати кнопку ледачою завантаження Lazy Load";
$MESS["CP_CPV_TPL_LOAD_ON_SCROLL"] = "Довантажувати товари при прокручуванні до кінця";
$MESS["CP_CPV_TPL_MESS_BTN_ADD_TO_BASKET"] = "Текст кнопки \"Додати в кошик\"";
$MESS["CP_CPV_TPL_MESS_BTN_ADD_TO_BASKET_DEFAULT"] = "В кошик";
$MESS["CP_CPV_TPL_MESS_BTN_BUY"] = "Текст кнопки \"Купити\"";
$MESS["CP_CPV_TPL_MESS_BTN_BUY_DEFAULT"] = "купити";
$MESS["CP_CPV_TPL_MESS_BTN_COMPARE"] = "Текст кнопки \"Порівняти\"";
$MESS["CP_CPV_TPL_MESS_BTN_COMPARE_DEFAULT"] = "Порівняти";
$MESS["CP_CPV_TPL_MESS_BTN_DETAIL"] = "Текст кнопки \"Детальніше\"";
$MESS["CP_CPV_TPL_MESS_BTN_DETAIL_DEFAULT"] = "Детальніше";
$MESS["CP_CPV_TPL_MESS_BTN_LAZY_LOAD"] = "Текст кнопки \"Показати ще\"";
$MESS["CP_CPV_TPL_MESS_BTN_LAZY_LOAD_DEFAULT"] = "Показати ще";
$MESS["CP_CPV_TPL_MESS_BTN_SUBSCRIBE"] = "Текст кнопки \"Повідомити про надходження\"";
$MESS["CP_CPV_TPL_MESS_BTN_SUBSCRIBE_DEFAULT"] = "Підписатися";
$MESS["CP_CPV_TPL_MESS_NOT_AVAILABLE"] = "Повідомлення про відсутність товару";
$MESS["CP_CPV_TPL_MESS_NOT_AVAILABLE_DEFAULT"] = "Немає в наявності";
$MESS["CP_CPV_TPL_MESS_RELATIVE_QUANTITY_FEW"] = "Текст для значення менше";
$MESS["CP_CPV_TPL_MESS_RELATIVE_QUANTITY_FEW_DEFAULT"] = "мало";
$MESS["CP_CPV_TPL_MESS_RELATIVE_QUANTITY_MANY"] = "Текст для значення більше";
$MESS["CP_CPV_TPL_MESS_RELATIVE_QUANTITY_MANY_DEFAULT"] = "багато";
$MESS["CP_CPV_TPL_MESS_SHOW_MAX_QUANTITY"] = "Текст для залишку";
$MESS["CP_CPV_TPL_MESS_SHOW_MAX_QUANTITY_DEFAULT"] = "наявність";
$MESS["CP_CPV_TPL_OFFER_ADD_PICT_PROP"] = "Додаткові картинки пропозиції";
$MESS["CP_CPV_TPL_OFFER_TREE_PROPS"] = "Властивості для відбору пропозицій";
$MESS["CP_CPV_TPL_PRODUCT_BLOCKS_ORDER"] = "Порядок відображення блоків товару";
$MESS["CP_CPV_TPL_PRODUCT_BLOCK_BUTTONS"] = "дії";
$MESS["CP_CPV_TPL_PRODUCT_BLOCK_COMPARE"] = "порівняння";
$MESS["CP_CPV_TPL_PRODUCT_BLOCK_PRICE"] = "Ціна";
$MESS["CP_CPV_TPL_PRODUCT_BLOCK_PROPS"] = "властивості";
$MESS["CP_CPV_TPL_PRODUCT_BLOCK_QUANTITY"] = "кількість";
$MESS["CP_CPV_TPL_PRODUCT_BLOCK_QUANTITY_LIMIT"] = "залишок";
$MESS["CP_CPV_TPL_PRODUCT_BLOCK_SKU"] = "торгова пропозиція";
$MESS["CP_CPV_TPL_PRODUCT_DISPLAY_MODE"] = "схема відображення";
$MESS["CP_CPV_TPL_PRODUCT_ROW_VARIANTS"] = "Варіант відображення товарів";
$MESS["CP_CPV_TPL_PRODUCT_SUBSCRIPTION"] = "Дозволити оповіщення для відсутніх товарів";
$MESS["CP_CPV_TPL_PROPERTY_CODE_MOBILE"] = "Властивості товарів, які відображаються на мобільних пристроях";
$MESS["CP_CPV_TPL_PROP_EMPTY"] = "не обрано";
$MESS["CP_CPV_TPL_RELATIVE_QUANTITY_FACTOR"] = "Значення, від якого відбувається підміна";
$MESS["CP_CPV_TPL_SETTINGS_DELETE"] = "видалити";
$MESS["CP_CPV_TPL_SETTINGS_INVALID_CONDITION"] = "Умова задано невірно";
$MESS["CP_CPV_TPL_SETTINGS_QUANTITY"] = "Кількість товарів на сторінці";
$MESS["CP_CPV_TPL_SETTINGS_QUANTITY_BIG_DATA"] = "Кількість товарів BigData на сторінці";
$MESS["CP_CPV_TPL_SETTINGS_VARIANT"] = "різновид";
$MESS["CP_CPV_TPL_SHOW_CLOSE_POPUP"] = "Показувати кнопку продовження покупок у спливаючих вікнах";
$MESS["CP_CPV_TPL_SHOW_DISCOUNT_PERCENT"] = "Показувати відсоток знижки";
$MESS["CP_CPV_TPL_SHOW_MAX_QUANTITY"] = "Показувати залишок товару";
$MESS["CP_CPV_TPL_SHOW_MAX_QUANTITY_M"] = "з підміною залишку текстом";
$MESS["CP_CPV_TPL_SHOW_MAX_QUANTITY_N"] = "Не відображати";
$MESS["CP_CPV_TPL_SHOW_MAX_QUANTITY_Y"] = "з відображенням реального залишку";
$MESS["CP_CPV_TPL_SHOW_OLD_PRICE"] = "Показувати стару ціну";
$MESS["CP_CPV_TPL_SHOW_SLIDER"] = "Показувати слайдер для товарів";
$MESS["CP_CPV_TPL_SLIDER_INTERVAL"] = "Інтервал зміни слайдів, мс";
$MESS["CP_CPV_TPL_SLIDER_PROGRESS"] = "Показувати смугу прогресу";
$MESS["CP_CPV_TPL_TEMPLATE_THEME"] = "Колірна тема";
$MESS["CP_CPV_TPL_THEME_BLUE"] = "синя (тема за замовчуванням)";
$MESS["CP_CPV_TPL_THEME_GREEN"] = "зелена";
$MESS["CP_CPV_TPL_THEME_RED"] = "червона";
$MESS["CP_CPV_TPL_THEME_SITE"] = "Брати тему з налаштувань сайту (для вирішення bitrix.eshop)";
$MESS["CP_CPV_TPL_THEME_YELLOW"] = "жовта";
$MESS["CP_CPV_TPL_USE_ENHANCED_ECOMMERCE"] = "Відправляти дані електронної торгівлі в Google і Яндекс";
$MESS["LABEL_PROP_TIP"] = "Властивість міток товару";
$MESS["MESS_BTN_ADD_TO_BASKET_TIP"] = "Який текст виводити на кнопці";
$MESS["MESS_BTN_BUY_TIP"] = "Який текст виводити на кнопці";
$MESS["MESS_BTN_COMPARE_TIP"] = "Який текст виводити на кнопці";
$MESS["MESS_BTN_DETAIL_TIP"] = "Який текст виводити на кнопці";
$MESS["MESS_BTN_SUBSCRIBE_TIP"] = "Який текст виводити на кнопці";
$MESS["MESS_NOT_AVAILABLE_TIP"] = "Повідомлення про відсутність товару і неможливості його купити";
$MESS["OFFER_ADD_PICT_PROP_TIP"] = "Властивість додаткових картинок пропозицій (якщо є)";
$MESS["OFFER_TREE_PROPS_TIP"] = "Властивості, за значеннями яких будуть групуватися торгові пропозиції";
$MESS["PRODUCT_BLOCKS_ORDER_TIP"] = "Для деяких блоків порядок може не дотримуватися в зв'язку з особливостями шаблону. Також, блок товару може не відображатися, так як його функціонал вимкнений або недоступний.";
$MESS["PRODUCT_DISPLAY_MODE_TIP"] = "Схема відображення товарів (з SKU або без і т.д.)";
$MESS["PRODUCT_SUBSCRIPTION_TIP"] = "Дозволити сповіщати клієнта, що цікавить його товар став доступний до покупки";
$MESS["RELATIVE_QUANTITY_FACTOR_TIP"] = "Порівняння відбувається зі значенням, розрахованим за формулою \"Доступне кількість\" / \"Коефіцієнт одиниці виміру\"";
$MESS["SHOW_CLOSE_POPUP_TIP"] = "Якщо опція відзначена, у спливаючих вікнах буде показана кнопка \"Продовжити покупки\"";
$MESS["SHOW_DISCOUNT_PERCENT_TIP"] = "Висновок процентного значення знижки, якщо знижка є";
$MESS["SHOW_OLD_PRICE_TIP"] = "Показувати стару ціну, якщо є знижка";
$MESS["TEMPLATE_THEME_TIP"] = "Колірна тема для відображення. За замовчуванням береться синя тема.";
$MESS["USE_ENHANCED_ECOMMERCE_TIP"] = "Потрібна додаткова настройка в Google Analytics Enhanced Ecommerce і / або Яндекс.Метрика";
?>