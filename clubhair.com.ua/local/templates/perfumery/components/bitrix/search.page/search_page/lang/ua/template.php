<?
$MESS["CT_BSP_ADDITIONAL_PARAMS"] = "Додаткові параметри пошуку";
$MESS["CT_BSP_KEYBOARD_WARNING"] = "У запиті \"#query#\" відновлена ​​розкладка клавіатури.";
$MESS["SEARCH_ALL"] = "(Скрізь)";
$MESS["SEARCH_AND"] = "і";
$MESS["SEARCH_AND_ALT"] = "Оператор <i> логічне &quot; і &quot; </i> мається на увазі, його можна опускати: запит & quot; контактна інформація & quot; повністю еквівалентний запиту & quot; контактна і інформація & quot ;.";
$MESS["SEARCH_BRACKETS_ALT"] = "<I> Круглі дужки </ i> задають порядок дії логічних операторів.";
$MESS["SEARCH_CORRECT_AND_CONTINUE"] = "Виправте пошукову фразу і повторіть пошук.";
$MESS["SEARCH_DESCRIPTION"] = "опис";
$MESS["SEARCH_ERROR"] = "У пошуковій фразі виявлена ​​помилка:";
$MESS["SEARCH_GO"] = "Шукати";
$MESS["SEARCH_LOGIC"] = "Логічні оператори:";
$MESS["SEARCH_MODIFIED"] = "змінено:";
$MESS["SEARCH_NOT"] = "НЕ";
$MESS["SEARCH_NOTHING_TO_FOUND"] = "На жаль, по Вашому запиту нічого не знайдено.";
$MESS["SEARCH_NOT_ALT"] = "Оператор <i> логічне & quot, не & quot; </ i> обмежує пошук сторінок, що не містять слово, вказане після оператора.";
$MESS["SEARCH_OPERATOR"] = "оператор";
$MESS["SEARCH_OR"] = "або";
$MESS["SEARCH_OR_ALT"] = "Оператор <i> логічне & quot; або & quot; </ i> дозволяє шукати товари, що містять хоча б один з операндів.";
$MESS["SEARCH_PATH"] = "шлях:";
$MESS["SEARCH_SINTAX"] = "<B> Синтаксис пошукового запиту: </ b> <br /> <br /> Зазвичай запит вдає із себе просто одне або кілька слів, наприклад: <br /> <i> контактна інформація </ i> <br /> За таким запитом будуть знайдені сторінки, на яких зустрічаються обидва слова запиту. <br";
$MESS["SEARCH_SORTED_BY_DATE"] = "Сортувати по даті";
$MESS["SEARCH_SORTED_BY_RANK"] = "Відсортовано за релевантністю";
$MESS["SEARCH_SORT_BY_DATE"] = "Сортувати за датою";
$MESS["SEARCH_SORT_BY_RANK"] = "Сортувати за релевантністю";
$MESS["SEARCH_SYNONIM"] = "Синоніми";
?>