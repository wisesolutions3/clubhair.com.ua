<?
include_once($_SERVER['DOCUMENT_ROOT'].'/bitrix/modules/main/include/urlrewrite.php');

CHTTP::SetStatus("404 Not Found");
@define("ERROR_404","Y");

require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");

$APPLICATION->SetTitle("404 Not Found");

?>


<?if($_REQUEST['test'] == 'test'){?>

	<form action="https://secure.wayforpay.com/pay" method="post" id="wfp_payment_form" accept-charset="utf-8"><input type="hidden" name="merchantAccount" value="clubhair_com_ua" /><input type="hidden" name="orderReference" value="W4P_180_1627053880" /><input type="hidden" name="orderDate" value="1627053880" /><input type="hidden" name="merchantAuthType" value="simpleSignature" /><input type="hidden" name="merchantDomainName" value="megabai.com.ua:443" /><input type="hidden" name="merchantTransactionSecureType" value="AUTO" /><input type="hidden" name="currency" value="UAH" /><input type="hidden" name="amount" value="11112" /><input type="hidden" name="productName[]" value="BAI 0751 Пенал підвісний 40 cм, палісандр" /><input type="hidden" name="productPrice[]" value="11112" /><input type="hidden" name="productCount[]" value="1" /><input type="hidden" name="serviceUrl" value="https://megabai.com.ua/bitrix/tools/w4p_result.php" /><input type="hidden" name="returnUrl" value="https://megabai.com.ua/personal/orders/?pay_order=180" /><input type="hidden" name="clientFirstName" value="test" /><input type="hidden" name="clientLastName" value="фівіфвфі" /><input type="hidden" name="clientEmail" value="фівфівфів@123123.123" /><input type="hidden" name="clientPhone" value="381231231231" /><input type="hidden" name="clientCity" value="test" /><input type="hidden" name="clientAddress" value=", " /><input type="hidden" name="language" value=", " /><input type="hidden" name="merchantSignature" value="a073507d8d7da271f3ed946cd172c236" /><input type="submit" /></form>
<br>

<?}?>





<div class="wrapper-inner">

    <div class="page_404 row">
        <div class="number col-lg-5 col-md-12 col-12">404</div>
        <div class="text_404 col-lg-7 col-md-12 col-12">
            <div class="title_404">Сталася помилка, нам дуже шкода</div>
            <div class="content_404">Помилка могла статися з кількох причин:
                <ul class="line">
                    <li>Ви ввели неправильну адресу</li>
                    <li>Сторінка, на яку ви хотіли зайти, застаріла і була видалена</li>
                    <li>На сервері сталася помилка. Якщо так, то ми вже знаємо про неї і обов'язково виправимо</li>
                </ul>
            </div>
            <div class="back_link">
                <a href="/">
                    <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" viewBox="275.25 157.75 40 40">
                        <path d="M286.421,178.089l5.326,4.517c0.228,0.193,0.569,0.193,0.798,0c0.228-0.193,0.228-0.482,0-0.676l-4.33-3.672  h15.466c0.313,0,0.569-0.217,0.569-0.482c0-0.267-0.256-0.484-0.569-0.484h-15.522l4.387-3.719c0.228-0.193,0.228-0.483,0-0.677  c-0.114-0.097-0.257-0.146-0.399-0.146c-0.142,0-0.284,0.049-0.398,0.146l-5.326,4.517  C286.193,177.605,286.193,177.896,286.421,178.089z"></path>
                    </svg>
                    повернутися на головну
                </a>
            </div>
        </div>
        <div style="clear: both;"></div>
    </div>

</div>

<?

require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>