<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();                        
if(\Bitrix\Main\Loader::includeSharewareModule("krayt.perfumery") == \Bitrix\Main\Loader::MODULE_DEMO_EXPIRED || 
   \Bitrix\Main\Loader::includeSharewareModule("krayt.perfumery") ==  \Bitrix\Main\Loader::MODULE_NOT_FOUND
    )
{ return false;}
use Bitrix\Main\Localization\Loc;
Loc::loadMessages(__FILE__);
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);?>

    <div class="modal-dialog modal-dialog-centered">
        <div class="modal-content">
            <?if(!empty($arResult["STORES"]) && $arParams["MAIN_TITLE"] != ''):?>
                <div class="modal-header"><?=$arParams["MAIN_TITLE"]?></div>
            <?endif;?>
            <div class="modal-body">
<!--                <pre>--><?//=print_r($arResult["STORES"])?><!--</pre>-->
                <div class="bx_storege" id="catalog_store_amount_div">
                    <?if(!empty($arResult["STORES"])):?>
                        <div class="header-stores-descr d-lg-block d-none">
                            <div class="row m-0">
                                <div class="col-4 pl-0"><?=GetMessage('S_STORE')?></div>
                                <div class="col-3"><?=GetMessage('S_PHONE')?></div>
                                <div class="col-3"><?=GetMessage('S_SCHEDULE')?></div>
                                <div class="col-2 pr-0"><?=GetMessage('S_AMOUNT')?></div>
                            </div>
                        </div>
                        <ul class="stores-list" id="c_store_amount">
                            <?foreach($arResult["STORES"] as $pid => $arProperty):?>
                                <li class="stores-list-item" style="display: <? echo ($arParams['SHOW_EMPTY_STORE'] == 'N' && isset($arProperty['REAL_AMOUNT']) && $arProperty['REAL_AMOUNT'] <= 0 ? 'none' : ''); ?>;">
                                    <div class="row m-0">
                                        <div class="col-lg-4 col-12 col-name col-item pl-0">
                                            <?if (isset($arProperty["IMAGE_ID"]) && !empty($arProperty["IMAGE_ID"])):?>
                                                <span class="schedule"><?=GetMessage('S_IMAGE')?> <?=CFile::ShowImage($arProperty["IMAGE_ID"], 200, 200, "border=0", "", true);?></span><br />
                                            <?endif;?>
                                            <?if (isset($arProperty["TITLE"])):?>
<!--                                                <a class="name-store" href="--><?//=$arProperty["URL"]?><!--">--><?//=$arProperty["TITLE"]?><!--</a>-->
                                                <div class="name-store"><?=$arProperty["TITLE"]?></div>
                                                <div class="address-store"><?=$arProperty["ADDRESS"]?></div>
                                            <?endif;?>
                                            <?if (isset($arProperty["DESCRIPTION"])):?>
                                                <span><?=$arProperty["DESCRIPTION"]?></span><br />
                                            <?endif;?>
                                            <?if (isset($arProperty["COORDINATES"])):?>
                                                <span><?=GetMessage('S_COORDINATES')?> <?=$arProperty["COORDINATES"]["GPS_N"]?>, <?=$arProperty["COORDINATES"]["GPS_S"]?></span><br />
                                            <?endif;?>
                                        </div>
                                        <div class="col-lg-3 col-md-4 col-sm-6 col-phone col-item pl-lg-3 pl-0">
                                            <?if (isset($arProperty["PHONE"])):?>
                                                <div class="phone-store">
                                                    <span class="fa fa-phone"></span>
                                                    <span><a href="tel:<?=$arProperty["PHONE_URL"]?>"><?=$arProperty["PHONE"]?></a></span>
                                                </div>
                                            <?endif;?>
                                            <?if (isset($arProperty["EMAIL"])):?>
                                                <div class="mail-store"><?=GetMessage('S_EMAIL')?> <?=$arProperty["EMAIL"]?></div>
                                            <?endif;?>
                                        </div>
                                        <div class="col-lg-3 col-md-4 col-sm-6 col-schedule col-item pl-sm-3 pl-0">
                                            <?if (isset($arProperty["SCHEDULE"])):?>
                                                <div class="schedule-store">
                                                    <span class="fa fa-clock-o"></span>
                                                    <span><?=$arProperty["SCHEDULE"]?></span>
                                                </div>
                                            <?endif;?>
                                        </div >
                                        <div class="col-lg-2 col-md-4 col-balance col-item pr-0 pl-md-3 pl-0">
                                            <div class="d-lg-none" style="font-weight: normal;"><?=GetMessage('S_AMOUNT')?>:</div>
                                            <div class="balance-store <?=($arProperty["REAL_AMOUNT"] == "0")? 'absent' :'' ;?>" id="<?=$arResult['JS']['ID']?>_<?=$arProperty['ID']?>"><?=$arProperty["AMOUNT"]?></div>
                                        </div>
                                        <?
                                        if (!empty($arProperty['USER_FIELDS']) && is_array($arProperty['USER_FIELDS']))
                                        {?>
                                            <span style="display: none;">
                                        <?
                                        foreach ($arProperty['USER_FIELDS'] as $userField)
                                        {
                                            if (isset($userField['CONTENT']))
                                            {
                                                ?><span><?=$userField['TITLE']?>: <?=$userField['CONTENT']?></span><br /><?
                                            }
                                        }?>
                                        </span>
                                            <?
                                        }
                                        ?>
                                    </div>
                                </li>
                            <?endforeach;?>
                        </ul>
                    <?endif;?>
                </div>
                <?if (isset($arResult["IS_SKU"]) && $arResult["IS_SKU"] == 1):?>
                    <script type="text/javascript">
                        var obStoreAmount = new JCCatalogStoreSKU(<? echo CUtil::PhpToJSObject($arResult['JS'], false, true, true); ?>);
                    </script>
                    <?
                endif;?>
            </div>
            <button type="button" class="close-btn" data-dismiss="modal"></button>
        </div>
    </div>
