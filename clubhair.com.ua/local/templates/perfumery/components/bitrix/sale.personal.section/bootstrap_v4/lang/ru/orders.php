<?php
$MESS["SPS_CHAIN_MAIN"] = "Мой кабинет";
$MESS["SPS_CHAIN_ORDERS"] = "Мои заказы";

$MESS["AUTH_PLEASE_AUTH"] = "Пожалуйста, авторизуйтесь для входа на сайт";
$MESS["AUTH_LOGIN"] = "Логин";
$MESS["AUTH_PASSWORD"] = "Пароль";
$MESS["AUTH_REMEMBER_ME"] = "Запомнить меня на этом компьютере";
$MESS["AUTH_AUTHORIZE"] = "Войти";
$MESS["AUTH_REGISTER"] = "Регистрация";
$MESS["AUTH_FIRST_ONE"] = "Если вы впервые на сайте, заполните, пожалуйста, регистрационную форму.";
$MESS["AUTH_FORGOT_PASSWORD_2"] = "Забыли свой пароль?";
$MESS["AUTH_CAPTCHA_PROMT"] = "Введите слово на картинке";
$MESS["AUTH_TITLE"] = "Авторизация";
$MESS["AUTH_SECURE_NOTE"] = "Перед отправкой формы авторизации пароль будет зашифрован в браузере. Это позволит избежать передачи пароля в открытом виде.";
$MESS["AUTH_NONSECURE_NOTE"] = "Пароль будет отправлен в открытом виде. Включите JavaScript в браузере, чтобы зашифровать пароль перед отправкой.";

$MESS["SOPC_TPL_BILL"] = "Счет";
$MESS["SOPC_TPL_NUMBER_SIGN"] = "№";
$MESS["SOPC_TPL_FROM_DATE"] = "от";
$MESS["SOPC_TPL_NOTPAID"] = "Не оплачено";
$MESS["SOPC_TPL_PAID"] = "Оплачено";
$MESS["SOPC_TPL_SUM_TO_PAID"] = "Сумма к оплате по счету";
$MESS["SOPC_TPL_RESTRICTED_PAID"] = "На проверке менеджером";
$MESS["SOPC_TPL_RESTRICTED_PAID_MESSAGE"] = "<b>Обратите внимание:</b> оплата заказа будет доступна после подтверждения менеджером";
