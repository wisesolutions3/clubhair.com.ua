<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();                        
if(\Bitrix\Main\Loader::includeSharewareModule("krayt.perfumery") == \Bitrix\Main\Loader::MODULE_DEMO_EXPIRED || 
   \Bitrix\Main\Loader::includeSharewareModule("krayt.perfumery") ==  \Bitrix\Main\Loader::MODULE_NOT_FOUND
    )
{ return false;}
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
use Bitrix\Main\Localization\Loc;

$this->setFrameMode(true);
?>
<div class="promo-detail">
<?$ElementID = $APPLICATION->IncludeComponent(
	"bitrix:news.detail",
	"flat",
	Array(
		"DISPLAY_DATE" => $arParams["DISPLAY_DATE"],
		"DISPLAY_NAME" => $arParams["DISPLAY_NAME"],
		"DISPLAY_PICTURE" => $arParams["DISPLAY_PICTURE"],
		"DISPLAY_PREVIEW_TEXT" => $arParams["DISPLAY_PREVIEW_TEXT"],
		"IBLOCK_TYPE" => $arParams["IBLOCK_TYPE"],
		"IBLOCK_ID" => $arParams["IBLOCK_ID"],
		"FIELD_CODE" => $arParams["DETAIL_FIELD_CODE"],
		"PROPERTY_CODE" => $arParams["DETAIL_PROPERTY_CODE"],
		"DETAIL_URL" => $arResult["FOLDER"].$arResult["URL_TEMPLATES"]["detail"],
		"SECTION_URL" => $arResult["FOLDER"].$arResult["URL_TEMPLATES"]["section"],
		"META_KEYWORDS" => $arParams["META_KEYWORDS"],
		"META_DESCRIPTION" => $arParams["META_DESCRIPTION"],
		"BROWSER_TITLE" => $arParams["BROWSER_TITLE"],
		"SET_CANONICAL_URL" => $arParams["DETAIL_SET_CANONICAL_URL"],
		"DISPLAY_PANEL" => $arParams["DISPLAY_PANEL"],
		"SET_LAST_MODIFIED" => $arParams["SET_LAST_MODIFIED"],
		"SET_TITLE" => "Y",
		"MESSAGE_404" => $arParams["MESSAGE_404"],
		"SET_STATUS_404" => $arParams["SET_STATUS_404"],
		"SHOW_404" => $arParams["SHOW_404"],
		"FILE_404" => $arParams["FILE_404"],
		"INCLUDE_IBLOCK_INTO_CHAIN" => $arParams["INCLUDE_IBLOCK_INTO_CHAIN"],
		"ADD_SECTIONS_CHAIN" => $arParams["ADD_SECTIONS_CHAIN"],
		"ACTIVE_DATE_FORMAT" => $arParams["DETAIL_ACTIVE_DATE_FORMAT"],
		"CACHE_TYPE" => $arParams["CACHE_TYPE"],
		"CACHE_TIME" => $arParams["CACHE_TIME"],
		"CACHE_GROUPS" => $arParams["CACHE_GROUPS"],
		"USE_PERMISSIONS" => $arParams["USE_PERMISSIONS"],
		"GROUP_PERMISSIONS" => $arParams["GROUP_PERMISSIONS"],
		"DISPLAY_TOP_PAGER" => $arParams["DETAIL_DISPLAY_TOP_PAGER"],
		"DISPLAY_BOTTOM_PAGER" => $arParams["DETAIL_DISPLAY_BOTTOM_PAGER"],
		"PAGER_TITLE" => $arParams["DETAIL_PAGER_TITLE"],
		"PAGER_SHOW_ALWAYS" => "N",
		"PAGER_TEMPLATE" => $arParams["DETAIL_PAGER_TEMPLATE"],
		"PAGER_SHOW_ALL" => $arParams["DETAIL_PAGER_SHOW_ALL"],
		"CHECK_DATES" => $arParams["CHECK_DATES"],
		"ELEMENT_ID" => $arResult["VARIABLES"]["ELEMENT_ID"],
		"ELEMENT_CODE" => $arResult["VARIABLES"]["ELEMENT_CODE"],
		"SECTION_ID" => $arResult["VARIABLES"]["SECTION_ID"],
		"SECTION_CODE" => $arResult["VARIABLES"]["SECTION_CODE"],
		"IBLOCK_URL" => $arResult["FOLDER"].$arResult["URL_TEMPLATES"]["news"],
		"SEARCH_PAGE" => $arResult["FOLDER"].$arResult["URL_TEMPLATES"]["search"],
		"USE_SHARE" => $arParams["USE_SHARE"],
		"SHARE_HIDE" => $arParams["SHARE_HIDE"],
		"SHARE_TEMPLATE" => $arParams["SHARE_TEMPLATE"],
		"SHARE_HANDLERS" => $arParams["SHARE_HANDLERS"],
		"SHARE_SHORTEN_URL_LOGIN" => $arParams["SHARE_SHORTEN_URL_LOGIN"],
		"SHARE_SHORTEN_URL_KEY" => $arParams["SHARE_SHORTEN_URL_KEY"],
		"ADD_ELEMENT_CHAIN" => (isset($arParams["ADD_ELEMENT_CHAIN"]) ? $arParams["ADD_ELEMENT_CHAIN"] : ''),
		"USE_RATING" => $arParams["USE_RATING"],
		"MAX_VOTE" => $arParams["MAX_VOTE"],
		"VOTE_NAMES" => $arParams["VOTE_NAMES"],
		"MEDIA_PROPERTY" => $arParams["MEDIA_PROPERTY"],
		"SLIDER_PROPERTY" => $arParams["SLIDER_PROPERTY"],
		"TEMPLATE_THEME" => $arParams["TEMPLATE_THEME"],
		"STRICT_SECTION_CHECK" => $arParams["STRICT_SECTION_CHECK"],
	),
	$component
);?>
<?

if($arParams['PRODUCT_PROPERTY'] && $ElementID && $arParams['CATALOG_IBLOCK_ID']):?>
    <?
    $arParamsElm[$ElementID]['PROPERTIES'] = [];

    CIBlockElement::GetPropertyValuesArray(
        $arParamsElm,
        $arParams['IBLOCK_ID'],
        ['ID' => $ElementID],
        ['CODE' =>[$arParams['PRODUCT_PROPERTY'], 'PRODUCT_SECTION', 'PRODUCT_BRAND']]
    );
    
    //ws add
    $ar_section_elements_ids = [];

    if($arParamsElm[$ElementID]['PROPERTIES']['PRODUCT_BRAND']["VALUE_XML_ID"]) {
        $brand_ids = [];
        foreach ($arParamsElm[$ElementID]['PROPERTIES']['PRODUCT_BRAND']["VALUE_XML_ID"] as $brand_link_code) {
            $ar_brand_id = explode('_', $brand_link_code);
            $brand_ids[] = $ar_brand_id[2];
        }

        $arFilterBrand = [
            'IBLOCK_ID' => $arParams['CATALOG_IBLOCK_ID'],
            'PROPERTY_BRAND' => $brand_ids
        ];
        if (!empty($arParamsElm[$ElementID]['PROPERTIES']['PRODUCT_SECTION']["VALUE"])) {
            $arFilterBrand['SECTION_ID'] = $arParamsElm[$ElementID]['PROPERTIES']['PRODUCT_SECTION']["VALUE"];
        }
        $db_elements = CIBlockElement::GetList(
            [],
            $arFilterBrand,
            false,
            false,
            ['ID']
        );

        while($element = $db_elements->Fetch()) {
            $ar_section_elements_ids[$element['ID']] = $element['ID'];
        }
    }

    if($arParamsElm[$ElementID]['PROPERTIES']['PRODUCT_SECTION']["VALUE"]) {
        $arFilterSection = [
            'IBLOCK_ID' => $arParams['CATALOG_IBLOCK_ID'],
            'SECTION_ID' => $arParamsElm[$ElementID]['PROPERTIES']['PRODUCT_SECTION']["VALUE"]
        ];
        if (!empty($brand_ids)) {
            $arFilterSection['PROPERTY_BRAND'] = $brand_ids;
        }
        $db_elements = CIBlockElement::GetList(
            [],
            $arFilterSection,
        false,
        false,
            ['ID']
        );

        while($element = $db_elements->Fetch()) {
            $ar_section_elements_ids[$element['ID']] = $element['ID'];
        }
    }

    if(!is_array($arParamsElm[$ElementID]['PROPERTIES'][$arParams['PRODUCT_PROPERTY']]["VALUE"])) {
        $arParamsElm[$ElementID]['PROPERTIES'][$arParams['PRODUCT_PROPERTY']]["VALUE"] = [];
    }

    $ar_total_element_ids = array_merge($ar_section_elements_ids, $arParamsElm[$ElementID]['PROPERTIES'][$arParams['PRODUCT_PROPERTY']]["VALUE"]);
    $ar_total_element_ids = array_unique($ar_total_element_ids, SORT_NUMERIC);

    //ws end add
    if(!empty($ar_total_element_ids)):
    ?>
    <div class="brand-products">
        <div class="title_box">
            <h2><?=Loc::getMessage('TITLE_SALE_LIST');?></h2>
        </div>

        <?
        global $arFilterSales;
        $arFilterSales["ID"] = $ar_total_element_ids;
        
        $APPLICATION->IncludeComponent(
            "bitrix:catalog.section",
            "product-list",
            array(
                "ACTION_VARIABLE" => "action",
                "ADD_PICT_PROP" => "-",
                "ADD_PROPERTIES_TO_BASKET" => "Y",
                "ADD_SECTIONS_CHAIN" => "N",
                "ADD_TO_BASKET_ACTION" => "ADD",
                "AJAX_MODE" => "N",
                "AJAX_OPTION_ADDITIONAL" => "",
                "AJAX_OPTION_HISTORY" => "N",
                "AJAX_OPTION_JUMP" => "N",
                "AJAX_OPTION_STYLE" => "Y",
                "BACKGROUND_IMAGE" => "",
                "BASKET_URL" => SITE_DIR."basket/",
                "BRAND_PROPERTY" => "-",
                "BROWSER_TITLE" => "-",
                "CACHE_FILTER" => "N",
                "CACHE_GROUPS" => $arParams['CACHE_GROUPS'],
                "CACHE_TIME" => $arParams['CACHE_TIME'],
                "CACHE_TYPE" => $arParams['CACHE_TYPE'],
                "COMPATIBLE_MODE" => "",
                "CONVERT_CURRENCY" => "",
                "CURRENCY_ID" => "",
                "DATA_LAYER_NAME" => "",
                "DETAIL_URL" => SITE_DIR."catalog/#SECTION_CODE_PATH#/#ELEMENT_CODE#/",
                "DISABLE_INIT_JS_IN_COMPONENT" => "N",
                "DISCOUNT_PERCENT_POSITION" => "top-left",
                "DISPLAY_BOTTOM_PAGER" => "Y",
                "DISPLAY_TOP_PAGER" => "N",
                "ELEMENT_SORT_FIELD" => "",
                "ELEMENT_SORT_ORDER" => "",
                "ELEMENT_SORT_FIELD2" => "shows",
                "ELEMENT_SORT_ORDER2" => "asc",
                "ENLARGE_PRODUCT" => "STRICT",
                "ENLARGE_PROP" => "-",
                "FILTER_NAME" => "arFilterSales",
                "HIDE_NOT_AVAILABLE" => "Y",
                "HIDE_NOT_AVAILABLE_OFFERS" => "Y",
                "IBLOCK_ID" => $arParams['CATALOG_IBLOCK_ID'],
                "IBLOCK_TYPE" => "",
                "INCLUDE_SUBSECTIONS" => "Y",
                "LABEL_PROP" => array(
                    0 => "PRODUCT_HIT",
                    1 => "NEW",
                ),
                "LABEL_PROP_MOBILE" => "",
                "LABEL_PROP_POSITION" => "top-left",
                "LAZY_LOAD" => "N",
                "LINE_ELEMENT_COUNT" => "3",
                "LOAD_ON_SCROLL" => "N",
                "MESSAGE_404" => "",
                "META_KEYWORDS" => "-",
                "OFFERS_CART_PROPERTIES" => array(
                ),
                "OFFERS_FIELD_CODE" => array(
                    0 => "",
                    1 => "",
                ),
                "OFFERS_LIMIT" => "0",
                "OFFERS_PROPERTY_CODE" => $arParams['OFFERS_PROPERTY_CODE'],
                "OFFERS_SORT_FIELD" => "shows",
                "OFFERS_SORT_FIELD2" => "shows",
                "OFFERS_SORT_ORDER" => "asc",
                "OFFERS_SORT_ORDER2" => "asc",
                "OFFER_ADD_PICT_PROP" => "-",
                "OFFER_TREE_PROPS" => "",
                "PAGER_BASE_LINK_ENABLE" => "N",
                "PAGER_DESC_NUMBERING" => "N",
                "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
                "PAGER_SHOW_ALL" => "N",
                "PAGER_SHOW_ALWAYS" => "N",
                "PAGER_TEMPLATE" => "visual_1",
                "PAGER_TITLE" => $arParams["DETAIL_PAGER_TITLE"],
                "PAGE_ELEMENT_COUNT" => 24,
                "PARTIAL_PRODUCT_PROPERTIES" => "Y",
                "PRICE_CODE" => $arParams['PRICE_CODE'],
                "PRICE_VAT_INCLUDE" => "Y",
                "PRODUCT_BLOCKS_ORDER" => "price,props,sku,quantityLimit,quantity,buttons,compare",
                "PRODUCT_DISPLAY_MODE" => "N",
                "PRODUCT_ID_VARIABLE" => "id",
                "PRODUCT_PROPERTIES" => array(
                ),
                "PRODUCT_PROPS_VARIABLE" => "prop",
                "PRODUCT_QUANTITY_VARIABLE" => "",
                "PRODUCT_ROW_VARIANTS" => "[{'VARIANT':'3','BIG_DATA':false},{'VARIANT':'3','BIG_DATA':false},{'VARIANT':'3','BIG_DATA':false},{'VARIANT':'3','BIG_DATA':false},{'VARIANT':'3','BIG_DATA':false}]",
                "PRODUCT_SUBSCRIPTION" => "Y",
                "PROPERTY_CODE" => $arParams['CATALOG_PROPPERTY'],
                "PROPERTY_CODE_MOBILE" => array(
                ),
                "RCM_PROD_ID" => '',
                "RCM_TYPE" => "personal",
                "SECTION_CODE" => "",
                "SECTION_ID" => "",
                "SECTION_ID_VARIABLE" => "SECTION_ID",
                "SECTION_URL" => SITE_DIR."catalog/#SECTION_CODE_PATH#/",
                "SECTION_USER_FIELDS" => array(
                    0 => "",
                    1 => "",
                ),
                "SEF_MODE" => "N",
                "SET_BROWSER_TITLE" => "N",
                "SET_LAST_MODIFIED" => "N",
                "SET_META_DESCRIPTION" => "N",
                "SET_META_KEYWORDS" => "Y",
                "SET_STATUS_404" => "N",
                "SET_TITLE" => "N",
                "SHOW_404" => "N",
                "SHOW_ALL_WO_SECTION" => "Y",
                "SHOW_CLOSE_POPUP" => "Y",
                "SHOW_DISCOUNT_PERCENT" => "Y",
                "SHOW_FROM_SECTION" => "N",
                "SHOW_MAX_QUANTITY" => "N",
                "SHOW_OLD_PRICE" => "Y",
                "SHOW_PRICE_COUNT" => "1",
                "SHOW_SLIDER" => "N",
                "SLIDER_INTERVAL" => "3000",
                "SLIDER_PROGRESS" => "N",
                "TEMPLATE_THEME" => "blue",
                "USE_ENHANCED_ECOMMERCE" => "Y",
                "USE_MAIN_ELEMENT_SECTION" => "N",
                "USE_PRICE_COUNT" => "N",
                "USE_PRODUCT_QUANTITY" => "N",
                "DISPLAY_COMPARE" => "N",
                "SEF_RULE" => "",
                "SECTION_CODE_PATH" => "",
                "COMPONENT_TEMPLATE" => "product-list",
            ),
            $component
        );
        ?>
    </div>
    <?endif;?>
<?endif;?>
</div>