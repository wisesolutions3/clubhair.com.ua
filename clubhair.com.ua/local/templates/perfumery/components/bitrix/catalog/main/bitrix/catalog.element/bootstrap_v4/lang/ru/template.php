<?
$MESS["STORES_BUTTON"] = "Наличие в магазинах";
$MESS["CT_BCE_QUANTITY"] = "Количество";
$MESS["CT_BCE_CATALOG_BUY"] = "Купить";
$MESS["CT_BCE_CATALOG_ADD"] = "Купить";
$MESS["CT_BCE_CATALOG_COMPARE"] = "Сравнить";
$MESS["CT_BCE_CATALOG_NOT_AVAILABLE"] = "нет на складе";
$MESS["CT_BCE_CATALOG_ECONOMY_INFO2"] = "Скидка #ECONOMY#";
$MESS["CT_BCE_CATALOG_DESCRIPTION"] = "Описание";
$MESS["CT_BCE_CATALOG_PROPERTIES"] = "Характеристики";
$MESS["CT_BCE_CATALOG_COMMENTARY"] = "Комментарии";
$MESS["CATALOG_QUANTITY"] = "Количество";
$MESS["CT_BCE_CATALOG_TITLE_ERROR"] = "Ошибка";
$MESS["CT_BCE_CATALOG_TITLE_BASKET_PROPS"] = "Свойства товара, добавляемые в корзину";
$MESS["CT_BCE_CATALOG_BASKET_UNKNOWN_ERROR"] = "Неизвестная ошибка при добавлении товара в корзину";
$MESS["CT_BCE_CATALOG_BTN_SEND_PROPS"] = "Выбрать";
$MESS["CT_BCE_CATALOG_BTN_MESSAGE_CLOSE"] = "Закрыть";
$MESS["CT_BCE_CATALOG_BTN_MESSAGE_CLOSE_POPUP"] = "Продолжить покупки";
$MESS["CT_BCE_CATALOG_BTN_MESSAGE_BASKET_REDIRECT"] = "Перейти в корзину";
$MESS["CT_BCE_CATALOG_ADD_TO_BASKET_OK"] = "Товар добавлен в корзину";
$MESS["CT_BCE_CATALOG_MESS_COMPARE_OK"] = "Товар добавлен в список сравнения";
$MESS["CT_BCE_CATALOG_MESS_COMPARE_TITLE"] = "Сравнение товаров";
$MESS["CT_BCE_CATALOG_MESS_COMPARE_UNKNOWN_ERROR"] = "При добавлении товара в список сравнения произошла ошибка";
$MESS["CT_BCE_CATALOG_BTN_MESSAGE_COMPARE_REDIRECT"] = "Перейти в список сравнения";
$MESS["CT_BCE_CATALOG_PRODUCT_GIFT_LABEL"] = "Подарок";
$MESS["CT_BCE_CATALOG_MESS_PRICE_TOTAL_PREFIX"] = "на сумму";
$MESS["CT_BCE_CATALOG_PRICE_RANGES_TITLE"] = "Цены";
$MESS["CT_BCE_CATALOG_DESCRIPTION_TAB"] = "Описание";
$MESS["CT_BCE_CATALOG_PROPERTIES_TAB"] = "Характеристики";
$MESS["CT_BCE_CATALOG_COMMENTS_TAB"] = "Комментарии";
$MESS["CT_BCE_CATALOG_SHOW_MAX_QUANTITY"] = "Наличие";
$MESS["CT_BCE_CATALOG_RELATIVE_QUANTITY_MANY"] = "много";
$MESS["CT_BCE_CATALOG_RELATIVE_QUANTITY_FEW"] = "мало";
$MESS["CT_BCE_CATALOG_RANGE_FROM"] = "от #FROM#";
$MESS["CT_BCE_CATALOG_RANGE_TO"] = "до #TO#";
$MESS["CT_BCE_CATALOG_RANGE_MORE"] = "и более";
$MESS["CT_BCE_CATALOG_RATIO_PRICE"] = "цена за #RATIO#";
$MESS["CT_BCE_CATALOG_GIFT_BLOCK_TITLE_DEFAULT"] = "Выберите один из подарков к этому товару";
$MESS["CT_BCE_CATALOG_GIFTS_MAIN_BLOCK_TITLE_DEFAULT"] = "Выберите один из товаров, чтобы получить подарок";

$MESS['K_NOT_REVIEW'] = "Отзывов пока нет";
$MESS['K_REW_FORM_FORM_TITLE'] = "Написать отзыв о товаре";
$MESS['FORM_MESS_ADD_REVIEW_ERROR'] = "Внимание!<br>Ошибка добавления отзыва";
$MESS['FORM_MESS_ADD_REVIEW_EVENT_THEME'] = "<p>#USER_NAME# добавил(а) новый отзыв (оценка: #RATING#) ##ID#</p>
    <p>Открыть в админке #LINK_ADMIN#</p>
    <p>Открыть на сайте #LINK#</p>";
$MESS['FORM_MESS_ADD_REVIEW_MODERATION'] = "Спасибо!<br>Ваш отзыв отправлен на модерацию";
$MESS['FORM_MESS_ADD_REVIEW_EVENT_THEME'] = "Отзыв о товаре в вашем магазине (оценка: #RATING#) ##ID#";
$MESS['FORM_MESS_ADD_REVIEW_VIZIBLE'] = "Спасибо!<br>Ваш отзыв №#ID# добавлен и опубликован";
$MESS['FORM_MESS_EULA'] = "Нажимая кнопку «Отправить отзыв», я принимаю условия Пользовательского соглашения и даю своё согласие на обработку моих персональных данных, в соответствии с Федеральным законом от 27.07.2006 года №152-ФЗ «О персональных данных», на условиях и для целей, определенных Политикой конфиденциальности.";
$MESS['FORM_MESS_EULA_CONFIRM'] = "Для продолжения вы должны принять условия Пользовательского соглашения";
$MESS['FORM_MESS_PRIVACY'] = "Я согласен на обработку персональных данных";
$MESS['FORM_MESS_PRIVACY_CONFIRM'] = "Для продолжения вы должны принять соглашение на обработку персональных данных";
$MESS['FORM_MESS_STAR_RATING_1'] = "Ужасный товар";
$MESS['FORM_MESS_STAR_RATING_2'] = "Плохой товар";
$MESS['FORM_MESS_STAR_RATING_3'] = "Обычный товар";
$MESS['FORM_MESS_STAR_RATING_4'] = "Хороший товар";
$MESS['FORM_MESS_STAR_RATING_5'] = "Отличный товар";
$MESS['FORM_RULES_TEXT'] = "Правила публикации отзывов";
$MESS['FORM_SHOP_BTN_TEXT'] = "Добавить отзыв";
$MESS['LIST_MESS_ADD_UNSWER_EVENT_TEXT'] = "#USER_NAME#, здравствуйте!
    К Вашему отзыву добавлен официальный ответ, для просмотра перейдите по ссылке #LINK#";
$MESS['LIST_MESS_ADD_UNSWER_EVENT_THEME'] = "Официальный ответ к вашему отзыву";
$MESS['LIST_MESS_HELPFUL_REVIEW'] = "Отзыв полезен?";
$MESS['LIST_MESS_TRUE_BUYER'] = "Проверенный покупатель";
$MESS['LIST_SHOP_NAME_REPLY'] = "Интернет-магазин";
$MESS['STAT_MESS_CUSTOMER_RATING'] = "На основе #N# оценок покупателей";
$MESS['STAT_MESS_CUSTOMER_REVIEWS'] = "Отзывы покупателей <span class=\"api-reviews-count\"></span>";
$MESS['STAT_MESS_TOTAL_RATING'] = "Рейтинг покупателей";
$MESS['MESS_FIELD_NAME_RATING'] = "Ваша оценка";
$MESS['MESS_FIELD_NAME_TITLE'] = "Заголовок отзыва";
$MESS['MESS_FIELD_NAME_ANNOTATION'] = "Текст отзыва";
$MESS['MESS_FIELD_NAME_GUEST_NAME'] = "Ваше имя";

$MESS['K_MORE_PRODUCT_2'] = "Вам также может понравится";

$MESS['K_MORE_BUY_TITLE'] = "C этим товаром покупают";
$MESS["K_SIMILAR_TITLE"] = "Похожие товары";
$MESS["NOT_SELECTED"] = 'Не выбрано';
?>