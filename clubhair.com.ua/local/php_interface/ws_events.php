<?
$eventManager = \Bitrix\Main\EventManager::getInstance();

//sale
$eventManager->addEventHandler('sale', 'OnSaleOrderBeforeSaved', ['CSaleHandlers', 'OnSaleOrderBeforeSaved']);
$eventManager->addEventHandler('sale', 'OnSaleStatusOrderChange', ['CSaleHandlers', 'OnSaleStatusOrderChangeHandler']);
//

//main
$eventManager->addEventHandler('main', 'OnProlog', ['CMainHandlers', 'OnProlog']);
//

//search
$eventManager->addEventHandler('search', 'BeforeIndex', ['CSearchHandler', 'BeforeIndexHandler']);
//
