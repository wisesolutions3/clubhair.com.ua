<? if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();

/**
 * @var CBitrixComponentTemplate $this
 * @var CatalogElementComponent $component
 */
foreach ($arResult["OFFERS"] as $key => &$OFFER){
    if(!empty($OFFER['PROPERTIES']['TON']['VALUE_ENUM_ID'])) {
        if(!empty($OFFER['DETAIL_PICTURE'])) {
            $ar_offer_ton_img[$OFFER['PROPERTIES']['TON']['VALUE_ENUM_ID']] = $OFFER['DETAIL_PICTURE'];
        }

        if(!empty($OFFER['PROPERTIES']['CML2_ARTICLE']['VALUE'])) {
            $ar_offer_ton_articles[$OFFER['PROPERTIES']['TON']['VALUE_ENUM_ID']] = $OFFER['PROPERTIES']['CML2_ARTICLE']['VALUE'];
        }
    }

    if($OFFER["DETAIL_TEXT"]=="" && $arResult["DETAIL_TEXT"]!=""){
        $OFFER["DETAIL_TEXT"]=$arResult["DETAIL_TEXT"];
    }
    foreach ($arResult["DISPLAY_PROPERTIES"] as $code => $PROPERTY){
        if($OFFER["DISPLAY_PROPERTIES"][$code]["DISPLAY_VALUE"]=="" && $PROPERTY["DISPLAY_VALUE"]!=''){
            $OFFER["DISPLAY_PROPERTIES"][$code]=$PROPERTY;
        }
    }
    
}

$arResult['OFFERS_TON_IMG'] = $ar_offer_ton_img;
$arResult['OFFERS_TON_ARTICLE'] = $ar_offer_ton_articles;

$component = $this->getComponent();
$arParams = $component->applyTemplateModifications();

$ton_prop_code = LANGUAGE_ID == 'ua' ? 'PROP_74' : 'PROP_75';

$ar_ton_values = [];
foreach ($arResult["JS_OFFERS"] as &$JSOFFER) {
    if(isset($JSOFFER['TREE'][$ton_prop_code])) {
        foreach ($JSOFFER['TREE'] as $prop_code => $prop_value_id) {
            if($prop_code != $ton_prop_code) {
                $ar_ton_values[$prop_value_id][] = $JSOFFER['TREE'][$ton_prop_code];
            }
        }
    }
    foreach ($arResult["OFFERS"] as $OFFER) {
        if($JSOFFER["ID"]==$OFFER["ID"]){
            $JSOFFER["DETAIL_TEXT"]=$OFFER["DETAIL_TEXT"];
        }
    }
}

if(!empty($ar_ton_values)) {
    $arResult['OFFERS_TON_VALUES_CUSTOM'] = $ar_ton_values;
}
