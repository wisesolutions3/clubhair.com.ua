<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();                        
if(\Bitrix\Main\Loader::includeSharewareModule("krayt.perfumery") == \Bitrix\Main\Loader::MODULE_DEMO_EXPIRED || 
   \Bitrix\Main\Loader::includeSharewareModule("krayt.perfumery") ==  \Bitrix\Main\Loader::MODULE_NOT_FOUND
    )
{ return false;}
function parsePhone ($phone){
    $phone = str_replace("-","", $phone);
    $phone = str_replace(" ","", $phone);
    $phone = str_replace("(","", $phone);
    $phone = str_replace(")","", $phone);
    return $phone;
}

function getStringCount($count){
    if ($count ==0){
        return GetMessage('ABSENT_T');
    }else if($count <= 10){
        return GetMessage('NOT_MUCH_GOOD_T');
    }else{
        return GetMessage('LOT_OF_GOOD_T');
    }
}

$dbResult = CCatalogStore::GetList(
    array('ID' => "ASC"),
    array('ACTIVE' => 'Y'),
    false,
    false,
    array("ID", "TITLE", "ADDRESS", "PHONE")
);
while ($ar = $dbResult->fetch()) {

    $arADDR[$ar['ID']]["ADDRESS"] = $ar["ADDRESS"];
    $arADDR[$ar['ID']]["TITLE"] = $ar["TITLE"];
    $arADDR[$ar['ID']]["PHONE"] = $ar["PHONE"];
    $arADDR[$ar['ID']]["AMOUNT"] = $ar["AMOUNT"];
}
foreach ($arResult["STORES"] as &$STORE) {
    $STORE['TITLE'] = $arADDR[$STORE['ID']]["TITLE"];
    $STORE['ADDRESS'] = $arADDR[$STORE['ID']]["ADDRESS"];
    $STORE['PHONE_URL'] = parsePhone($arADDR[$STORE['ID']]["PHONE"]);
    $STORE['AMOUNT'] = getStringCount($STORE['REAL_AMOUNT']);
}
/**/
$arResult['JS']['MESSAGES']["ABSENT"] = GetMessage('ABSENT_T');
$arResult['JS']['MESSAGES']["NOT_MUCH_GOOD"] = GetMessage('NOT_MUCH_GOOD_T');
$arResult['JS']['MESSAGES']["LOT_OF_GOOD"] = GetMessage('LOT_OF_GOOD_T');

//PR($arResult);