<?php
use Bitrix\Main\Page\Asset;
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();


require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_admin_before.php");

global $APPLICATION;


CUtil::JSPostUnescape();
$APPLICATION->ShowAjaxHead();

/** @var array $arResult */

$APPLICATION->IncludeComponent(
    'bitrix:catalog.product.search',
    '',
    array(
        'IBLOCK_ID' => $arResult['CATALOG_ID'],
        'CHECK_PERMISSIONS' => 'N'
    ),
    false,
    array('HIDE_ICONS'=>true)
);
Asset::getInstance()->addJs("/bitrix/js/main/core/core_admin_interface.js");
Asset::getInstance()->addJs("/bitrix/js/main/hot_keys.js");
Asset::getInstance()->addJs("/bitrix/js/main/popup_menu.js?160683208612913");
Asset::getInstance()->addCss("/bitrix/css/main/hot_keys.css");
?>
    <script>
        BX.Catalog.ProductSearchDialog.prototype.SelEl= function(arParams, scope){
            BX.onCustomEvent("CrmProductSearchDialog_SelectProduct", [arParams.id]);
        }
        
    </script>
<?

CJSCore::Init(["popup"]);
require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/epilog_admin_after.php");