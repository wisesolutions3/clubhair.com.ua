<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();                        
if(\Bitrix\Main\Loader::includeSharewareModule("krayt.perfumery") == \Bitrix\Main\Loader::MODULE_DEMO_EXPIRED || 
   \Bitrix\Main\Loader::includeSharewareModule("krayt.perfumery") ==  \Bitrix\Main\Loader::MODULE_NOT_FOUND
    )
{ return false;}
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>

<div class="sales_container">

            <div class="sales__wrapper">
                <?if($arResult['left']):?>
                    <div class="row-cell-left">
                        <div class="sales__item first"  id="<?=$this->GetEditAreaId($arResult['left']['ID']);?>">
                            <div class="sales_img_wrapper">
                                <?if($arParams['LOAD_IMG_JS'] == 'Y'):?>
                                    <a href="<?=$arResult['left']["DETAIL_PAGE_URL"]?>" class="sales_link">
                                        <div class="sales_img LOAD_IMG_JS_STOCK"
                                             data-src = "<?=$arResult['left']["PREVIEW_PICTURE"]["SRC"]?>"
                                             style="background-image:url('<?=$templateFolder?>/images/fon.svg');"
                                             title="<?=$arResult['left']["PREVIEW_PICTURE"]["TITLE"]?>"></div>
                                    </a>
                                    <?else:?>
                                    <a href="<?=$arResult['left']["DETAIL_PAGE_URL"]?>" class="sales_link">
                                        <div class="sales_img"
                                             style="background-image:url(<?=$arResult['left']["PREVIEW_PICTURE"]["SRC"]?>);"
                                             title="<?=$arResult['left']["PREVIEW_PICTURE"]["TITLE"]?>"></div>
                                    </a>
                                <?endif;?>
                            </div>
                        </div>
                    </div>
                <?endif;?>

                <div class="row-cell-right">
                    <?if($arResult['rigth1']):?>
                        <div class="sales__item col-top"  id="<?=$this->GetEditAreaId($arResult['rigth1']['ID']);?>">
                        <div class="sales_img_wrapper">
                            <?if($arParams['LOAD_IMG_JS'] == 'Y'):?>
                                <a href="<?=$arResult['rigth1']["DETAIL_PAGE_URL"]?>" class="sales_link">
                                    <div class="sales_img LOAD_IMG_JS_STOCK"
                                         data-src = "<?=$arResult['rigth1']["PREVIEW_PICTURE"]["SRC"]?>"
                                         style="background-image:url('<?=$templateFolder?>/images/fon.svg');"
                                         title="<?=$arResult['rigth1']["PREVIEW_PICTURE"]["TITLE"]?>"></div>
                                </a>
                            <?else:?>
                                <a href="<?=$arResult['rigth1']["DETAIL_PAGE_URL"]?>" class="sales_link">
                                    <div class="sales_img"
                                         style="background-image:url(<?=$arResult['rigth1']["PREVIEW_PICTURE"]["SRC"]?>);"
                                         title="<?=$arResult['rigth1']["PREVIEW_PICTURE"]["TITLE"]?>"></div>
                                </a>
                            <?endif;?>
                        </div>
                    </div>
                    <?endif;?>
                    <?if($arResult['rigth2']):?>
                    <div class="sales__item col-bottom"  id="<?=$this->GetEditAreaId($arResult['rigth2']['ID']);?>">
                        <div class="sales_img_wrapper">
                            <?if($arParams['LOAD_IMG_JS'] == 'Y'):?>
                                <a href="<?=$arResult['rigth2']["DETAIL_PAGE_URL"]?>" class="sales_link">
                                    <div class="sales_img LOAD_IMG_JS_STOCK"
                                         data-src = "<?=$arResult['rigth2']["PREVIEW_PICTURE"]["SRC"]?>"
                                         style="background-image:url('<?=$templateFolder?>/images/fon.svg');"
                                         title="<?=$arResult['rigth2']["PREVIEW_PICTURE"]["TITLE"]?>"></div>
                                </a>
                            <?else:?>
                                <a href="<?=$arResult['rigth2']["DETAIL_PAGE_URL"]?>" class="sales_link">
                                    <div class="sales_img"
                                         style="background-image:url(<?=$arResult['rigth2']["PREVIEW_PICTURE"]["SRC"]?>);"
                                         title="<?=$arResult['rigth2']["PREVIEW_PICTURE"]["TITLE"]?>"></div>
                                </a>
                            <?endif;?>
                        </div>
                    </div>
                    <?endif;?>
                </div>
            </div>
</div>
