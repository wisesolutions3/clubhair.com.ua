<?
$MESS["TP_BND_MEDIA_PROPERTY"] = "Властивість для відображення медіа";
$MESS["TP_BND_SLIDER_PROPERTY"] = "Властивість із зображеннями для слайдера";
$MESS["TP_BND_TEMPLATE_THEME"] = "Колірна тема";
$MESS["TP_BND_THEME_BLACK"] = "темна";
$MESS["TP_BND_THEME_BLUE"] = "синя (тема за замовчуванням)";
$MESS["TP_BND_THEME_GREEN"] = "зелена";
$MESS["TP_BND_THEME_RED"] = "червона";
$MESS["TP_BND_THEME_SITE"] = "Брати тему з налаштувань сайту (для вирішення bitrix.eshop)";
$MESS["TP_BND_THEME_WOOD"] = "дерево";
$MESS["TP_BND_THEME_YELLOW"] = "жовта";
$MESS["T_IBLOCK_DESC_NEWS_DATE"] = "Виводити дату елемента";
$MESS["T_IBLOCK_DESC_NEWS_NAME"] = "Виводити назву елемента";
$MESS["T_IBLOCK_DESC_NEWS_PICTURE"] = "Виводити детальне зображення";
$MESS["T_IBLOCK_DESC_NEWS_SHARE_HIDE"] = "Чи не розкривати панель соц. закладок за замовчуванням";
$MESS["T_IBLOCK_DESC_NEWS_SHARE_SHORTEN_URL_KEY"] = "Ключ для для bit.ly";
$MESS["T_IBLOCK_DESC_NEWS_SHARE_SHORTEN_URL_LOGIN"] = "Логін для bit.ly";
$MESS["T_IBLOCK_DESC_NEWS_SHARE_SYSTEM"] = "Використовувані соц. закладки та мережі";
$MESS["T_IBLOCK_DESC_NEWS_SHARE_TEMPLATE"] = "Шаблон компонента панелі соц. закладок";
$MESS["T_IBLOCK_DESC_NEWS_TEXT"] = "Виводити текст анонса";
$MESS["T_IBLOCK_DESC_NEWS_USE_SHARE"] = "Відображати панель соц. закладок";
?>