<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();                        
if(\Bitrix\Main\Loader::includeSharewareModule("krayt.perfumery") == \Bitrix\Main\Loader::MODULE_DEMO_EXPIRED || 
   \Bitrix\Main\Loader::includeSharewareModule("krayt.perfumery") ==  \Bitrix\Main\Loader::MODULE_NOT_FOUND
    )
{ return false;}
function parsePhone ($phone){
    $phone = str_replace("-","", $phone);
    $phone = str_replace(" ","", $phone);
    $phone = str_replace("(","", $phone);
    $phone = str_replace(")","", $phone);
    return $phone;
}

$dbResult = CCatalogStore::GetList(
    array('ID' => "ASC"),
    array('ACTIVE' => 'Y'),
    false,
    false,
    array("ID", "TITLE", "ADDRESS", "PHONE")
);
while ($ar = $dbResult->fetch()) {

    $arADDR[$ar['ID']]["ADDRESS"] = $ar["ADDRESS"];
    $arADDR[$ar['ID']]["TITLE"] = $ar["TITLE"];
    $arADDR[$ar['ID']]["PHONE"] = $ar["PHONE"];
    $arADDR[$ar['ID']]["AMOUNT"] = $ar["AMOUNT"];
}
foreach ($arResult["STORES"] as &$STORE) {
    $STORE['TITLE'] = $arADDR[$STORE['ID']]["TITLE"];
    $STORE['ADDRESS'] = $arADDR[$STORE['ID']]["ADDRESS"];
    $STORE['PHONE_URL'] = parsePhone($arADDR[$STORE['ID']]["PHONE"]);
}

//PR($arResult);