<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();                        
if(\Bitrix\Main\Loader::includeSharewareModule("krayt.perfumery") == \Bitrix\Main\Loader::MODULE_DEMO_EXPIRED || 
   \Bitrix\Main\Loader::includeSharewareModule("krayt.perfumery") ==  \Bitrix\Main\Loader::MODULE_NOT_FOUND
    )
{ return false;}
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
if(strlen($arResult["ERROR_MESSAGE"])>0)
	ShowError($arResult["ERROR_MESSAGE"]);
$arPlacemarks = array();
$gpsN = '';
$gpsS = '';
?>
<? if(is_array($arResult["STORES"]) && !empty($arResult["STORES"])){?>

<div class="map_wrapper mt-3">
		<?
		if ($arResult['VIEW_MAP'])
		{
            $gpsN = $arResult['STORES'][0]['GPS_N'];
            $gpsS = $arResult['STORES'][0]['GPS_S'];

            $arPlacemarks =array("TEXT"=>$arResult['STORES'][0]['TITLE'], "LON"=>$gpsS,"LAT"=>$gpsN);

            $APPLICATION->IncludeComponent("bitrix:map.google.view", ".default", array(
                "API_KEY" => 'AIzaSyDcibCB5xnBBm7znv2iK5ktOX_cFbtT5S8',
                "INIT_MAP_TYPE" => "MAP",
                "MAP_DATA" => serialize(array("google_lat"=>$gpsN,"google_lon"=>$gpsS,"google_scale"=>10,"PLACEMARKS" => array($arPlacemarks))),
                "MAP_WIDTH" => "100%",
                "MAP_HEIGHT" => "500",
                "CONTROLS" => array(
                    0 => "ZOOM",
                ),
                "OPTIONS" => array(
                    0 => "ENABLE_SCROLL_ZOOM",
                    1 => "ENABLE_DBLCLICK_ZOOM",
                    2 => "ENABLE_DRAGGING",
                ),
                "MAP_ID" => "1"
            ),
                $component,
                array("HIDE_ICONS" => "Y")
            );
		}
		?>
</div>

<?}?>