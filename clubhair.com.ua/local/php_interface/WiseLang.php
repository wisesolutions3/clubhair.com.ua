<?
CModule::IncludeModule('iblock');
global $LANG_PARAMS;
$LANG_PARAMS['DEFAULT_LANG'] = 'ua';


class WsLanguage
{
    public static $arPropertyUseOfAllLanguage = array('MORE_PHOTO','PRODUCT', 'SHOW_MAIN','PRODUCT_HIT', 'NEW',
        'SIMILAR', 'RECOMEND_PRODUCT', 'PRODUCT_SECTION', 'PRODUCT_BRAND');

    public static function GetPropertyPrefix()
    {
        global $LANG_PROPERTY_PREFIX;
        $LANG_PROPERTY_PREFIX = false;
        switch (strtoupper(LANGUAGE_ID)) {
            case 'RU':
                $LANG_PROPERTY_PREFIX = 'RU_';
                break;
        }
        return $LANG_PROPERTY_PREFIX;
    }

    public function GetListProperties($cache_time = '36000')
    {
        
        global $properties_catalog_ws;
        if (empty($properties_catalog_ws)) {
            CModule::IncludeModule('iblock');

            $obCache = new CPHPCache;
            $cache_id = 'props_data';

            if ($obCache->InitCache($cache_time, $cache_id, '/cache_arr_list/')) {
                $vars = $obCache->GetVars();
                $arPropertiesFun = $vars['arPropertiesFun'];
                $arPropertiesCodeFun = $vars['arPropertiesCodeFun'];

                $obCache->Output();
            } elseif ($obCache->StartDataCache()) {

                $res = CIBlockProperty::GetList(
                    array(),
                    array("IBLOCK_ID" => CATALOG_IBLOCK_ID)
                );

                while ($arPropFun = $res->GetNext()) {
//                    $arPropertiesFun[$arPropFun["XML_ID"]] = $arPropFun;
                    $keyFirst = $arPropFun['XML_ID'] ? $arPropFun['XML_ID'] : $arPropFun['ID'];
                    $arPropertiesFun[$keyFirst] = $arPropFun;
                    $arPropertiesCodeFun[$arPropFun["CODE"]] = $arPropFun;
                }
                $obCache->EndDataCache(array("arPropertiesFun" => $arPropertiesFun, 'arPropertiesCodeFun' => $arPropertiesCodeFun));
            }

            $properties_catalog_ws = array($arPropertiesFun, $arPropertiesCodeFun);
        }

        return $properties_catalog_ws;
    }


    public function GetParrentPropertyName($arProperty)
    {

        
        $properties = self::GetListProperties();

        if (empty($arProperty["XML_ID"])) {
            return $arProperty["NAME"];
        }

        $ruXmlId = substr($arProperty['XML_ID'], 0, 36);


        $ruProp = $properties[0][$ruXmlId];

        if ($ruProp) {

            $name = str_replace("RU_", "", $arProperty["NAME"]);
            $name = str_replace($ruProp["NAME"], "", $name);
            $name = trim($name);

            $arProperty["CODE"] = "RU_" . $ruProp["CODE"];
            $arProperty["~NAME"] = $arProperty["NAME"] = $name;

        }

        return $arProperty;

    }


    public function getFilterLanguage($language = 'ua')
    {
        $filter = array("LOGIC" => "OR");
        $filter[] = array("PROPERTY_LANGUAGE_VALUE" => $language);
        $filter[] = array("PROPERTY_LANGUAGE" => false);

        return $filter;
    }

    function GetCorrectFieldName($type, $field_name, $properties = array())
    {
        global $LANG_PARAMS;
        if ($LANG_PARAMS['DEFAULT_LANG'] != LANGUAGE_ID && strpos($_SERVER['REQUEST_URI'], '/admin/') <= 0) {

            $prefix = strtoupper(LANGUAGE_ID) . '_';
            switch ($type) {
                case 'section':
                    $add_prefix = 'UF_';
                    $field_name = str_replace($add_prefix, '', $field_name);
                    $res = array(
                        $field_name => $add_prefix . $prefix . $field_name,
                        "UF_" . $field_name => $add_prefix . $prefix . $field_name
                    );

                    break;
                case 'seo':
                    if (strpos("AAA" . $field_name, "this.catalog") !== false) {
                        $res = $field_name;
                    } else {

                        switch ($field_name) {
                            case "this.Name":
                                $res = "this.property.ru_nazvanie";
                                break;

                            case "this.DetailText":
                                $res = "this.property.ru_opisanie";
                                break;

                            default:
                                $ruProp = '';

                                if (strpos("AAA" . $field_name, 'this.property.') !== false && strpos("AAA" . $field_name, 'this.property.ru_') !== true) {

                                    $code = str_replace("this.property.", "", $field_name);

                                    $ruProp = $properties[1][$code];

                                    if ($ruProp) {
                                        $ruXmlId = $ruProp["XML_ID"] . "-2";
                                        $ruProp = $properties[0][$ruXmlId];
                                    }

                                }

                                if ($ruProp) {
                                    $res = strtolower("this.property." . $ruProp["CODE"]);

                                } else {

                                    //old
                                    $to_remove = 'this.';

                                    $add_prefix = strtolower($to_remove . 'property.' . $prefix);
                                    $rest = strtolower(str_replace($to_remove, $add_prefix, $field_name));

                                    if (strpos('test' . $field_name, $add_prefix) <= 0) {
                                        $res = strtolower(str_replace($to_remove, $add_prefix, $field_name));
                                    } else {
                                        $res = $field_name;
                                    }
                                }
                                break;
                        }

                    }
                    break;
                default:

                    $ruProp = '';

                    $ruProp = $properties[1][$field_name];
                    if ($ruProp) {
                        $ruXmlId = $ruProp["XML_ID"] . "-2";
                        $ruProp = $properties[0][$ruXmlId];
                    }


                    $add_prefix = 'PROPERTY_';

                    if ($ruProp) {
                        $res = array(
                            $field_name => $add_prefix . $ruProp["CODE"]
                        );
                    } else {

                        if ($field_name == "NAME") {
                            $res = array(
                                $field_name => "PROPERTY_RU_NAZVANIE"
                            );
                        } elseif ($field_name == "DETAIL_TEXT") {
                            $res = array(
                                $field_name => "PROPERTY_RU_OPISANIE"
                            );

                        } elseif ($field_name == "PREVIEW_TEXT") {
                            $res = array(
                                $field_name => "PROPERTY_RU_OPISANIE_ANONS"
                            );

                        } elseif($field_name == 'DETAIL_PICTURE' || $field_name == 'PREVIEW_PICTURE') {
                            if ($field_name == 'DETAIL_PICTURE') {
                                $res = array(
                                    $field_name => "PROPERTY_RU_DETAIL_PICTURE"
                                );
                            }

                            if ($field_name == 'PREVIEW_PICTURE') {
                                $res = array(
                                    $field_name => "PROPERTY_RU_PREVIEW_PICTURE"
                                );
                            }
                        } else {
                            $field_name = str_replace($add_prefix, '', $field_name);

                            $res = array(
                                $field_name => $add_prefix . $prefix . $field_name
                            );
                        }

                    }
                    break;
            }
          
            return $res;
        }
        
        
        return $res;
    }

    function CorrectDataResult($type, $res, $name = false)
    {
        global $LANG_PARAMS;
        $oldRes = $res;

        $properties = self::GetListProperties();

        if ($LANG_PARAMS['DEFAULT_LANG'] != LANGUAGE_ID && strpos($_SERVER['REQUEST_URI'], '/admin/') <= 0) {
            while ($item = $res->Fetch()) {

                foreach ($item as $key => $value) {
                    $new_replace_field = WsLanguage::GetCorrectFieldName($type, $key, $properties);
                    //echo "<pre>";print_r($new_replace_field);echo "</pre>";
                    switch ($type) {
                        case 'section':

                            $new_value = $item[$new_replace_field[$key]];

                            //fix breadcrumb
                            if ($name == true && empty($new_value) && $key == "NAME") {
                                $name = CIBlockSection::GetList(array(), array("ID" => $item["ID"], "IBLOCK_ID" => $item["IBLOCK_ID"]), false, array("ID", $new_replace_field[$key]))->Fetch();
                                if ($name[$new_replace_field[$key]] != "") {
                                    $item[$key] = $name[$new_replace_field[$key]];
                                }
                            }

                            if (!empty($item[$new_replace_field[$key]])) {
                                $item[$key] = $new_value;
                            }
                            break;
                        case 'element':

                            $new_value = $item[$new_replace_field[$key] . '_VALUE'];

                            if (array_key_exists('DETAIL_PICTURE',$new_replace_field)) {
                                if (!$new_value) {
                                    $new_replace_field['DETAIL_PICTURE'] = 'PROPERTY_DETAIL_PICTURE';
                                }
                            }

                            if (array_key_exists('PREVIEW_PICTURE',$new_replace_field)) {
                                if (!$new_value) {
                                    $new_replace_field['PREVIEW_PICTURE'] = 'PROPERTY_PREVIEW_PICTURE';
                                }
                            }



                            if (
                                strpos('aaa' . $new_replace_field[$key], 'PREVIEW_TEXT') > 0
                                || strpos('aaa' . $new_replace_field[$key], 'DETAIL_TEXT')) {
                                $new_value = html_entity_decode(unserialize($new_value)['TEXT']);
                            }
                            if ($new_replace_field['DETAIL_TEXT'] == 'PROPERTY_RU_OPISANIE' ||
                                $new_replace_field['PREVIEW_TEXT'] == 'PROPERTY_RU_OPISANIE_ANONS') {
                                $arDetailText = @unserialize($new_value);
                                if (is_array($arDetailText)) {
                                    $new_value = html_entity_decode($arDetailText['TEXT']);
                                }
                            }


                            if (!empty($new_value)) {
                                $item[$key] = $new_value;
                            }




                            break;
                        default:
                            $new_value = $item[$new_replace_field[$key] . '_VALUE'];
                            if (!empty($new_value)) {
                                $item[$key] = $new_value;
                            }
                            break;
                    }
                    unset($item[$new_replace_field[$key]]);
                }
                $res_formated[] = $item;
            }

            $res = new CDBResult;
            $res->InitFromArray($res_formated);
        }
        $res->result = $oldRes->result;
        $res->NavPageCount = $oldRes->NavPageCount;
        $res->NavPageNomer = $oldRes->NavPageNomer;
        $res->NavPageSize = $oldRes->NavPageSize;
        $res->NavRecordCount = $oldRes->NavRecordCount;
        $res->nPageWindow = $oldRes->nPageWindow;
        $res->nSelectedCount = $oldRes->nSelectedCount;
        $res->nStartPage = $oldRes->nStartPage;
        $res->nEndPage = $oldRes->nEndPage;
        $res->PAGEN = $oldRes->PAGEN;
        $res->NavNum = $oldRes->NavNum;

        return $res;
    }


    function correctProperties(& $result)
    {

        if (LANGUAGE_ID != 'ru') {
            foreach ($result as $keyProp => $valueProp) {
                if (strpos($valueProp['CODE'], PREFIX_UA) === 0 && LANGUAGE_ID == 'ua') {
                    $result[substr($valueProp['CODE'], 3)] = $valueProp;
                    unset($result[$keyProp]);
                }
            }
        }
    }

    function GetPropertyValuesArray(&$result)
    {
        $allProperties = self::GetListProperties();
        switch (strtolower(LANGUAGE_ID)) {
            case 'ua':
                foreach ($result as $keyElement => $arElement) {
                    foreach ($arElement['PROPERTIES'] as $keyProp => $arProperty) {
                        if (stristr($arProperty['CODE'], 'RU_')) {
                            unset($result[$keyElement]['PROPERTIES'][$keyProp]);
                        }
                    }
                }
                break;
            case 'ru':
                global $APPLICATION;
                if (stristr($APPLICATION->GetCurDir(), 'sale.basket.basket')) {
                    break;
                }

                foreach ($result as $keyElement => $arElement) {
                    $result[$keyElement]['PROPERTIES'] = array();
                    foreach ($arElement['PROPERTIES'] as $keyProp => $arProperty) {
                        if (stristr($arProperty['CODE'], 'RU_') !== false
                            && $arProperty['VALUE'] != false
                            || in_array($arProperty['CODE'], self::$arPropertyUseOfAllLanguage)
                        ) {
                            $result[$keyElement]['PROPERTIES'][str_replace("RU_",
                                "", $arProperty['CODE'])] = self::CorrectDataProp($arProperty);
                        }
                    }
                }
                break;
        }
    }

    function CorrectDataProp($arProp){
        $arProp["CODE"] = str_replace("RU_", "", $arProp["CODE"]);
        $arProp["NAME"] = str_replace("RU_", "", $arProp["NAME"]);
        
        return $arProp;
    }
    
    function getPropertyByID($data)
    {
        $all_callers = debug_backtrace();
        $run = 0;
        foreach ($all_callers as $arr) {
            if (strpos($arr['file'], 'filter')) {
                $run = 1;
                break;
            }
        }

        if ($run) {
            if (strtolower(LANGUAGE_ID) == 'ua' && stristr($data['CODE'],'RU_') && isset($data['CODE'])) {
                $data = false;
            }

            if ( strtolower(LANGUAGE_ID) == 'ru' && !stristr($data['CODE'],'RU_') && isset($data['CODE'])) {
                $data = false;
            }
        }

        if (isset($data['NAME']) || isset($data['CODE'])) {
            $data['NAME'] = str_replace('RU_', '', $data['NAME']);
//            $data['CODE'] = str_replace('RU_', '', $data['CODE']);
        }

        return $data;
    }

    function DisplayPropertiesElement(&$result, $arIblockParams = false)
    {
        $propertyPrefix = self::GetPropertyPrefix();
        foreach ($result as $keyElement => $valueElement) {
            $arChoosePropertiesOfElement = $arIblockParams[CATALOG_IBLOCK_ID]['PROPERTY_CODE'];
            if (!empty($arChoosePropertiesOfElement)) {
                foreach ($result as $elementId => &$arItem) {
                    foreach ($arChoosePropertiesOfElement as $key => $choosePropertyCode) {
                        $langPropertyCode = $propertyPrefix.$choosePropertyCode;
                        $arProperty = $arItem['PROPERTIES'][$langPropertyCode];
                        if (!empty($arProperty['VALUE'])) {
                            $arItem['DISPLAY_PROPERTIES'][$choosePropertyCode] = array(
                                'ID' => $arProperty['ID'],
                                'NAME' => str_replace($propertyPrefix, '',$arProperty['NAME']),
                                'CODE' => $arProperty['CODE'],
                                'VALUE' => $arProperty['VALUE'],
                                'DISPLAY_VALUE' => $arProperty['VALUE'],
                            );
                        }
                    }
                }
            }
        }

        self::MakePropertiesArrayOfElement($result, $propertyPrefix);
    }

    public static function MakePropertiesArrayOfElement(&$result, $propertyPrefix)
    {
        //NOT DISPLAY_PROPERTIES
        if (!empty($propertyPrefix)) {
            foreach ($result as $id => &$arItem) {
                foreach ($arItem['PROPERTIES'] as $propertyCode => $arProperty) {
                    if (!stristr($propertyCode,$propertyPrefix)) {
                        $langPropertyCode = $propertyPrefix . $propertyCode;
                    } else {
                        $langPropertyCode = $propertyCode;
                    }

                    if (!empty($arItem['PROPERTIES'][$langPropertyCode])) {
                        $arProperty['CODE'] = str_replace($propertyPrefix,'',$arProperty['CODE']);
                        $arProperty['NAME'] = str_replace($propertyPrefix,'',$arProperty['NAME']);
                        $arItem["PROPERTIES"][ $arProperty['CODE'] ] = $arProperty;
                    }
                }
            }
        }
    }

    //smart filter getIBlockItems
    function loadFilterDisplayPropertyCodesCurrent($arResult)
    {
        foreach ($arResult as $k => $v) {
            if ($v['PRICE']) {
                continue;
            }

            if (strpos($v['CODE'], PREFIX_UA) === 0) {


                if (LANGUAGE_ID != 'ua') {
                    unset($arResult[$k]);
                } else {
                    $modifyProp = self::GetParrentPropertyName($v);

                    $arResult[$k]["NAME"] = $modifyProp["NAME"];
                }
            } else {
                if (LANGUAGE_ID != 'ru') {
                    unset($arResult[$k]);
                }
            }
        }
        return $arResult;
    }

    public static function GetSeoTemplate($template) {
        $template = explode("||",$template);

        switch(strtoupper(LANGUAGE_ID)){
            case "UA":
                $temp = $template[0];
                break;
            case "RU":
                $temp = $template[1];
                break;
        }

        if(empty($temp)){
            $temp = $template[0];
        }
        return trim($temp);
    }

    public function UpdateBasketOfSiteID() {
        session_start();
        if(!isset($_SESSION["OLD_SITE_ID"])){
            $_SESSION["OLD_SITE_ID"] = SITE_ID;
        }


        if( $_SESSION["OLD_SITE_ID"] != SITE_ID){
            CModule::IncludeModule("sale");
            $dbBasketItems = CSaleBasket::GetList(
                array(
                    "NAME" => "ASC",
                    "ID" => "ASC"
                ),
                array(
                    "FUSER_ID" => CSaleBasket::GetBasketUserID(),
                    "LID" =>  $_SESSION["OLD_SITE_ID"],
                    "ORDER_ID" => "NULL"
                ),
                false,
                false,
                array("ID")
            );
            while ($arItems = $dbBasketItems->Fetch())
            {
                $arFields = array(
                    "LID" => SITE_ID,
                );
                CSaleBasket::Update($arItems["ID"], $arFields);
            }
        }

        $_SESSION["OLD_SITE_ID"] = SITE_ID;
    }

    public static function OrderElementProperties(&$arElementPropsArray, &$arElement)
    {
        //викликається в result_modifier.php
        $prefix = self::GetPropertyPrefix();
        if ($prefix) {
            $rsElement = CIBlockElement::GetList([],['ID'=>$arElement['PRODUCT_ID']],false,false,
                ['ID','IBLOCK_ID','PROPERTY_'.$prefix.'NAZVANIE'])->fetch();

            if ($rsElement['PROPERTY_'.$prefix.'NAZVANIE_VALUE']) {
                $arElement['NAME'] = $rsElement['PROPERTY_'.$prefix.'NAZVANIE_VALUE'];
            }
        }

        $arPropertiesSave = $arElementPropsArray;
        $arElementPropsArray = array();
        if ($prefix) {
            foreach ($arPropertiesSave as $key => $arProperty) {
                if (stristr($arProperty['CODE'], $prefix) && $arProperty['VALUE']) {
                    $arProperty['NAME'] = str_replace($prefix, '', $arProperty['NAME']);
                    $arElementPropsArray[] = $arProperty;
                }
            }
        } else {
            foreach ($arPropertiesSave as $key => $arProperty) {
                if (stristr($arProperty['CODE'],'RU_')) {
                    unset($arPropertiesSave[$key]);
                }
            }
            $arElementPropsArray = array_values($arPropertiesSave);
        }

        return $arElementPropsArray;
    }

    public static function getCorrentOfferPropertyName($codeProp, $currentName) {
        if (LANGUAGE_ID == 'ua') {
            return $currentName;
        } else {
            $ruCode = 'RU_'.$codeProp;
            $arProperty = CIBlockProperty::GetList([], ['IBLOCK_ID'=>6, 'CODE'=>$ruCode])->fetch();
            if ($arProperty['NAME']) {
                return $arProperty['NAME'];
            } else {
                return $currentName;
            }
        }
    }

}


if (strpos($_SERVER['REQUEST_URI'], '/admin/') <= 0) {
    require_once($_SERVER['DOCUMENT_ROOT'] . '/local/modules/iblock__/classes/general/iblock_element.php');
    require_once($_SERVER['DOCUMENT_ROOT'] . '/local/modules/iblock__/classes/general/iblockelement.php');
    require_once($_SERVER['DOCUMENT_ROOT'] . '/local/modules/iblock__/classes/general/iblocksection.php');
    require_once($_SERVER['DOCUMENT_ROOT'] . '/local/modules/iblock__/classes/mysql/iblockelement.php');
    require_once($_SERVER['DOCUMENT_ROOT'] . '/local/modules/iblock__/classes/mysql/iblocksection.php');
    require_once($_SERVER['DOCUMENT_ROOT'] . '/local/modules/iblock__/lib/inheritedproperty/sectionvalues.php');
    require_once($_SERVER['DOCUMENT_ROOT'] . '/local/modules/iblock__/lib/template/engine.php');
    require_once($_SERVER['DOCUMENT_ROOT'] . '/local/modules/iblock__/lib/component/element.php');
    require_once($_SERVER['DOCUMENT_ROOT'] . '/local/modules/iblock__/classes/general/iblockpropresult.php');
}


function changeOrderPropertyTitle($arResult)
{

    global $LANG_PARAMS;
    $LANG_PARAMS['DEFAULT_LANG'] = 'ua';

    foreach ($arResult["ORDER_PROP"]["properties"] as $key => &$arProp) {
        if (LANGUAGE_ID != $LANG_PARAMS['DEFAULT_LANG']) {
            $messageCode = strtoupper(LANGUAGE_ID) . "_" . $arProp["CODE"];

            $arProp["NAME"] = GetMessage($messageCode);
        }

    }

    return $arResult;
}


