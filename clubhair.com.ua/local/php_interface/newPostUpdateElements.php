<?
// nova powta api
function GetNextTime($name)
{
    return COption::GetOptionString("main",$name.'_NEXT_TIME_UPDATE');
}

function GetStep($name)
{
    return COption::GetOptionString("main",$name.'_NEXT_STEP');
}

function GetSubStep($name)
{
    return COption::GetOptionString("main",$name.'_NEXT_SUB_STEP');
}

function NextTime($name)
{
    $date = date("d.m.Y")." 02:00:00";
    $stmp = MakeTimeStamp($date, "DD.MM.YYYY HH:MI::SS");
    $stmp = $stmp+86400;
    COption::SetOptionString("main",$name.'_NEXT_TIME_UPDATE',$stmp);
    return true;
}

function NextStep($name,$step)
{
    if(is_int($step)){
        COption::SetOptionString("main",$name.'_NEXT_STEP',$step);
        return true;
    } else {
        return false;
    }
}

function NextSubStep($name,$step)
{
    if(is_int($step)){
        COption::SetOptionString("main",$name.'_NEXT_SUB_STEP',$step);
        return true;
    } else {
        return false;
    }
}

function requestNewPost($model, $method, $params = NULL, $language='ru',$apiKey)
{
    $url = 'https://api.novaposhta.ua/v2.0/json/';

    $data = array(
        'apiKey' => $apiKey,
        'modelName' => $model,
        'calledMethod' => $method,
        'language' => "ru",
        'methodProperties' => $params
    );


    $post = json_encode($data);

    $ch = curl_init($url);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json'));
    curl_setopt($ch, CURLOPT_HEADER, 0);
    curl_setopt($ch, CURLOPT_POST, 1);
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
    curl_setopt($ch, CURLOPT_POSTFIELDS, $post);
    $result = curl_exec($ch);
    curl_close($ch);

    return json_decode($result, 1);
}

function SendErrorNewPost($errorsAr)
{
    $error = "Обновление новой почти перенесено на 1 неделю причина:\n";
    foreach($errorsAr as $er){
        $error .=$er."\n";
    }
    mail('rkm@wise-solutions.com.ua', 'clubhair.com.ua', $error);
}

//update nova powta agent
function UpdateNovaPoshta()
{
//    COption::SetOptionString("main",'NEW_POST_NEXT_TIME_UPDATE',time());
    $nameOption = "NEW_POST";

    $NextTime = GetNextTime($nameOption);

    CModule::IncludeModule("iblock");
    CModule::IncludeModule("main");
    //если дата пуста или настало время обновления
    if(empty($NextTime) || $NextTime<time()){
        $arParams["IBLOCK_ID"] = 7;
        $arParams["STEP_DEACTIVE_SEC"] = 300;
        $arParams["STEP_SEC"] = 200;
        $arParams["STEP_DEACTIVE_EL"] = 300;
        $arParams["STEP_EL"] = 300;
        $arParams["FILE_CITIES"] = 'NewPostCitiesIntegrate.txt';
        $arParams["FILE_WAREHOUSE"] = 'NewPostWarehouseIntegrate.txt';
        $arParams["API_KEY"] = "f8cf44a2969b7816e393c4a7b850272f";

        $step = GetStep($nameOption);
        $subStep = GetSubStep($nameOption);

        $step = ($step>0) ? $step : 1;
        $subStep = ($subStep>0) ? $subStep : 1;

        CModule::IncludeModule("iblock");
        echo 'step:'.$step.'<br>'.'sub_step:'.$subStep;
        switch($step){
            case "1":
                $resultCities = requestNewPost("Address", "getCities", array('Page'=>0,'FindByString'=>'','Ref'=>''), 'ru',$arParams["API_KEY"]);

                if($resultCities["success"]==true && empty($resultCities["errors"])){
                    $fileCity = fopen($_SERVER["DOCUMENT_ROOT"]."/upload/".$arParams["FILE_CITIES"],"w+");
                    fwrite($fileCity,serialize($resultCities["data"]));
                    fclose($fileCity);
                    NextStep($nameOption,2);
                    NextSubStep($nameOption,1);
                }
                else{
                    SendErrorNewPost($resultCities["errors"]);
                    NextTime($nameOption);
                }
                break;
            case "2":

                $resultWarehouses = requestNewPost("Address", "getWarehouses", array('Page'=>0,'CityRef'=>''), 'ru',$arParams["API_KEY"]);

                if($resultWarehouses["success"]==true && empty($resultWarehouses["errors"])){
                    $fileCity = fopen($_SERVER["DOCUMENT_ROOT"]."/upload/".$arParams["FILE_WAREHOUSE"],"w+");
                    fwrite($fileCity,serialize($resultWarehouses["data"]));
                    fclose($fileCity);

                    NextStep($nameOption,3);
                    NextSubStep($nameOption,1);
                }
                else{
                    SendErrorNewPost($resultWarehouses["errors"]);
                    NextTime($nameOption);
                }
                break;
            case "3":

                $countIBlockSection = CIBlockSection::GetCount(array("IBLOCK_ID"=>$arParams["IBLOCK_ID"]));
                $subStep = GetSubStep($nameOption);

                if(($arParams["STEP_DEACTIVE_SEC"]*$subStep-$arParams["STEP_DEACTIVE_SEC"])<$countIBlockSection){

                    $resSection = CIBlockSection::GetList(
                        array("ID"=>"DESC"),
                        array("IBLOCK_ID"=>$arParams["IBLOCK_ID"]),
                        false,
                        array("ID"),
                        array("iNumPage"=>$subStep,"nPageSize"=>$arParams["STEP_DEACTIVE_SEC"])
                    );

                    $bs = new CIBlockSection;

                    while($arSec = $resSection->Fetch()){
//                        $bs->Update($arSec["ID"],array("ACTIVE"=>"N"));
                    }
                    $subStep++;
                    NextSubStep($nameOption,$subStep);
                }
                else{
                    NextStep($nameOption,4);
                    NextSubStep($nameOption,1);
                }

                break;
            case "4":

                $subStep = GetSubStep($nameOption);

                $RangeStart = ($subStep==1) ? 0 : ($subStep-1)*$arParams["STEP_SEC"];

                $allCity = unserialize(file_get_contents($_SERVER["DOCUMENT_ROOT"]."/upload/".$arParams["FILE_CITIES"]));

                $scaning_array = array_slice($allCity,$RangeStart,$arParams["STEP_SEC"]);

                if(count($scaning_array)>0){
                    foreach($scaning_array as $key => $arItem){

                        $arItem["DescriptionRu"] = htmlspecialchars_decode($arItem["DescriptionRu"]);
                        $arItem["Description"] = htmlspecialchars_decode($arItem["Description"]);

                        $resReg = CIBlockSection::GetList(array(),array("IBLOCK_ID"=>$arParams["IBLOCK_ID"],"XML_ID"=>$arItem["Ref"]),false,array("ID","NAME","IBLOCK_ID"),array("nPageSize"=>"1","iNumPage"=>1))->Fetch();

                        $bs = new CIBlockSection;

                        if($resReg["ID"]>0){
                            $arFields = Array(
                                "ACTIVE" => "Y",
                                "IBLOCK_ID" => $arParams["IBLOCK_ID"],
                                "XML_ID" => $arItem["Ref"],
                                "UF_NAME_RU" => trim($arItem["DescriptionRu"]),
                                "NAME" => $arItem["Description"],
                            );

                            $res = $bs->Update($resReg["ID"], $arFields);
                        }
                        else{

                            $arFields = Array(
                                "NAME" => $arItem["Description"],
                                "ACTIVE" => "Y",
                                "IBLOCK_ID" => $arParams["IBLOCK_ID"],
                                "XML_ID" => $arItem["Ref"],
                                "UF_NAME_RU" => trim($arItem["DescriptionRu"]),
                            );
                            $res = $bs->add($arFields);
                        }

                    }
                    $subStep++;
                    NextSubStep($nameOption,$subStep);
                }
                else{
                    NextStep($nameOption,5);
                    NextSubStep($nameOption,1);
                }
                break;
            case "5":

                $countIBlockElement = CIBlockElement::GetList(array(),array("IBLOCK_ID"=>$arParams["IBLOCK_ID"]),array());
                $subStep = GetSubStep($nameOption);

                if(($arParams["STEP_DEACTIVE_EL"]*$subStep-$arParams["STEP_DEACTIVE_EL"])<$countIBlockElement){

                    $resElements = CIBlockElement::GetList(
                        array("ID"=>"DESC"),
                        array("IBLOCK_ID"=>$arParams["IBLOCK_ID"]),
                        false,
                        array("iNumPage"=>$subStep,"nPageSize"=>$arParams["STEP_DEACTIVE_EL"]),
                        array("ID")
                    );

                    $el = new CIBlockElement;

                    while($arEl = $resElements->Fetch()){
//                        $el->Update($arEl["ID"],array("ACTIVE"=>"N"));
                    }
                    $subStep++;
                    NextSubStep($nameOption,$subStep);
                }
                else{
                    NextStep($nameOption,6);
                    NextSubStep($nameOption,1);
                }
                break;
            case "6":
                $subStep = GetSubStep($nameOption);

                $RangeStart = ($subStep==1) ? 0 : ($subStep-1)*$arParams["STEP_EL"];

                $allWarehouse = unserialize(file_get_contents($_SERVER["DOCUMENT_ROOT"]."/upload/".$arParams["FILE_WAREHOUSE"]));

                $scaning_array = array_slice($allWarehouse,$RangeStart,$arParams["STEP_EL"]);

                if(count($scaning_array)>0){
                    foreach($scaning_array as $key => $arItem){

                        $arItem["DescriptionRu"] = htmlspecialchars_decode($arItem["DescriptionRu"]);
                        $arItem["Description"] = htmlspecialchars_decode($arItem["Description"]);

                        $name = trim($arItem["Description"]);

                        $resEl = CIBlockElement::GetList(array(),array("IBLOCK_ID"=>$arParams["IBLOCK_ID"],"XML_ID"=>$arItem["Ref"]),false,array("nPageSize"=>"1","iNumPage"=>1),array("ID","NAME","IBLOCK_ID","IBLOCK_SECTION_ID"))->Fetch();

                        $el = new CIBlockElement;
                        $resSection = CIBlockSection::GetList(array(),array("IBLOCK_ID"=>$arParams["IBLOCK_ID"],"XML_ID"=>$arItem["CityRef"]),false,array("ID","NAME"),array("nPageSize"=>"1","iNumPage"=>1))->Fetch();


                        $arFields = array();

                        if($resEl["ID"]>0){

                            if($resEl["IBLOCK_SECTION_ID"]!=$resSection["ID"]){$arFields["IBLOCK_SECTION_ID"] = $resSection["ID"];}
                            $arFields["ACTIVE"] = "Y";
                            $arFields["IBLOCK_ID"] = $arParams["IBLOCK_ID"];
                            $arFields["XML_ID"] = $arItem["Ref"];
                            $arFields["NAME"] = $name;

                            $res = $el->Update($resEl["ID"], $arFields);

                            CIBlockElement::SetPropertyValuesEx($resEl["ID"], $arParams["IBLOCK_ID"],
                                array(
                                    "LONGIRUDE"=>$arItem["Longitude"] ,
                                    "LATITUDE"=>$arItem["Latitude"],
                                    "NAME_RU"=>$arItem["DescriptionRu"],
                                    "TotalMaxWeightAllowed"=>$arItem["TotalMaxWeightAllowed"],
                                    "PlaceMaxWeightAllowed"=>$arItem["PlaceMaxWeightAllowed"],
                                )
                            );


                        }
                        else{

                            $arFields["IBLOCK_ID"] = $arParams["IBLOCK_ID"];
                            $arFields["NAME"] = $name;
                            $arFields["XML_ID"] = $arItem["Ref"];
                            $arFields["IBLOCK_SECTION_ID"] = $resSection["ID"];
                            $arFields["ACTIVE"] = "Y";


                            $res = $el->add($arFields);

                            CIBlockElement::SetPropertyValuesEx($res, $arParams["IBLOCK_ID"], array("LONGIRUDE"=>$arItem["Longitude"] ,"LATITUDE"=>$arItem["Latitude"],"NAME_RU"=>$arItem["DescriptionRu"]));

                        }
                    }
                    $subStep++;

                    NextSubStep($nameOption,$subStep);
                }
                else{
                    NextStep($nameOption,7);
                    NextSubStep($nameOption,1);
                }
                break;
            case "7":

                NextTime($nameOption);
                NextStep($nameOption,1);
                NextSubStep($nameOption,1);

                BXClearCache(true, "/cache_arr_list/");
                break;
        }
    }
    return "UpdateNovaPoshta();";
}