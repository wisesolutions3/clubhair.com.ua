$(document).ready(function () {
    
    var arProp = GetNewPostPropertiesId();

    BX.addCustomEvent('onAjaxSuccess', function() {
        OrderUpdate(arProp);
        OnLoad();
    });

    OrderUpdate(arProp);
    OnLoad();

    InitUlDropdown();

	$(document).on("click", ".phone-link, .contacts_phone a",function (){
        gtag("event", "call", {"event_category" : "click_phone"});
    })

    $(document).on("click", ".contacts_sales a",function (){
        gtag("event", "send_mail", {"event_category" : "email"});
    })

    $(document).on("click",".bx_pagination_bottom a.show_more ",function (){
        $this = this;
        $url = $($this).attr("href");

        $.ajax({
            url: $url,
            success:function (data){
                $(data).find(".catalog__list-content .product-item-container").each(function (i,e){
                    $(".catalog__list-content .product__list").append(e)
                })

                $(".bx_pagination_bottom").html($(data).find(".bx_pagination_bottom").html())
            }
        })

        history.pushState(null, null, $url);

        return false
    })

});

$( document ).ajaxComplete(function( event, xhr, settings ) {
    var arProp = GetNewPostPropertiesId();
    OrderUpdate(arProp);
    DepartmentSelect(arProp);
    $('.select2.select2-container.select2-container--default').css({'width':'100%'});

});

function InitUlDropdown() {
	
    var dd1 = new DropDown($('#card-select'));
    $(document).click(function () {
        $('.wrap-drop').removeClass('active');
    });
	var imgStock = '';
	$('#card-select').on('click', function(){
		imgStock = $('.product-item-detail-slider-image img').attr('src'); 
	});
		
	$('#card-select .product-item-scu-item-text-block').hover(function(){
		var imgHover = $(this).find('img').attr('src');
		$('.product-item-detail-slider-image img').attr('src', imgHover);
	  }, function(){
		$('.product-item-detail-slider-image img').attr('src', imgStock);
	});
	
}

function GetNewPostPropertiesId() {
    if (BX.message('SITE_DIR') === '/') {
        return {'CITY_WS':24,'DEPARTMENT_WS':25, 'TITLE_CHOOSE_CITY': "Виберіть місто", 'TITLE_CHOOSE_DEP':'Виберіть відділення' }
    } else { //ru
        return {'CITY_WS':18,'DEPARTMENT_WS':19, 'TITLE_CHOOSE_CITY': "Выберите город", 'TITLE_CHOOSE_DEP':'Выберите отделение'}
    }
}

function NewPostInit() {
    var arProp = GetNewPostPropertiesId();
    $('select[name="ORDER_PROP_'+arProp.CITY_WS+'"]').change(function(){
        newPost(this.value, arProp);
    });

    if($('select[name=ORDER_PROP_'+arProp.CITY_WS+']').length > 0){
        $('select[name="ORDER_PROP_'+arProp.CITY_WS+'"]').select2({
            placeholder: arProp.TITLE_CHOOSE_CITY,
            minimumInputLength: 3,
            ajax: {
                url: window.location.pathname,
                data: function (params) {
                    var query = {
                        search: params.term,
                        type: 'public'
                    }

                    return query;
                },
                processResults: function (data) {
                    data = JSON.parse(data);

                    return {
                        results: data.results
                    };
                }
            }
        });
    }

    if($('select[name=ORDER_PROP_'+arProp.DEPARTMENT_WS+']').length > 0){
        $('select[name=ORDER_PROP_'+arProp.DEPARTMENT_WS+']').select2({
            placeholder: arProp.TITLE_CHOOSE_DEP,
            language: {
                "inputTooShort": function(e) {
                    return arProp.TITLE_CHOOSE_DEP
                },
                "noResults": function() {
                    return "Не найдено"
                },
                "searching": function() {
                    return "Загрузка…"
                }
            },
        });
    }

    $('.select2.select2-container.select2-container--default').css({'width':'100%'});
}

function newPost(id, arProp){
    $.ajax({
        data:{'delivery_city':id},
        type:"post",
        dataType: "json",
        beforeSend: function() {
            $('#loading_screen').show();
            $('.popup-window-overlay.bx-step-opacity').show();
        },
        success: function(data){

            if ($('[data-property-id-row='+arProp.DEPARTMENT_WS+']').length > 0) {
                $('[data-property-id-row='+arProp.DEPARTMENT_WS+']').find('div.soa-property-container').html(
                    '<select name="ORDER_PROP_'+arProp.DEPARTMENT_WS+'"></select>'
                );
                $.each(data,function(index,value){
                    $('[data-property-id-row='+arProp.DEPARTMENT_WS+'] select').append($("<option></option>", {value: index, text: value}));
                })
            }
            OnLoad();
            $('#loading_screen').hide();
            $('.popup-window-overlay.bx-step-opacity').hide();

        }
    });
}

function DepartmentSelect(arProp) {

    if($('select[name=ORDER_PROP_'+arProp.DEPARTMENT_WS+']').length > 0){
        $('select[name=ORDER_PROP_'+arProp.DEPARTMENT_WS+']').select2({
            placeholder: "Выберите отделение",
            language: {
                "inputTooShort": function(e) {
                    return arProp.TITLE_CHOOSE_DEP
                },
                "noResults": function() {
                    return "Не найдено"
                },
                "searching": function() {
                    return "Загрузка…"
                }
            },
        });
    }
}

function OrderUpdate(arProp) {
    if($('select[name="ORDER_PROP_'+arProp.CITY_WS+'"] option:first').val() <= 0) {
        $('select[name="ORDER_PROP_'+arProp.CITY_WS+'"] option:first').val('');
        $('select[name="ORDER_PROP_'+arProp.CITY_WS+'"] option:first').prop('disabled', 'disabled');
    }

    if($('select[name="ORDER_PROP_'+arProp.DEPARTMENT_WS+'"] option:first').val() <= 0) {
        $('select[name="ORDER_PROP_'+arProp.DEPARTMENT_WS+'"] option:first').val('');
        $('select[name="ORDER_PROP_'+arProp.DEPARTMENT_WS+'"] option:first').prop('disabled', 'disabled');
    }
}

function FixNotSelectedOfferOnStartPage() {
    if ($('.product-item-scu-item-text-container.selected').length <= 0) {
        $('.product-item-scu-item-text-container:first').addClass('selected');
    }
}

function phoneMasked() {
    $(".ws-need-mask").maskWise("+380 (99) 999-99-99");
    $(".ws-need-mask").attr('placeholder', '+380 (__) ___-__-__');
}


function OnLoad() {
    NewPostInit();
    FixNotSelectedOfferOnStartPage();
    phoneMasked();
    basketFix();
}

BX.addCustomEvent('onAjaxSuccess', function(){
    basketFix();
});

function basketFix()
{
    return //28.07.2021 added
    $(".basket-item-property.basket-item-property-scu-text:eq(1)").hide();
}

function DropDown(el) {
	this.dd = el;
	this.placeholder = this.dd.children('span');
	this.opts = this.dd.find('ul.product-item-scu-item-list li');
	this.val = '';
	this.index = -1;
	this.initEvents();
}

DropDown.prototype = {
	initEvents: function () {
		var obj = this;
		obj.dd.on('click', function (e) {
			e.preventDefault();
			e.stopPropagation();
			$(this).toggleClass('active');
		});
		obj.opts.on('click', function () {
			var opt = $(this);
			obj.val = opt.html();
			obj.index = opt.index();
			obj.placeholder.html(obj.val);
			opt.siblings().removeClass('selected');
			opt.filter(':contains("' + obj.val + '")').addClass('selected');
		}).change();
	},
	getValue: function () {
		return this.val;
	},
	getIndex: function () {
		return this.index;
	}
};

		
