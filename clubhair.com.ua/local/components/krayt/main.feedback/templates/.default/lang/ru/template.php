<?
$MESS ['MFT_NAME'] = "Ваше имя";
$MESS ['MFT_EMAIL'] = "E-mail";
$MESS ['MFT_PHONE'] = "Телефон";
$MESS ['MFT_MESSAGE'] = "Ваше сообщение";
$MESS ['MFT_CAPTCHA'] = "Защита от автоматических сообщений";
$MESS ['MFT_CAPTCHA_CODE'] = "Введите слово на картинке";
$MESS ['MFT_SUBMIT'] = "Отправить сообщение";
$MESS ['MF_CAPTHCA_EMPTY'] = "Не указан код защиты от автоматических сообщений.";
?>