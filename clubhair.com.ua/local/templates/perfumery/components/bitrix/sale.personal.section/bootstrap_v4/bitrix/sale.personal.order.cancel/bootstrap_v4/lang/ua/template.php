<?
$MESS["SALE_CANCEL_ORDER1"] = "Ви впевнені, що хочете скасувати";
$MESS["SALE_CANCEL_ORDER2"] = "замовлення";
$MESS["SALE_CANCEL_ORDER3"] = "Скасування замовлення необоротна.";
$MESS["SALE_CANCEL_ORDER4"] = "Будь ласка, вкажіть, причину скасування замовлення:";
$MESS["SALE_CANCEL_ORDER_BTN"] = "Скасувати замовлення";
$MESS["SALE_RECORDS_LIST"] = "У список замовлень";
?>