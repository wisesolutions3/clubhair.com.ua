<?
$MESS["TP_BST_CONTAINER_ID"] = "ID контейнера, по ширині якого будуть виводитися результати";
$MESS["TP_BST_CONVERT_CURRENCY"] = "Показувати ціни в одній валюті";
$MESS["TP_BST_CURRENCY_ID"] = "Валюта, в яку будуть сконвертовані ціни";
$MESS["TP_BST_INPUT_ID"] = "ID рядки введення пошукового запиту";
$MESS["TP_BST_PREVIEW_HEIGHT"] = "Висота картинки";
$MESS["TP_BST_PREVIEW_TRUNCATE_LEN"] = "Максимальна довжина анонса для виведення";
$MESS["TP_BST_PREVIEW_WIDTH"] = "Ширина картинки";
$MESS["TP_BST_PRICE_CODE"] = "Тип ціни";
$MESS["TP_BST_PRICE_VAT_INCLUDE"] = "Включати ПДВ в ціну";
$MESS["TP_BST_SHOW_INPUT"] = "Показувати форму введення пошукового запиту";
$MESS["TP_BST_SHOW_PREVIEW"] = "Показати картинку";
$MESS["TP_BST_TEMPLATE_THEME"] = "Колірна тема";
$MESS["TP_BST_THEME_BLUE"] = "синя (тема за замовчуванням)";
$MESS["TP_BST_THEME_GREEN"] = "зелена";
$MESS["TP_BST_THEME_RED"] = "червона";
$MESS["TP_BST_THEME_SITE"] = "Брати тему з налаштувань сайту (для вирішення bitrix.eshop)";
$MESS["TP_BST_THEME_YELLOW"] = "жовта";
?>