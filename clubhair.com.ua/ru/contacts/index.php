<?
require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/header.php");
$APPLICATION->SetPageProperty("description", "CLUBHAIR: интернет-магазин профессиональной косметики и профессиональных средств по уходу за волосами");
$APPLICATION->SetPageProperty("keywords", "CLUBHAIR: интернет-магазин профессиональной косметики и профессиональных средств по уходу за волосами");
$APPLICATION->SetPageProperty("title", "Контакты - CLUBHAIR: интернет-магазин профессиональной косметики и профессиональных средств по уходу за волосами");
$APPLICATION->SetTitle("Контакты");
?>    <div class="contacts_wrapper">
    <div class="title_box">
        <h1 class="title">Контакты</h1>
    </div>
    <div class="contacts_content row">
        <div class="contacts__adress col-lg-3 col-sm-12 col-md-3">
            <?php $APPLICATION->IncludeFile(SITE_DIR . 'contacts/include/contacts.php') ?>
        </div>
        <div class="contacts__form col-lg-9 col-sm-9 col-9 map-relative">
            <? $APPLICATION->IncludeComponent(
                "bitrix:catalog.store",
                "stores",
                array(
                    "CACHE_TIME" => "3600",
                    "CACHE_TYPE" => "A",
                    "MAP_TYPE" => "0",
                    "PHONE" => "Y",
                    "SCHEDULE" => "Y",
                    "SEF_MODE" => "N",
                    "SET_TITLE" => "N",
                    "TITLE" => "",
                    "COMPONENT_TEMPLATE" => "stores"
                ),
                false,
                array(
                    "ACTIVE_COMPONENT" => "Y"
                )
            ); ?>

        </div>
    </div>
    </div>

<? require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/footer.php"); ?>