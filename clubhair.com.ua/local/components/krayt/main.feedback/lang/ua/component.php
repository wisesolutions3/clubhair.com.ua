<?
$MESS['MF_OK_MESSAGE'] = "Спасибі, ваше повідомлення прийнято.";
$MESS['MF_REQ_NAME'] = "Вкажіть ваше ім'я";
$MESS['MF_REQ_EMAIL'] = "Вкажіть ваш E-mail";
$MESS['MF_REQ_EMAIL_FORMAT'] = "Вкажіть вірний E-mail";
$MESS['MF_REQ_PHONE'] = "Вкажіть ваш телефон";
$MESS['MF_REQ_MESSAGE'] = "Ви не написали повідомлення";
$MESS['MF_EMAIL_NOT_VALID'] = "Вказаний E-mail є коректним.";
$MESS['MF_CAPTCHA_WRONG'] = "Невірно вказаний код захисту від автоматичних повідомлень.";
$MESS['MF_CAPTHCA_EMPTY'] = "Не вказаний код захисту від автоматичних повідомлень.";
$MESS['MF_SESS_EXP'] = "Ваша сесія закінчилася. Написати користувачеві повторно.";
?>