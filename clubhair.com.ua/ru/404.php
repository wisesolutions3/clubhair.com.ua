<?
include_once($_SERVER['DOCUMENT_ROOT'].'/bitrix/modules/main/include/urlrewrite.php');

CHTTP::SetStatus("404 Not Found");
@define("ERROR_404","Y");

require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");

$APPLICATION->SetTitle("404 Not Found");

?>

<div class="wrapper-inner">

    <div class="page_404 row">
        <div class="number col-lg-5 col-md-12 col-12">404</div>
        <div class="text_404 col-lg-7 col-md-12 col-12">
            <div class="title_404">Произошла ошибка, нам очень жаль</div>
            <div class="content_404">Ошибка могла произойти по нескольким причинам:
                <ul class="line">
                    <li>Вы ввели неправильный адрес</li>
                    <li>Страница, на которую вы хотели зайти, устарела и была удалена</li>
                    <li>На сервере произошла ошибка. Если так, то мы уже знаем о ней и обязательно исправим</li>
                </ul>
            </div>
            <div class="back_link">
                <a href="/">
                    <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" viewBox="275.25 157.75 40 40">
                        <path d="M286.421,178.089l5.326,4.517c0.228,0.193,0.569,0.193,0.798,0c0.228-0.193,0.228-0.482,0-0.676l-4.33-3.672  h15.466c0.313,0,0.569-0.217,0.569-0.482c0-0.267-0.256-0.484-0.569-0.484h-15.522l4.387-3.719c0.228-0.193,0.228-0.483,0-0.677  c-0.114-0.097-0.257-0.146-0.399-0.146c-0.142,0-0.284,0.049-0.398,0.146l-5.326,4.517  C286.193,177.605,286.193,177.896,286.421,178.089z"></path>
                    </svg>
                    вернуться на главную
                </a>
            </div>
        </div>
        <div style="clear: both;"></div>
    </div>

</div>

<?

require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>