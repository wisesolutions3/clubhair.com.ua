<? if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();

/**
 * @var CBitrixComponentTemplate $this
 * @var CatalogSectionComponent $component
 */

$component = $this->getComponent();
$arParams = $component->applyTemplateModifications();
foreach($arResult['ITEMS'] as $key => &$val)
{
    if(!empty($arResult['OFFERS']))
    {

            $itemColorXmlId = $arResult['OFFERS'][0]['PROPERTIES']['PRODUCT_COLOR']['VALUE'];
            foreach($arResult['SKU_PROPS']['PRODUCT_COLOR']["VALUES"] as $sKey => &$sVal)
            {
                if($sVal['XML_ID'] == $itemColorXmlId)
                {
                    $sVal['PICT']['SRC'] =  $oVal['MORE_PHOTO'][0]['SRC'];
                }
            }

    }
}
unset($val);
