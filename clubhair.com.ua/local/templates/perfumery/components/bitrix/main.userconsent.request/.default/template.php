<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

/** @var array $arParams */
/** @var array $arResult */

use Bitrix\Main\Localization\Loc;

Loc::loadMessages(__DIR__ . '/user_consent.php');
$config = \Bitrix\Main\Web\Json::encode($arResult['CONFIG']);

$linkClassName = 'main-user-consent-request-announce';
if ($arResult['URL'])
{
    $url = htmlspecialcharsbx(\CUtil::JSEscape($arResult['URL']));
    $label = $arParams['TEXT'].' '.htmlspecialcharsbx($arResult['LABEL']);
    $label = explode('%', $label);
    $label = implode('', array_merge(
        array_slice($label, 0, 1),
        ['<a class="user_consent_link" href="' . $url  . '" target="_blank">'],
        array_slice($label, 1, 1),
        ['</a>'],
        array_slice($label, 2)
    ));
}
else
{
    $label = htmlspecialcharsbx($arResult['INPUT_LABEL']);
    $linkClassName .= '-link';
}
?>

<label data-bx-user-consent="<?=htmlspecialcharsbx($config)?>" class="main-user-consent-request">
    <input type="checkbox" value="Y" <?=($arParams['IS_CHECKED'] ? 'checked' : '')?> name="<?=htmlspecialcharsbx($arParams['INPUT_NAME'])?>">
    <span class="<?=$linkClassName?>"><?=$label?></span>
</label>
<script type="text/html" data-bx-template="main-user-consent-request-loader">
    <div class="bx-soa-tooltip bx-soa-tooltip-static bx-soa-tooltip-danger tooltip top"  style="opacity: 1; display: block;">
        <div class="tooltip-inner"><?=GetMessage("USER_CONSENT_UNSIGNED")?></div>
    </div>
</script>