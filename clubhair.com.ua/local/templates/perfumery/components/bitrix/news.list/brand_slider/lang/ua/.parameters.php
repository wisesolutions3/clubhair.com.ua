<?
$MESS["TP_BND_AVERAGE"] = "Середнє значення";
$MESS["TP_BND_DISPLAY_AS_RATING"] = "Як рейтингу показувати";
$MESS["TP_BND_DISPLAY_DATE"] = "Виводити дату елемента";
$MESS["TP_BND_DISPLAY_NAME"] = "Виводити назву елемента";
$MESS["TP_BND_DISPLAY_PICTURE"] = "Виводити зображення для анонса";
$MESS["TP_BND_DISPLAY_PREVIEW_TEXT"] = "Виводити текст анонса";
$MESS["TP_BND_MAX_VOTE"] = "Максимальний бал";
$MESS["TP_BND_MEDIA_PROPERTY"] = "Властивість для відображення медіа";
$MESS["TP_BND_RATING"] = "Рейтинг";
$MESS["TP_BND_SEARCH_PAGE"] = "Шлях сторінки пошуку";
$MESS["TP_BND_SHARE_HANDLERS"] = "Використовувані соц. закладки та мережі";
$MESS["TP_BND_SHARE_SHORTEN_URL_KEY"] = "Ключ для для bit.ly";
$MESS["TP_BND_SHARE_SHORTEN_URL_LOGIN"] = "Логін для bit.ly";
$MESS["TP_BND_SHARE_TEMPLATE"] = "Шаблон компонента панелі соц. закладок";
$MESS["TP_BND_SLIDER_PROPERTY"] = "Властивість із зображеннями для слайдера";
$MESS["TP_BND_TEMPLATE_THEME"] = "Колірна тема";
$MESS["TP_BND_THEME_BLACK"] = "темна";
$MESS["TP_BND_THEME_BLUE"] = "синя (тема за замовчуванням)";
$MESS["TP_BND_THEME_GREEN"] = "зелена";
$MESS["TP_BND_THEME_RED"] = "червона";
$MESS["TP_BND_THEME_SITE"] = "Брати тему з налаштувань сайту (для вирішення bitrix.eshop)";
$MESS["TP_BND_THEME_WOOD"] = "дерево";
$MESS["TP_BND_THEME_YELLOW"] = "жовта";
$MESS["TP_BND_USE_RATING"] = "дозволити голосування";
$MESS["TP_BND_USE_SHARE"] = "Відображати панель соц. закладок";
$MESS["TP_BND_VOTE_NAMES"] = "Підписи до балів";
?>