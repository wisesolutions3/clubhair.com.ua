<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();                        
if(\Bitrix\Main\Loader::includeSharewareModule("krayt.perfumery") == \Bitrix\Main\Loader::MODULE_DEMO_EXPIRED || 
   \Bitrix\Main\Loader::includeSharewareModule("krayt.perfumery") ==  \Bitrix\Main\Loader::MODULE_NOT_FOUND
    )
{ return false;}?>
<?if (!empty($arResult)):?>
	<ul class="lk__nav">
		<?
		$previousLevel = 0;
		foreach($arResult as $arItem):?>

			<li class="lk__nav_item <?if ($arItem["SELECTED"]):?>current<?endif?>"><a href="<?=$arItem["LINK"]?>"><?=$arItem["TEXT"]?></a></li>


		<?endforeach?>
	</ul>
<?endif?>