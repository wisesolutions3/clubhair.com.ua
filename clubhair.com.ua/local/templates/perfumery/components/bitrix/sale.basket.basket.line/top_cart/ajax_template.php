<?if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED!==true) die();

$this->IncludeLangFile('template.php');

$cartId = $arParams['cartId'];

require(realpath(dirname(__FILE__)).'/top_template.php');

if ($arParams["SHOW_PRODUCTS"] == "Y" && ($arResult['NUM_PRODUCTS'] > 0 || !empty($arResult['CATEGORIES']['DELAY'])))
{
?>
    <div data-role="basket-item-list" class="cartline_wrapper top_slider">
        <div class="wrapper-inner">
            <div id="<?=$cartId?>products">
                <div class="owl-carousel">

                    <?foreach ($arResult["CATEGORIES"] as $category => $items):
                        if (empty($items))
                            continue;
                        ?>
                        <?foreach ($items as $v):?>
                        <div class="product-item-container">
                            <div class="product_item">
                                <div class="close-btn" onclick="<?=$cartId?>.removeItemFromCart(<?=$v['ID']?>)"></div>
                                <?if ($arParams["SHOW_IMAGE"] == "Y" && $v["PICTURE_SRC"]):?>

                                    <div class="product_item_img">
                                        <?if($v["DETAIL_PAGE_URL"]):?>
                                            <a class="product_item-link" href="<?=$v["DETAIL_PAGE_URL"]?>" title="<?=$v["NAME"]?>">
                                                <div class="product_item_wrapper_img">
                                                    <img class="product_item_img-bg" src="<?=$v['PICTURE_SRC']; ?>">
                                                </div>
                                            </a>
                                        <?else:?>
                                            <div class="product_item_wrapper_img">
                                                <img class="product_item_img-bg" src="<?= $v['PICTURE_SRC'] ?>">
                                            </div>
                                        <?endif?>
                                    </div>
                                <?endif?>
                                <div class="product_item_name_box">
                                    <?if ($v["DETAIL_PAGE_URL"]):?>
                                        <a class="item_name" href="<?=$v["DETAIL_PAGE_URL"]?>"><?=$v["NAME"]?></a>
                                    <?else:?>
                                        <p class="item_name"><?=$v["NAME"]?></p>
                                    <?endif?>
                                </div>
                                <div class="product_item__price">
                                    <?if ($arParams["SHOW_PRICE"] == "Y"):?>
                                        <div class="price"><span><?=$v["PRICE_FMT"]?></span> <span style="color: #8f8f8f; font-size: 12px; font-weight: normal;">x <?=$v["QUANTITY"]?> шт.</span></div>
                                        <?if ($v["FULL_PRICE"] != $v["PRICE_FMT"]):?>
                                            <div class="old_price"><span><?=$v["FULL_PRICE"]?></span></div>
                                        <?endif?>
                                    <?endif?>
                                </div>

                            </div>
                        </div>
                        <?endforeach?>
                    <?endforeach?>
                </div>
            </div>
        </div>
        <div class="cartline_footer">
            <div class="go_to">
                <div class="price-goods">
                    <span class="goods-counter"><?=\Bitrix\Main\Localization\Loc::getMessage('TSB1_TOTAL_PRICE')?> </span>
                    <span class="total-price"><?= $arResult["TOTAL_PRICE"] ?> грн.</span>
                </div>
                <div class="btn-goods">
                    <a href="javscript:void(0)" class="btn-goods-link btn-green-border close_cart_line">
                        <span><?=\Bitrix\Main\Localization\Loc::getMessage('TSB1_BTN_CLOSE')?></span>
                    </a>
                    <a href="<?=$arParams['PATH_TO_BASKET'];?>" class="btn-goods-link btn-green-gradient">
                        <span><?=\Bitrix\Main\Localization\Loc::getMessage('TSB1_BTN_BASKET')?></span>
                    </a>
                </div>
            </div>
        </div>
    </div>

	<script>
		BX.ready(function(){
			<?=$cartId?>.fixCart();
		});

        $('.top_slider .owl-carousel').owlCarousel({
            items: 6,
            margin: -1,
            dots: false,
            nav: true,
            navSpeed: 500,
            lazyContent: true
        });
	</script>
<?
}