<?
$MESS["TP_BST_SHOW_INPUT"] = "Das Formular zur Eingabe der Suchanfrage anzeigen";
$MESS["TP_BST_INPUT_ID"] = "ID der Zeile zur Angabe der Suchanfrage";
$MESS["TP_BST_CONTAINER_ID"] = "ID des Containers zur BeschrГ¤nkung der Suchergebnisse durch die Breite";
$MESS["TP_BST_PRICE_CODE"] = "Preistyp";
$MESS["TP_BST_SHOW_PREVIEW"] = "Bild anzeigen";
$MESS["TP_BST_PREVIEW_WIDTH"] = "Bildbreite";
$MESS["TP_BST_PREVIEW_HEIGHT"] = "BildhГ¶he";
$MESS["TP_BST_PREVIEW_TRUNCATE_LEN"] = "Maximale TextlГ¤nge fГјr die Vorschau";
$MESS["TP_BST_CONVERT_CURRENCY"] = "Preise in einer WГ¤hrung anzeigen";
$MESS["TP_BST_CURRENCY_ID"] = "WГ¤hrung in die die Preise konvertiert werden";
$MESS["TP_BST_PRICE_VAT_INCLUDE"] = "Preis inkl. MwSt.";
?>