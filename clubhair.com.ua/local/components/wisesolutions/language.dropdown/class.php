<?

class LanguageDropdown extends CBitrixComponent
{
    const PATH_TO_UA_ICON = '/upload/language/ua-icon.svg';
    const PATH_TO_RU_ICON = '/upload/language/ru-icon.svg';

    public function executeComponent()
    {
        $this->MakeArResult();
        $this->IncludeComponentTemplate();
    }

    public function MakeArResult() {
        $this->GetSites();
        $this->SetNamesAndPictures();
        $this->MakeCorrectLinks();

        $this->SetCurrentCheckedSite();
    }

    public function GetSites() {
        $by = 'sort';
        $order = 'desc';
        $obSites = CSite::GetList($by, $order);
        while ($arSite = $obSites->fetch()) {
            $this->arResult['SITES_LIST'][$arSite['LID']] = $arSite;
        }
    }

    public function SetNamesAndPictures() {
        foreach ($this->arResult['SITES_LIST'] as $lid => &$arSite) {
            switch ($arSite['DIR']) {
                case '/':
                    $arSite['PICTURE'] = self::PATH_TO_UA_ICON;
                    $arSite['TITLE'] = 'UA';
                    break;
                case '/ru/':
                    $arSite['PICTURE'] = self::PATH_TO_RU_ICON;
                    $arSite['TITLE'] = 'RU';
            }
        }
    }

    private function MakeCorrectLinks()
    {
        $currentPage = substr_replace($_SERVER['REQUEST_URI'], '', 0, strlen(SITE_DIR));
        foreach ($this->arResult['SITES_LIST'] as $lid => &$arSite) {
           $arSite['LINK_TO_CHECKED'] = str_replace("//","/",$arSite["DIR"].$currentPage);
        }
    }

    public function SetCurrentCheckedSite()
    {
        $this->arResult['CHECKED_SITE'] = $this->arResult['SITES_LIST'][SITE_ID];
        unset($this->arResult['SITES_LIST'][SITE_ID]);
    }




}