<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();                        
if(\Bitrix\Main\Loader::includeSharewareModule("krayt.perfumery") == \Bitrix\Main\Loader::MODULE_DEMO_EXPIRED || 
   \Bitrix\Main\Loader::includeSharewareModule("krayt.perfumery") ==  \Bitrix\Main\Loader::MODULE_NOT_FOUND
    )
{ return false;}
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
$letter_l = htmlspecialchars($_GET['L']);
//echo '<pre>'; print_r($arResult); echo '</pre>';
?>
<div class="brand-menu">
    <div class="row" style="position: static;">
        <? if ($arResult['arLetter']): ?>
            <div class="title col-auto">
                <?= GetMessage('TITLE') ?>
            </div>
            <div class="letters-list col pl-0">
                <div class="row" style="position: static;">

                    <div class="letters col">
                        <div class="row" style="position: static;">
                            <? if ($arParams['SHOW_ALL']): ?>
                                <div class="letter-item col-auto show_all">
                                    <a class="letter-link <? if ($letter_l == $key_l): ?>active<? endif ?>"
                                       href="<?= $arResult['SHOW_ALL_URL'] ?>"><?= GetMessage('SHOW_ALL_TEXT') ?></a>
                                    <div class="brand-tab bx-nav-2-lvl-container">
                                        <div class="wrapper-inner">
                                            <div class="brand-tab-wrp">
                                                <div class="brand-tab-list">
                                                    <? foreach ($arResult['arLetter'] as $key_l=>$letter): ?>
                                                        <div class="brand-tab-list-wrapper">
                                                            <div class="brand-item-letter"><?=$key_l?></div>
                                                            <div class="brand-item-list">
                                                                <? foreach ($letter['ITEMS'] as $key=>$brand): ?>
                                                                    <div class="brand-item">
                                                                        <a class="brand-item-link <? if ($brand['HIGHLIGHT']): ?>highlight<? endif; ?>"
                                                                           href="<? echo $arResult['SHOW_ALL_URL'] . $brand['CODE'] . '/';?>">
                                                                            <?= $brand['NAME'] ?>
                                                                        </a>
                                                                    </div>
                                                                <? endforeach ?>
                                                            </div>
                                                        </div>
                                                    <? endforeach; ?>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            <? endif ?>
                            <? foreach ($arResult['arLetter'] as $key_l=>$letter): ?>
                                <div class="letter-item col-auto show_one">
                                    <a class="letter-link <? if ($letter_l == $key_l): ?>active<? endif ?>"
                                       href="<?= $letter['BRAND_URL'] ?>"><?= strtoupper($key_l) ?></a>
                                    <div class="brand-tab bx-nav-2-lvl-container">
                                        <div class="wrapper-inner">
                                            <div class="brand-tab-wrp row">
                                                <div class="col-2 brand-letter-big">
                                                    <span><?=$key_l?></span>
                                                </div>
                                                <div class="col-10 brand-tab-list hidden_list">
                                                    <? foreach ($letter['ITEMS'] as $key=>$brand): ?>
                                                        <div class="brand-item">
                                                            <a class="brand-item-link <? if ($brand['HIGHLIGHT']): ?>highlight<? endif; ?>"
                                                               href="<? echo $arResult['SHOW_ALL_URL'] . $brand['CODE'] . '/';?>"><?= $brand['NAME'] ?></a>
                                                        </div>
                                                    <? endforeach ?>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                            <? endforeach ?>
                        </div>

                        <?/* foreach ($arResult['arLerterE'] as $key_l=>$leter): ?>
                <a class="leter-item <? if ($leter_l == $key_l): ?>active<? endif ?>"
                   href="<?= $leter['BREND_URL'] ?>"><?= strtoupper($key_l) ?></a>
            <? endforeach ?>
            <? if (!empty($arResult['arLerterR'])): ?>
                <? foreach ($arResult['arLerterR'] as $key_l): ?>
                    <a class="leter-item <? if ($leter_l == $key_l): ?>active<? endif ?> "
                       href="<?= $leter['BREND_URL'] ?>"><?= strtoupper($key_l) ?></a>
                <? endforeach ?>
            <? endif */?>
                    </div>
                </div>
            </div>

        <? endif ?>
    </div>
</div>
