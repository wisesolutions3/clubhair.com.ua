<?
if(!defined("B_PROLOG_INCLUDED")||B_PROLOG_INCLUDED!==true)die();
/**
 * Bitrix vars
 *
 * @var array $arParams
 * @var array $arResult
 * @var CBitrixComponentTemplate $this
 * @global CMain $APPLICATION
 * @global CUser $USER
 */
?>
<div class="mfeedback mf-contact">  <!-- вёрстка формы обратного звонка для popup-окна -->
    <div class="container">
        <?
        if(strlen($arResult["OK_MESSAGE"]) > 0)
        {
            ?><div class="mf-ok-text"><?=$arResult["OK_MESSAGE"]?></div><?
        }else{
        ?>
        <div class="mf-title">Заполните форму, <br> мы вам перезвоним</div>
        <?}?>
        <form action="<?=POST_FORM_ACTION_URI?>" method="POST" class="form-feedback <?if(strlen($arResult["OK_MESSAGE"]) > 0){?>form-done<?}?>">
        <?=bitrix_sessid_post()?>

            <div class="form-row row">

                    <div class="mf-item-wrp">
                        <div class="mf-item mf-name">
                            <input type="text" name="user_name" value="<?=$arResult["AUTHOR_NAME"]?>" id="cbf_user_name" class="input__field">
                            <label class="mf-text label__field" for="cbf_user_name">
                                <span>имя</span> <span class="mf-req">*</span></label>
                        </div>
                        <?if (!empty($arResult["ERROR_MESSAGE"]["NO_NAME"])) {?>
                            <span class="mf-error"><?=$arResult["ERROR_MESSAGE"]["NO_NAME"]?></span>
                        <?}?>
                    </div>

                    <div class="mf-item-wrp">
                        <div class="mf-item mf-email">
                            <input type="text" name="user_email" value="<?=$arResult["AUTHOR_EMAIL"]?>" id="cbf_user_email" class="input__field">
                            <label class="mf-text label__field" for="cbf_user_email">
                                <span>email</span> <span class="mf-req">*</span></label>
                        </div>
                        <?if (!empty($arResult["ERROR_MESSAGE"]["NO_EMAIL"])) {?>
                            <span class="mf-error"><?=$arResult["ERROR_MESSAGE"]["NO_EMAIL"]?></span>
                        <?}?>
                    </div>

                    <div class="mf-item-wrp">
                        <div class="mf-item mf-phone">
                            <input type="text" name="user_phone" value="<?=$arResult["AUTHOR_PHONE"]?>" id="cbf_user_phone" class="input__field phone">
                            <label class="mf-text label__field" for="cbf_user_phone">
                                <span>телефон</span> <span class="mf-req">*</span></label>
                        </div>
                        <?if (!empty($arResult["ERROR_MESSAGE"]["NO_PHONE"])) {?>
                            <span class="mf-error"><?=$arResult["ERROR_MESSAGE"]["NO_PHONE"]?></span>
                        <?}?>
                    </div>

                    <?if($arParams["USE_CAPTCHA"] == "Y"):?>
                        <div class="mf-captcha">
                            <div class="mf-text"><?=GetMessage("MFT_CAPTCHA")?></div>
                            <input type="hidden" name="captcha_sid" value="<?=$arResult["capCode"]?>">
                            <img src="/bitrix/tools/captcha.php?captcha_sid=<?=$arResult["capCode"]?>" width="180" height="40" alt="CAPTCHA">
                            <div class="mf-text"><?=GetMessage("MFT_CAPTCHA_CODE")?><span class="mf-req">*</span></div>
                            <input type="text" name="captcha_word" size="30" maxlength="50" value="">
                        </div>
                    <?endif;?>
                    <input type="hidden" name="PARAMS_HASH" value="<?=$arResult["PARAMS_HASH"]?>">

                    <div class="mf-button">
                        <input type="submit" name="submit" value="Оставить заявку" class="red-btn">
                    </div>
            </div>
        </form>
    </div></div>


<script>

    $(".input__field.phone").mask("+7 (999) 999-99-99", {
        placeholder: "+7 (___) ___-__-__"
    });

    (function() {
        // trim polyfill : https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/String/Trim
        if (!String.prototype.trim) {
            (function() {
                // Make sure we trim BOM and NBSP
                var rtrim = /^[\s\uFEFF\xA0]+|[\s\uFEFF\xA0]+$/g;
                String.prototype.trim = function() {
                    return this.replace(rtrim, '');
                };
            })();
        }

        [].slice.call( document.querySelectorAll( 'input.input__field' ) ).forEach( function( inputEl ) {
            // in case the input is already filled..
            if( inputEl.value.trim() !== '' ) {
                classie.add( inputEl.parentNode, 'input--filled' );
            }

            // events:
            inputEl.addEventListener( 'focus', onInputFocus );
            inputEl.addEventListener( 'blur', onInputBlur );
        } );

        function onInputFocus( ev ) {
            classie.add( ev.target.parentNode, 'input--filled' );
        }

        function onInputBlur( ev ) {
            if( ev.target.value.trim() === '' ) {
                classie.remove( ev.target.parentNode, 'input--filled' );
            }
        }
    })();
</script>