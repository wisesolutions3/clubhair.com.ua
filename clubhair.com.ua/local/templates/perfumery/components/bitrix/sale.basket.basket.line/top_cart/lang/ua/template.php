<?
$MESS["TSB1_2ORDER"] = "Оформити замовлення";
$MESS["TSB1_BTN_BASKET"] = "Перейти до кошику";
$MESS["TSB1_BTN_CLOSE"] = "продовжити покупки";
$MESS["TSB1_CART"] = "Кошик";
$MESS["TSB1_COLLAPSE"] = "приховати";
$MESS["TSB1_DELAY"] = "відкладені товари";
$MESS["TSB1_DELETE"] = "видалити";
$MESS["TSB1_EXPAND"] = "розкрити";
$MESS["TSB1_LOGIN"] = "Увійти";
$MESS["TSB1_LOGOUT"] = "вийти";
$MESS["TSB1_NOTAVAIL"] = "недоступні товари";
$MESS["TSB1_PERSONAL"] = "Персональний розділ";
$MESS["TSB1_READY"] = "Готові до покупки товари";
$MESS["TSB1_REGISTER"] = "Реєстрація";
$MESS["TSB1_SUBSCRIBE"] = "підписані товари";
$MESS["TSB1_SUM"] = "на суму";
$MESS["TSB1_TOTAL_PRICE"] = "Разом:";
$MESS["TSB1_YOUR_CART"] = "Ваша корзина";
?>