<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();                        
if(\Bitrix\Main\Loader::includeSharewareModule("krayt.perfumery") == \Bitrix\Main\Loader::MODULE_DEMO_EXPIRED || 
   \Bitrix\Main\Loader::includeSharewareModule("krayt.perfumery") ==  \Bitrix\Main\Loader::MODULE_NOT_FOUND
    )
{ return false;}
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */

$this->setFrameMode(true);

if (empty($arResult["ALL_ITEMS"]))
	return;

CUtil::InitJSCore();

if (file_exists($_SERVER["DOCUMENT_ROOT"].$this->GetFolder().'/themes/'.$arParams["MENU_THEME"].'/colors.css'))
	$APPLICATION->SetAdditionalCSS($this->GetFolder().'/themes/'.$arParams["MENU_THEME"].'/colors.css');

$menuBlockId = "catalog_menu_".$this->randString();
?>
<div class="bx-top-nav bx-<?=$arParams["MENU_THEME"]?>" id="<?=$menuBlockId?>">
	<nav class="bx-top-nav-container" id="cont_<?=$menuBlockId?>">
		<ul class="bx-nav-list-1-lvl clearfix" id="ul_<?=$menuBlockId?>">
		<?foreach($arResult["MENU_STRUCTURE"] as $itemID => $arColumns):?>     <!-- first level-->
			<?$existPictureDescColomn = ($arResult["ALL_ITEMS"][$itemID]["PARAMS"]["picture_src"] || $arResult["ALL_ITEMS"][$itemID]["PARAMS"]["description"]) ? true : false;?>
			<li
				class="bx-nav-1-lvl bx-nav-list-<?=($existPictureDescColomn) ? count($arColumns)+1 : count($arColumns)?>-col <?if($arResult["ALL_ITEMS"][$itemID]["SELECTED"]):?>bx-active<?endif?><?if (is_array($arColumns) && count($arColumns) > 0):?> bx-nav-parent<?endif?>"
				<?if (is_array($arColumns) && count($arColumns) > 0):?>
					data-role="bx-menu-item"
				<?endif?>>
				<a
					class=" bx-nav-1-lvl-link <?= ((is_array($arColumns) && count($arColumns) > 0) ? 'hidden-xs' : '');?>"
					href="<?=$arResult["ALL_ITEMS"][$itemID]["LINK"]?>"
                    <?if (is_array($arColumns) && count($arColumns) > 0 && $existPictureDescColomn):?>
                        onmouseover="window.obj_<?=$menuBlockId?> && obj_<?=$menuBlockId?>.changeSectionPicure(this, '<?=$itemID?>');"
                    <?endif?>>
					<span class="bx-nav-1-lvl-link-text">
						<?=htmlspecialcharsbx($arResult["ALL_ITEMS"][$itemID]["TEXT"])?>
<!--						--><?//if (is_array($arColumns) && count($arColumns) > 0):?><!--<i class="fa fa-angle-down"></i>--><?//endif?>
					</span>
				</a>
			<?if (is_array($arColumns) && count($arColumns) > 0):?>
				<div class="bx-nav-2-lvl-container">
                    <div class="wrapper-inner">
                        <?foreach($arColumns as $key=>$arRow):
                            $menuMobIdLevel_1 = "cat_mobile_".randString(3);?>
                            <div class="mobile-menu-title">
                                <a class=" bx-nav-1-lvl-link"
                                   href="<?=$arResult["ALL_ITEMS"][$itemID]["LINK"]?>">
                                    <span class="bx-nav-1-lvl-link-text">
                                        <?=htmlspecialcharsbx($arResult["ALL_ITEMS"][$itemID]["TEXT"])?>
                                    </span>
                                </a>
                                <button class="open_list mini-plus visible-xs" data-id="<?=$menuMobIdLevel_1;?>"></button> <!-- for mobile -->
                            </div>
                            <ul class="bx-nav-list-2-lvl hidden_list col-9 p-0" id="<?=$menuMobIdLevel_1;?>">
                                <?foreach($arRow as $itemIdLevel_2=>$arLevel_3):
                                    $menuMobIdLevel_2 = "cat_mobile_".randString(3);?>  <!-- second level-->
                                    <li class="bx-nav-2-lvl <?if (is_array($arLevel_3) && count($arLevel_3) > 0){?>parent-column<?}?>">
                                        <a class=" bx-nav-2-lvl-link"
                                           href="<?=$arResult["ALL_ITEMS"][$itemIdLevel_2]["LINK"]?>"
                                            <?if ($existPictureDescColomn):?>
                                                onmouseover="window.obj_<?=$menuBlockId?> && obj_<?=$menuBlockId?>.changeSectionPicure(this, '<?=$itemIdLevel_2?>');"
                                            <?endif?>
                                           data-picture="<?=$arResult["ALL_ITEMS"][$itemIdLevel_2]["PARAMS"]["picture_src"]?>"
                                           <?if($arResult["ALL_ITEMS"][$itemIdLevel_2]["SELECTED"]):?>class="bx-active"<?endif?>
                                        >
                                            <span class="bx-nav-2-lvl-link-text"><?=$arResult["ALL_ITEMS"][$itemIdLevel_2]["TEXT"]?></span>
                                        </a>
                                        <?if (is_array($arLevel_3) && count($arLevel_3) > 0):?>
                                            <div class="mobile-menu-title">
                                                <a class=" bx-nav-2-lvl-link"
                                                   href="<?=$arResult["ALL_ITEMS"][$itemIdLevel_2]["LINK"]?>"
                                                   <?if($arResult["ALL_ITEMS"][$itemIdLevel_2]["SELECTED"]):?>class="bx-active"<?endif?>>
                                                    <span class="bx-nav-2-lvl-link-text"><?=$arResult["ALL_ITEMS"][$itemIdLevel_2]["TEXT"]?></span>
                                                </a>
                                                <button class="open_list fa fa-angle-down visible-xs" data-id="<?=$menuMobIdLevel_2;?>"></button> <!-- for mobile -->
                                            </div>
                                            <ul class="bx-nav-list-3-lvl hidden_list" id="<?=$menuMobIdLevel_2;?>">
                                                <?foreach($arLevel_3 as $itemIdLevel_3):?>	<!-- third level-->
                                                    <li class="bx-nav-3-lvl">
                                                        <a
                                                                class=" bx-nav-3-lvl-link"
                                                                href="<?=$arResult["ALL_ITEMS"][$itemIdLevel_3]["LINK"]?>"
                                                                <?if($arResult["ALL_ITEMS"][$itemIdLevel_3]["SELECTED"]):?>class="bx-active"<?endif?>
                                                        >
                                                            <span class="bx-nav-3-lvl-link-text"><?=$arResult["ALL_ITEMS"][$itemIdLevel_3]["TEXT"]?></span>
                                                        </a>
                                                    </li>
                                                <?endforeach;?>
                                            </ul>
                                        <?endif?>
                                    </li>
                                <?$i++;
                                endforeach;?>
                            </ul>
                        <?endforeach;?>
                        <?if ($existPictureDescColomn):?>
                            <div class="bx-nav-list-2-lvl bx-nav-catinfo dbg col-3" data-role="desc-img-block">
<!--                                <pre>--><?//=print_r($arResult["ALL_ITEMS"][$itemID]);?><!--</pre>-->
                                <a class="bx-nav-2-lvl-link-image" href="<?=$arResult["ALL_ITEMS"][$itemID]["LINK"]?>">
<!--                                    <span class="img-bg" -->
<!--                                          style="background-image: url(<?//=$arResult["ALL_ITEMS"][$itemID]["PARAMS"]["picture_src"]?>);"></span-->
                                    <img src="<?=$arResult["ALL_ITEMS"][$itemID]["PARAMS"]["picture_src"]?>" alt="">
                                </a>
<!--                                <p>--><?//=$arResult["ALL_ITEMS"][$itemID]["PARAMS"]["description"]?><!--</p>-->
                            </div>
                        <?endif?>
                    </div>
				</div>
			<?endif?>
			</li>
		<?endforeach;?>
            <li class="more bx-nav-1-lvl bx-nav-list-2-col bx-nav-parent">
                <span class="more_dot">...</span>
                <div class="bx-nav-2-lvl-container">
                    <div class="wrapper-inner">
                        <ul id="overflow" class="bx-nav-list-2-lvl"></ul>
                    </div>
                </div>
            </li>
		</ul>
	</nav>
</div>

<script>
    BX.ready(function () {
        window.obj_<?=$menuBlockId?> = new BX.Main.Menu.CatalogHorizontal('<?=CUtil::JSEscape($menuBlockId)?>', <?=CUtil::PhpToJSObject($arResult["ITEMS_IMG_DESC"])?>);
    });
</script>