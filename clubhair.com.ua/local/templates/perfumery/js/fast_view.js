/**
 * Created by aleksander on 05.03.2019.
 */

$(function () {


    $(document).trigger("OnAddProDuctFavorites");
    $("a.rating_link").click(function (e) {

        e.preventDefault();

        var id  = $(this).attr("href"),
            h = $('header').innerHeight(),
            top = $(id).offset().top - h;
        $("html,body, .product_card_fast.fast-in").animate({scrollTop: top}, 600);

        return false;

    });

    if ($(window).width() > 1024) {

        var gallery_slider;

        $('.product-item-detail-slider-controls-block .swiper-container').on("mouseenter", function (e) {
            if ($(this).hasClass('has-slider')) {
                return;
            }
            var x = document.getElementsByClassName("product-item-detail-slider-controls-block");
            for(var i = 0; i < x.length; i++) {
                var el = x[i];
                var nx = el.querySelector(".swiper-next");
                var pr = el.querySelector(".swiper-prev");
                var sliderId = $(this).attr("data-id");
                // console.log(nx);
                $(this).addClass('has-slider');
                gallery_slider = new Swiper('.' + sliderId, {
                    direction: 'vertical',
                    slidesPerView: 3,
                    spaceBetween: 13,
                    navigation: {
                        nextEl: nx,
                        prevEl: pr,
                    },
                    watchOverflow: true
                });
            }
        });

        $('.product-item-detail-slider-images-container').fancybox({
            selector: 'a.fancybox-prev',
            hash: false,
            buttons: [
                'close'
            ]
        });

    }

    $('.slider_in_fast .owl-carousel').owlCarousel({
        items: 4,
        margin: -1,
        dots: true,
        nav: true,
        navSpeed: 1000,
        lazyContent: true,
        responsive: {
            0: {
                items: 3,
                margin: 5,
                dots: true,
                dotsSpeed: 1000
            },
            415: {
                items: 2,
                margin: 15
            },
            569: {
                items: 3,
                margin: 20
            },
            1025: {
                items: 3
            },
            1440: {
                items: 4
            }
        }
    });


// region scroll-bar
    $(".stores-list").niceScroll({
        cursorcolor: "#d8d8d8",
        cursorborder: 0,
        cursorborderradius: 2,
        zindex: 999,
        cursoropacitymax: 1
    });
// endregion
});