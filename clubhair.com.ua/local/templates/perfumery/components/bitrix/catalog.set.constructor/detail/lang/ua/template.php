<?
$MESS["CATALOG_SET_ADDED2BASKET"] = "Набір був доданий в кошик";
$MESS["CATALOG_SET_BUTTON_ADD"] = "Додати";
$MESS["CATALOG_SET_BUTTON_BUY"] = "Перейти до кошику";
$MESS["CATALOG_SET_BUY"] = "купити";
$MESS["CATALOG_SET_BUY_SET"] = "купити набір";
$MESS["CATALOG_SET_CONSTRUCT"] = "Скласти свій набір";
$MESS["CATALOG_SET_DISCOUNT"] = "знижка";
$MESS["CATALOG_SET_DISCOUNT_DIFF"] = "(Економія #PRICE#)";
$MESS["CATALOG_SET_ECONOMY_PRICE"] = "економія";
$MESS["CATALOG_SET_IN_CART"] = "Товари в кошику";
$MESS["CATALOG_SET_MESS_NOT_AVAILABLE"] = "Немає в наявності";
$MESS["CATALOG_SET_POPUP_DESC"] = "Перетягніть будь-який товар за допомогою мишки в порожнє поле або просто натисніть на нього";
$MESS["CATALOG_SET_POPUP_LOADER"] = "Завантаження ...";
$MESS["CATALOG_SET_POPUP_TITLE"] = "Додайте аксесуар в набір";
$MESS["CATALOG_SET_POPUP_TITLE_BAR"] = "Збери власний набір";
$MESS["CATALOG_SET_PRODUCTS_PRICE"] = "Вартість товарів окремо";
$MESS["CATALOG_SET_SET_PRICE"] = "вартість набору";
$MESS["CATALOG_SET_SUM"] = "Разом";
$MESS["CATALOG_SET_WITHOUT_DISCOUNT"] = "Ціна без знижки";
$MESS["CT_BCE_CATALOG_MESS_EMPTY_SET"] = "У наборі відсутні товари, виберіть із запропонованого списку";
$MESS["SETS_TITLE"] = "придбати набір";
?>