<? if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();

/** @var array $arResult */

$arResult['DETAIL_TEXT'] = html_entity_decode($arResult['DETAIL_TEXT']);
$ar_offer_ton_img = [];
$ar_offer_ton_articles = [];

foreach ($arResult["OFFERS"] as $key => &$OFFER){
    if(!empty($OFFER['PROPERTIES']['TON']['VALUE_ENUM_ID'])) {
        if(!empty($OFFER['DETAIL_PICTURE'])) {
            $ar_offer_ton_img[$OFFER['PROPERTIES']['TON']['VALUE_ENUM_ID']] = $OFFER['DETAIL_PICTURE'];
        }

        if(!empty($OFFER['PROPERTIES']['CML2_ARTICLE']['VALUE'])) {
            $ar_offer_ton_articles[$OFFER['PROPERTIES']['TON']['VALUE_ENUM_ID']] = $OFFER['PROPERTIES']['CML2_ARTICLE']['VALUE'];
        }
    }
    
    if($OFFER["DETAIL_TEXT"]=="" && $arResult["DETAIL_TEXT"]!=""){
        $OFFER["DETAIL_TEXT"]=$arResult["DETAIL_TEXT"];
    }
    foreach ($arResult["DISPLAY_PROPERTIES"] as $code => $PROPERTY){
        if($OFFER["DISPLAY_PROPERTIES"][$code]["DISPLAY_VALUE"]=="" && $PROPERTY["DISPLAY_VALUE"]!=''){
            $OFFER["DISPLAY_PROPERTIES"][$code]=$PROPERTY;
        }
    }

}

$arResult['OFFERS_TON_IMG'] = $ar_offer_ton_img;
$arResult['OFFERS_TON_ARTICLE'] = $ar_offer_ton_articles;

$haveOffers = !empty($arResult['OFFERS']);
if ($haveOffers)
{
    $actualItem = isset($arResult['OFFERS'][$arResult['OFFERS_SELECTED']])
        ? $arResult['OFFERS'][$arResult['OFFERS_SELECTED']]
        : reset($arResult['OFFERS']);
    $showSliderControls = false;
    
    foreach ($arResult['OFFERS'] as $offer)
    {
        if ($offer['MORE_PHOTO_COUNT'] > 1)
        {
            $showSliderControls = true;
            break;
        }
    }
}
else
{
    $actualItem = $arResult;
    $showSliderControls = $arResult['MORE_PHOTO_COUNT'] > 1;
}

$price = $actualItem['ITEM_PRICES'][$actualItem['ITEM_PRICE_SELECTED']];


$arResult['ANALITICS'] = array(
    'ID' => $arResult['ID'],
    'NAME' => $arResult['NAME'],
    'CATEGORY' => CIblockSection::GetByID($arResult['IBLOCK_SECTION_ID'])->fetch()['NAME'],
    'PRICE' => $price['PRICE'],
);


$component = $this->getComponent();
$arParams = $component->applyTemplateModifications();

$ton_prop_code = LANGUAGE_ID == 'ua' ? 'PROP_74' : 'PROP_75';

$ar_ton_values = [];
foreach ($arResult["JS_OFFERS"] as &$JSOFFER) {
    if(isset($JSOFFER['TREE'][$ton_prop_code])) {
        foreach ($JSOFFER['TREE'] as $prop_code => $prop_value_id) {
            if($prop_code != $ton_prop_code) {
                $ar_ton_values[$prop_value_id][] = $JSOFFER['TREE'][$ton_prop_code];
            }
        }
    }
    
    foreach ($arResult["OFFERS"] as $OFFER) {
        if($JSOFFER["ID"]==$OFFER["ID"]){
            $JSOFFER["DETAIL_TEXT"]=$OFFER["DETAIL_TEXT"];
        }
    }
}

if(!empty($ar_ton_values)) {
    $arResult['OFFERS_TON_VALUES_CUSTOM'] = $ar_ton_values;
}

$component->setResultCacheKeys(array('ANALITICS'));

