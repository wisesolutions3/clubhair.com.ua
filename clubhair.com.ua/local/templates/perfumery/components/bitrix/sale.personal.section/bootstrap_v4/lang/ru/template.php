<?php
$MESS["SPS_ACCOUNT_PAGE_NAME"] = "Личный счет";
$MESS["SPS_PERSONAL_PAGE_NAME"] = "Профиль";
$MESS["SPS_PROFILE_PAGE_NAME"] = "Профили заказов";
$MESS["SPS_ORDER_PAGE_NAME"] = "Заказы";
$MESS["SPS_ORDER_PAGE_HISTORY"] = "История заказов";
$MESS["SPS_SUBSCRIBE_PAGE_NAME"] = "Подписки";
$MESS["SPS_BASKET_PAGE_NAME"] = "Корзина";
$MESS["SPS_CONTACT_PAGE_NAME"] = "Контакты";
$MESS["SPS_ERROR_NOT_CHOSEN_ELEMENT"] = "Отсутвуют выбранные элементы";
$MESS["SPS_CHAIN_MAIN"] = "Мой кабинет";
$MESS["SPS_TITLE_MAIN"] = "Персональный раздел";



$MESS["AUTH_PLEASE_AUTH"] = "Пожалуйста, авторизуйтесь для входа на сайт";
$MESS["AUTH_LOGIN"] = "Логин";
$MESS["AUTH_PASSWORD"] = "Пароль";
$MESS["AUTH_REMEMBER_ME"] = "Запомнить меня на этом компьютере";
$MESS["AUTH_AUTHORIZE"] = "Войти";
$MESS["AUTH_REGISTER"] = "Регистрация";
$MESS["AUTH_FIRST_ONE"] = "Если вы впервые на сайте, заполните, пожалуйста, регистрационную форму.";
$MESS["AUTH_FORGOT_PASSWORD_2"] = "Забыли свой пароль?";
$MESS["AUTH_CAPTCHA_PROMT"] = "Введите слово на картинке";
$MESS["AUTH_TITLE"] = "Авторизация";
$MESS["AUTH_SECURE_NOTE"] = "Перед отправкой формы авторизации пароль будет зашифрован в браузере. Это позволит избежать передачи пароля в открытом виде.";
$MESS["AUTH_NONSECURE_NOTE"] = "Пароль будет отправлен в открытом виде. Включите JavaScript в браузере, чтобы зашифровать пароль перед отправкой.";


