<?

class CSaleHandlers
{
    const NEW_POST_IBLOCK_ID = 7;

    public static function OnSaleOrderBeforeSaved($event)
    {
        if (strpos($_SERVER['REQUEST_URI'], '/admin/') <= 0) {
            self::UpdateNewPostInfo($event);
        }
    }

    private static function UpdateNewPostInfo($event)
    {
        $arParam = $event->getParameters();

        $order = $arParam['ENTITY'];
        $orderId = $order->getId();

        $propertyCollection = $order->getPropertyCollection();
        $properties = $propertyCollection->getArray()['properties'];

        foreach ($properties as $key => $ar) {
            $propertiesCodeValue[$ar['CODE']] = $ar;
        }

        if (empty($orderId)) {
            $city = $propertyCollection->getItemByOrderPropertyId($propertiesCodeValue['CITY_WS']['ID']);
            $department = $propertyCollection->getItemByOrderPropertyId($propertiesCodeValue['DEPARTMENT_WS']['ID']);
            if ($city != NULL) {
                $cityId = $city->getValue();
            }


            if ($department != NULL) {
                $departmentId = $department->getValue();
            }

            if (!empty($cityId)) {
                if (is_numeric($cityId)) {
                    $dbCity = CIBlockSection::GetList([], ['IBLOCK_ID' => self::NEW_POST_IBLOCK_ID, 'ACTIVE' => 'Y', 'ID' => $cityId], false, false, ['NAME', 'XML_ID']);
                    while ($ar_result = $dbCity->GetNext()) {
                        $cityName = $ar_result['NAME'];

                        $city->setValue($cityName);
                    }
                }

                if (is_numeric($departmentId)) {
                    $dbDepartment = CIBlockElement::GetList([], ['IBLOCK_ID' => self::NEW_POST_IBLOCK_ID, 'ACTIVE' => 'Y', 'IBLOCK_SECTION_ID' => $cityId, 'ID' => $departmentId], false, false,
                        ['ID', 'NAME', 'XML_ID']);
                    while ($ar_result = $dbDepartment->GetNext()) {
                        $departmentName = $ar_result['NAME'];
                        $department->setValue($departmentName);
                    }
                }
            }
        }
    }

    public static function OnSaleStatusOrderChangeHandler($event) {
        $order = $event->getParameter("ENTITY");
        $status = $order->getField('STATUS_ID');
        $propertyCollection = $order->getPropertyCollection();

        $user_phone = $propertyCollection->getPhone()->getValue();


        $sms_templates_db = \Bitrix\Main\Sms\TemplateTable::getList([
            'filter' => [
                'EVENT_NAME' => ['SMS_SALE_NEW_ORDER', 'SMS_ORDER_DONE']
            ]
        ]);

        $ar_sms_templates = [];
        while($sms_templates = $sms_templates_db->fetch()) {
            $ar_sms_templates[$sms_templates['EVENT_NAME']] = $sms_templates['MESSAGE'];
        }


        switch ($status) {
            case 'N':
                $turbo_sms = new TurboSMSHandler($user_phone, $ar_sms_templates['SMS_SALE_NEW_ORDER']);
                $turbo_sms->SendTurboSMS();
                break;
            case 'F':
                $turbo_sms = new TurboSMSHandler($user_phone, $ar_sms_templates['SMS_ORDER_DONE']);
                $turbo_sms->SendTurboSMS();
                break;
        }
    }
}