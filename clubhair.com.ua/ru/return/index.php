<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Возврат");
echo ' <div class="static_page">'?><div style="text-align: justify;">
<br>
 <span style="font-size: 20pt;"><b>Обмен и возврат товара</b></span><br>
 <br>
 <span style="font-size: 10pt;">В соответствии с Законом Украины «О защите прав потребителей» от 12.05.1991 и Постановлением Кабинета Министров Украины № 172 от 19.03.1994 парфюмерно-косметические товары входят в перечень непродовольственных товаров надлежащего качества, которые не подлежат возврату/обмену.</span><br>
 <span style="font-size: 10pt;"> </span><br>
 <span style="font-size: 10pt;">
	Мы дорожим своей репутацией. Все товары, представленные в нашем интернет-магазине, являются оригинальными. Покупая у нас, Вы защищаете себя от некачественного или просроченного товара и гарантированно получаете оригинальный, качественный и безопасный товар.</span><br>
 <span style="font-size: 10pt;"> </span><br>
 <span style="font-size: 10pt;">
	Обмен/возврат товара происходит исключительно в случае: </span><br>
 <span style="font-size: 10pt;"> </span><b><span style="font-size: 10pt;">-</span></b><span style="font-size: 10pt;"> выявления заводского дефекта товара (например, не работает распылитель, поврежден флакон);</span><br>
 <span style="font-size: 10pt;"> </span><b><span style="font-size: 10pt;">-</span></b><span style="font-size: 10pt;"> получения просроченного товара;</span><br>
 <span style="font-size: 10pt;"> </span><b><span style="font-size: 10pt;">-</span></b><span style="font-size: 10pt;"> получения товара, который не соответствует заказу (пересортица). </span><br>
 <span style="font-size: 10pt;"> </span><br>
 <span style="font-size: 10pt;">
	Для обмена/возврата товара в случаях упомянутых выше, пожалуйста обращайтесь по адресу </span><u><a href="mailto:clubhairua@gmail.com"><span style="font-size: 10pt;">clubhairua@gmail.com</span></a></u><span style="font-size: 10pt;"> или по телефону </span><u><span style="font-size: 10pt;">(050) 323 66 48</span></u><span style="font-size: 10pt;">, и наши специалисты помогут Вам решить проблему в кратчайшие сроки.</span><br>
 <span style="font-size: 10pt;"> </span><br>
 <span style="font-size: 10pt;">
	Если во время «осмотра отправления» в присутствии менеджера/курьера службы доставки окажется, что доставленный товар просроченный, имеет дефект или не соответствует заказу (пересортица), Вы имеете право отказаться от получения отправления. В этом случае мы произведем замену товара или вернем деньги уплаченные за товар, на Ваш выбор. В этом случае возможен возврат не отдельных позиций, а всего отправления. После оплаты и получения отправления, распакованную коробку с отправлением вернуть менеджеру/курьеру службы доставки уже будет невозможно. </span>
</div>
 <br><?echo "</div>"; require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>