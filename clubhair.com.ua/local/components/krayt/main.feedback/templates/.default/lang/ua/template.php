<?
$MESS['MFT_NAME'] = "Ваше ім'я";
$MESS['MFT_EMAIL'] = "E-mail";
$MESS['MFT_PHONE'] = "Телефон";
$MESS['MFT_MESSAGE'] = "Ваше повідомлення";
$MESS['MFT_CAPTCHA'] = "Захист від автоматичних повідомлень";
$MESS['MFT_CAPTCHA_CODE'] = "Введіть слово на картинці";
$MESS['MFT_SUBMIT'] = "Відправити повідомлення";
$MESS['MF_CAPTHCA_EMPTY'] = "Не вказаний код захисту від автоматичних повідомлень.";
?>