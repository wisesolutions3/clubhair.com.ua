$(function () {

    $('.bx-nav-1-lvl').hover(
        function () {
            $('.top-nav-wrapper').addClass("hover-menu");
        },
        function () {
            $('.top-nav-wrapper').removeClass("hover-menu");
        }
    );


});

BX.namespace("BX.Main.Menu");
BX.Main.Menu.CatalogHorizontal = (function()
{
    var CatalogHorizontal = function(menuBlockId, itemImgDesc)
    {
        this.menuBlockId = menuBlockId || "";
        this.itemImgDesc = itemImgDesc || "";
    };

    CatalogHorizontal.prototype.changeSectionPicure = function(element, itemId)
    {
        var curLi = BX.findParent(element, {className: "bx-nav-1-lvl"});
        if (!curLi)
            return;

        var imgDescObj = curLi.querySelector("[data-role='desc-img-block']");
        if (!imgDescObj)
            return;

        var imgObj = BX.findChild(imgDescObj, {tagName: "img"}, true, false);
        if (imgObj)
            imgObj.src = this.itemImgDesc[itemId]["PICTURE"];

        var linkObj = BX.findChild(imgDescObj, {tagName: "a"}, true, false);
        if (linkObj)
            linkObj.href = element.href;

        var descObj = BX.findChild(imgDescObj, {tagName: "p"}, true, false);
        if (descObj)
            descObj.innerHTML = this.itemImgDesc[itemId]["DESC"];

    };

    return CatalogHorizontal;

})();


