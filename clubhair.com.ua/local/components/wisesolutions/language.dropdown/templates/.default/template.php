<?
foreach ($arResult['SITES_LIST'] as $side_id => $site) {
    if($_SERVER['SERVER_NAME'] != $site['SERVER_NAME']) {
        unset($arResult['SITES_LIST'][$side_id]);
    }
}
?>
<div class="language">
    <a href="<?=$arResult['CHECKED_SITE']['LINK_TO_CHECKED']?>" class="language-btn">
        <img src="<?=$arResult['CHECKED_SITE']['PICTURE']?>"><?=$arResult['CHECKED_SITE']['TITLE']?>
    </a>
    <div class="language-dropdown">
        <?foreach ($arResult['SITES_LIST'] as $lid => $arSite) {?>
            <a href="<?=$arSite['LINK_TO_CHECKED']?>" >
                <img src="<?=$arSite['PICTURE']?>"><?=$arSite['TITLE']?>
            </a>
        <?}?>
    </div>
</div>