<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Title");
$APPLICATION->SetPageProperty("description", "CLUBHAIR: интернет-магазин профессиональной косметики и профессиональных средств по уходу за волосами");
$APPLICATION->SetPageProperty("keywords", "CLUBHAIR: интернет-магазин профессиональной косметики и профессиональных средств по уходу за волосами");
$APPLICATION->SetPageProperty("title", "Олата и доставка - CLUBHAIR: интернет-магазин профессиональной косметики и профессиональных средств по уходу за волосами");
$APPLICATION->SetTitle("Олата и доставка");
echo ' <div class="static_page">'?><div style="text-align: justify;">
 <br>
 <span style="font-size: 20pt;"><b>Способы доставки</b></span><br>
 <br>
 <span style="font-size: 14pt;">Адресная доставка курьером «Нова Пошта»</span><br>
 <br>
 <span style="font-size: 10pt;">Доставка возможна в населенные пункты, где есть отделения службы доставки «Нова Пошта». На данный момент доставка бесплатная. Минимальная сумма заказа отсутствует. Заказы доставляются до 3 рабочих дней. Конкретное время зависит от региона и графика работы службы доставки «Нова Пошта». Перед доставкой менеджер свяжется с Вами, чтобы подтвердить заказ и детали доставки. Просим быть на связи.<br>
	 ВАЖНО! Чтобы получить заказ, нужно обязательно иметь с собой удостоверение личности. Получить заказ может только тот человек, который указан как получатель. У курьера заказ хранится в течение 5 дней, затем возвращается обратно.</span><br>
 <br>
 <span style="font-size: 14pt;">Адресная доставка курьером Clubhair по Киеву, Одессе и Харькову</span><br>
 <br>
 <span style="font-size: 10pt;">Доставка осуществляется с понедельника по субботу с 13:00 до 18:00. Доставка бесплатная. Минимальная сумма заказа отсутствует. Заказы доставляются до 2-х рабочих дней. Перед доставкой менеджер свяжется с Вами, чтобы подтвердить заказ и детали доставки. Просим быть на связи.<br>
	 ВАЖНО! Чтобы получить заказ, нужно обязательно иметь с собой удостоверение личности. Получить заказ может только тот человек, который указан как получатель. У курьера заказ хранится в течение 2 дней, затем возвращается обратно.</span><br>
 <br>
 <span style="font-size: 14pt;">Доставка в отделение «Міст Експрес»</span><br>
 <br>
 <span style="font-size: 10pt;">Доставка возможна в населенные пункты, где есть отделения службы доставки «Міст Експрес». На данный момент доставка бесплатная. Минимальная сумма заказа отсутствует. Заказы доставляются до 3-х рабочих дней. Конкретное время зависит от региона и графика работы службы доставки «Міст Експрес». Перед доставкой менеджер свяжется с Вами, чтобы подтвердить заказ и детали доставки. Просим быть на связи.<br>
	 ВАЖНО! Чтобы получить заказ, нужно обязательно иметь с собой удостоверение личности. Получить заказ может только тот человек, который указан как получатель. В отделении заказ хранится в течение 5 дней, затем возвращается обратно.</span><br>
 <br>
 <span style="font-size: 14pt;">Доставка в отделение «Нова Пошта»</span><br>
 <br>
 <span style="font-size: 10pt;">Доставка возможна в населенные пункты, где есть отделения службы доставки «Нова Пошта». На данный момент доставка бесплатная. Минимальная сумма заказа отсутствует. Заказы доставляются до 3-х рабочих дней. Конкретное время зависит от региона и графика работы службы доставки «Нова Пошта». Перед доставкой менеджер свяжется с Вами, чтобы подтвердить заказ и детали доставки. Просим быть на связи.<br>
	 ВАЖНО! Чтобы получить заказ, нужно обязательно иметь с собой удостоверение личности. Получить заказ может только тот человек, который указан как получатель. В отделении заказ хранится в течение 5 дней, затем возвращается обратно.</span><br>
 <br>
 <span style="font-size: 14pt;">Обратите внимание!</span><br>
 <br>
 <span style="font-size: 10pt;">Срок доставки считается с момента обработки заказа. Заказ обрабатываются только в рабочие дни. Заказы, полученные и подтвержденные до 12:00, обрабатываются в тот же день. Заказы, полученные и подтвержденные после 12:00, обрабатываются на следующий рабочий день.</span><br>
 <br>
 <br>
 <span style="font-size: 20pt;"><b>Способы оплаты</b></span><br>
 <br>
 <span style="font-size: 14pt;">Платежная карта</span><br>
 <br>
 <span style="font-size: 10pt;">Вы можете полностью оплатить заказ при его оформлении в нашем интернет-магазине банковской платежной картой Visa/MasterCard. Данный способ оплаты позволит сэкономить время при получении заказа. При встрече с курьером, менеджерами отделения службы доставки Вам необходимо будет просто забрать заказ, предварительно предоставив документ, подтверждающий личность получателя.</span><br>
 <br>
 <span style="font-size: 14pt;">Наложенный платеж</span><br>
 <br>
 <span style="font-size: 10pt;">Вы можете произвести оплату за заказ при его получении. Службы доставки «Нова Пошта» и «Міст Експрес» взимают дополнительную комиссию за пользование данной услугой.</span><br>
 <span style="font-size: 10pt;"> ВАЖНО! Условием использования этого способа оплаты является предоплата в размере 100 грн. После оформления заказа менеджер свяжется с Вами, чтобы сообщить банковские реквизиты. В случае отказа от принятия заказа, мы удерживаем вышеуказанную сумму в качестве компенсации стоимости доставки и расформирования заказа.</span>
</div>
 <br><?echo "</div>";
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>