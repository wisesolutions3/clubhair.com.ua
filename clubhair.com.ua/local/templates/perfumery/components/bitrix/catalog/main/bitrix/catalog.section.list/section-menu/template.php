<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();                        
if(\Bitrix\Main\Loader::includeSharewareModule("krayt.perfumery") == \Bitrix\Main\Loader::MODULE_DEMO_EXPIRED || 
   \Bitrix\Main\Loader::includeSharewareModule("krayt.perfumery") ==  \Bitrix\Main\Loader::MODULE_NOT_FOUND
    )
{ return false;}
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
//echo '<pre>'; print_r($arResult['SECTIONS']); echo '</pre>';
if($arResult['SECTIONS'])
{?>
    <div class="catalog__section-list">
    <div class="title">
        <span><?=\Bitrix\Main\Localization\Loc::getMessage('K_TITLE_SECTION')?></span>
        <button class="open_list visible-xs" data-id="categories"></button>
    </div>
            <div class="section__list-wrapper hidden_list" id="categories">
            <?$APPLICATION->IncludeComponent(
            "bitrix:menu",
            "section-menu",
            array(
                "ALLOW_MULTI_SELECT" => "N",
                "CHILD_MENU_TYPE" => "",
                "DELAY" => "N",
                "MAX_LEVEL" => "3",
                "MENU_CACHE_GET_VARS" => array(
                ),
                "MENU_CACHE_TIME" => "3600",
                "MENU_CACHE_TYPE" => "A",
                "MENU_CACHE_USE_GROUPS" => "Y",
                "MENU_THEME" => "site",
                "ROOT_MENU_TYPE" => "catalog",
                "USE_EXT" => "Y",
                "SECTIONS" => $arResult['SECTIONS']
            ),
            $component
        );?>
    </div>
    </div>


<?}
