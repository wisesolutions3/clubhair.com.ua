<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Повернення");
echo ' <div class="static_page">'?><div style="text-align: justify;">
<br>
 <span style="font-size: 20pt;"><b>Обмін та повернення товару</b></span><br>
 <br>
 <span style="font-size: 10pt;">Згiдно iз Законом України «Про захист прав споживачів» від 12.05.1991 та Постановою Кабінету Міністрів України № 172 від 19.03.1994 парфумерно-косметичні товари входять до переліку непродовольчих товарів належної якості, що не підлягають поверненню/обміну.</span><br>
 <span style="font-size: 10pt;"> </span><br>
 <span style="font-size: 10pt;">
	Ми дорожимо своєю репутацією. Усі товари, представлені в нашому інтернет-магазині, є оригінальними. Купуючи у нас, Ви захищаєте себе від неякісного або простроченого товару і гарантовано отримуєте оригінальний, високоякісний і безпечний товар.</span><br>
 <span style="font-size: 10pt;"> </span><br>
 <span style="font-size: 10pt;">
	Обмін/повернення товару відбувається виключно у разі: </span><br>
 <span style="font-size: 10pt;"> </span><b><span style="font-size: 10pt;">-</span></b><span style="font-size: 10pt;"> виявлення заводського дефекту товару (наприклад, не працює розпилювач, пошкоджений флакон);</span><br>
 <span style="font-size: 10pt;"> </span><b><span style="font-size: 10pt;">-</span></b><span style="font-size: 10pt;"> отримання простроченого товару;</span><br>
 <span style="font-size: 10pt;"> </span><b><span style="font-size: 10pt;">-</span></b><span style="font-size: 10pt;"> отримання товару, що не відповідає замовленню (пересортиця). </span><br>
 <span style="font-size: 10pt;"> </span><br>
 <span style="font-size: 10pt;">
	Для обміну/повернення товару у випадках згаданих вище, будь ласка звертайтесь за адресою </span><u><a href="mailto:clubhairua@gmail.com"><span style="font-size: 10pt;">clubhairua@gmail.com</span></a></u><span style="font-size: 10pt;"> або за телефоном </span><u><span style="font-size: 10pt;">(050) 323 66 48</span></u><span style="font-size: 10pt;">, і наші спеціалісти допоможуть Вам вирішити проблему в найкоротші терміни.</span><br>
 <span style="font-size: 10pt;"> </span><br>
 <span style="font-size: 10pt;">
	Якщо під час «огляду відправлення» в присутності менеджера/кур’єра служби доставки виявиться, що доставлений товар прострочений, має дефект або не відповідає замовленню (пересортиця), Ви маєте право відмовитись від отримування відправлення. В цьому випадку ми здійснено заміну товару або повернемо гроші сплачені за товар, на Ваш вибір. В цьому випадку можливе повернення не окремих позицій, а всього відправлення. Після оплати та отримання відправлення, розпаковану коробку із відправленням повернути менеджеру/кур’єру служби доставки вже буде неможливо. </span>
</div>
 <span style="font-size: 10pt;"> </span><br><?echo "</div>"; require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>