<?

class TurboSMSHandler {

    private $phone;
    private $message;
    private $client;

    public function __construct($phone, $message){
        $this->phone = $this->ValidatePhone($phone);
        $this->message = $message;

        $this->CheckSOAPModule();
    }

    public function SendTurboSMS() {
        $client = new SoapClient('http://turbosms.in.ua/api/wsdl.html');
        $auth_res = $client->Auth($this->GetAuthData());

        $sms = [
            'sender' => 'Clubhair',
            'destination' => $this->phone,
            'text' => $this->message
        ];

        return $client->SendSMS($sms);
    }


    private function CheckSOAPModule () {
        if (!extension_loaded('soap')) {
            throw new ErrorException('SOAP module not found');
        }
    }

    private function ValidatePhone ($phone) {
        $validated_phone = preg_replace("/\D/", "", $phone);
        $validated_phone = '+' . $validated_phone;

        if(preg_match('/^\+380\d{9}$/', $validated_phone) !== false) {
            return $validated_phone;
        } else {
            throw new ErrorException('Phone is incorrect');
        }
    }

    private function GetAuthData() {
        $auth_data = \Bitrix\Main\Config\Option::get('main', 'TurboSMSAuthData', '');
        $ar_auth_data = explode(':', $auth_data);

        return ['login' => $ar_auth_data[0], 'password' => $ar_auth_data[1]];
    }
}