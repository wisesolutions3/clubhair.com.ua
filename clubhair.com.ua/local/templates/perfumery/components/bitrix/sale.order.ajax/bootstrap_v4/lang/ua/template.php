<?
$MESS["ADDITIONAL_PROPS_DEFAULT"] = "додаткові властивості";
$MESS["ADD_DEFAULT"] = "Додати";
$MESS["AUTH_BLOCK_NAME_DEFAULT"] = "авторизація";
$MESS["AUTH_REFERENCE_1_DEFAULT"] = "Символом \"зірочка\" (*) відзначені обов'язкові для заповнення поля.";
$MESS["AUTH_REFERENCE_2_DEFAULT"] = "Після реєстрації ви отримаєте інформаційний лист.";
$MESS["AUTH_REFERENCE_3_DEFAULT"] = "Особисті відомості, отримані в розпорядження інтернет-магазину при реєстрації або яким-небудь іншим чином, не будуть без дозволу користувачів передаватися третім організаціям і приватним особам за винятком ситуацій, коли цього вимагає закон або судове ріш";
$MESS["BACK_DEFAULT"] = "назад";
$MESS["BASKET_BLOCK_NAME_DEFAULT"] = "Товари в замовленні";
$MESS["BUYER_BLOCK_NAME_DEFAULT"] = "Покупець";
$MESS["CAPTCHA_REGF_PROMT"] = "Введіть слово на картинці";
$MESS["CAPTCHA_REGF_TITLE"] = "Захист від автоматичної реєстрації";
$MESS["COUPON_DEFAULT"] = "купон";
$MESS["DELIVERY_BLOCK_NAME_DEFAULT"] = "Доставка";
$MESS["DELIVERY_CALC_ERROR_TEXT_DEFAULT"] = "Ви можете продовжити оформлення замовлення, а трохи пізніше менеджер магазину зв'яжеться з вами і уточнить інформацію по доставці.";
$MESS["DELIVERY_CALC_ERROR_TITLE_DEFAULT"] = "Не вдалося розрахувати вартість доставки.";
$MESS["ECONOMY_DEFAULT"] = "Економія";
$MESS["EDIT_DEFAULT"] = "змінити";
$MESS["EMPTY_BASKET_HINT"] = "#A1# Натисніть тут #A2#, щоб продовжити покупки";
$MESS["EMPTY_BASKET_HINT_PATH_TIP"] = "Посилання для продовження покупок відображається в разі якщо кошик порожній. <br> За умови, що шлях порожньої, текст не буде відображений.";
$MESS["EMPTY_BASKET_TITLE"] = "ваш кошик порожній";
$MESS["FAIL_PRELOAD_TEXT_DEFAULT"] = "Ви замовляли в нашому інтернет-магазині, тому ми заповнили всі дані автоматично. <br /> Зверніть увагу на розгорнутий блок з інформацією про замовлення. Тут ви можете внести необхідні зміни або залишити як є і натиснути кнопку \"#ORDER_BUTTON#\".";
$MESS["FURTHER_DEFAULT"] = "Далі";
$MESS["INNER_PS_BALANCE_DEFAULT"] = "На вашому призначеному для користувача рахунку:";
$MESS["NAV_BACK_DEFAULT"] = "назад";
$MESS["NAV_FORWARD_DEFAULT"] = "вперед";
$MESS["NEAREST_PICKUP_LIST_DEFAULT"] = "Найближчі пункти:";
$MESS["ORDER_DEFAULT"] = "Оформити замовлення";
$MESS["ORDER_DESC_DEFAULT"] = "Коментарі до замовлення:";
$MESS["PAYMENT_BLOCK_NAME_DEFAULT"] = "Оплата";
$MESS["PAY_SYSTEM_PAYABLE_ERROR_DEFAULT"] = "Ви зможете оплатити замовлення після того, як менеджер перевірить наявність повного комплекту товарів на складі. Відразу після перевірки ви отримаєте лист з інструкціями з оплати. Оплатити замовлення можна буде в персональному розділі сайту.";
$MESS["PERIOD_DEFAULT"] = "Термін доставки";
$MESS["PERSON_TYPE_DEFAULT"] = "Тип платника";
$MESS["PICKUP_LIST_DEFAULT"] = "Пункти самовивезення:";
$MESS["PRICE_DEFAULT"] = "Вартість";
$MESS["PRICE_FREE_DEFAULT"] = "Безкоштовно";
$MESS["REGION_BLOCK_NAME_DEFAULT"] = "Регіон доставки";
$MESS["REGION_REFERENCE_DEFAULT"] = "Виберіть своє місто в списку. Якщо ви не знайшли своє місто, виберіть \"інше місце розташування\", а місто впишіть в поле \"Місто\"";
$MESS["REGISTRATION_REFERENCE_DEFAULT"] = "Якщо ви вперше на сайті, і хочете, щоб ми вас пам'ятали, і всі ваші замовлення зберігалися, заповніть реєстраційну форму.";
$MESS["REG_BLOCK_NAME_DEFAULT"] = "Реєстрація";
$MESS["SALE_SADC_TRANSIT"] = "Термін доставки";
$MESS["SELECT_FILE_DEFAULT"] = "вибрати";
$MESS["SELECT_PICKUP_DEFAULT"] = "вибрати";
$MESS["SELECT_PROFILE_DEFAULT"] = "Виберіть профіль";
$MESS["SOA_BAD_EXTENSION"] = "Невірний тип файлу";
$MESS["SOA_DATE_SUC"] = "Дата оформлення замовлення: <b>#ORDER_DATE#</ b>";
$MESS["SOA_DELIVERY"] = "Служба доставки";
$MESS["SOA_DELIVERY_SELECT_ERROR"] = "Служба доставки не вибрана";
$MESS["SOA_DISTANCE_KM"] = "км";
$MESS["SOA_DO_SOC_SERV"] = "Увійти за допомогою соцмереж";
$MESS["SOA_ERROR_ORDER"] = "Помилка формування замовлення";
$MESS["SOA_ERROR_ORDER_LOST"] = "Замовлення № #ORDER_ID# не найден.";
$MESS["SOA_ERROR_ORDER_LOST1"] = "Будь ласка зверніться до адміністрації інтернет-магазину або спробуйте оформити ваше замовлення ще раз.";
$MESS["SOA_FIELD"] = "поле";
$MESS["SOA_INVALID_EMAIL"] = "Введено невірний e-mail";
$MESS["SOA_INVALID_PATTERN"] = "не відповідає шаблоном";
$MESS["SOA_INVALID_PHONE"] = "Введено невірний номер телефону";
$MESS["SOA_LESS"] = "не менше";
$MESS["SOA_LOCATION_NOT_FOUND"] = "Місце розташування не знайдено";
$MESS["SOA_LOCATION_NOT_FOUND_PROMPT"] = "#ANCHOR# Виберіть розташування зі списку #ANCHOR_END#, щоб ми дізналися, куди нам доставити ваше замовлення";
$MESS["SOA_MAP_COORDS"] = "Координати на мапі";
$MESS["SOA_MAX_LENGTH"] = "Максимальна довжина поля";
$MESS["SOA_MAX_SIZE"] = "Перевищено максимальний розмір файлу";
$MESS["SOA_MAX_VALUE"] = "Максимальне значення поля";
$MESS["SOA_MIN_LENGTH"] = "Мінімальна довжина поля";
$MESS["SOA_MIN_VALUE"] = "Мінімальне значення поля";
$MESS["SOA_MORE"] = "не більше";
$MESS["SOA_NO"] = "немає";
$MESS["SOA_NOT_CALCULATED"] = "не розрахована";
$MESS["SOA_NOT_FOUND"] = "НЕ знайдений";
$MESS["SOA_NOT_NUMERIC"] = "має бути числовим";
$MESS["SOA_NOT_SELECTED"] = "не обрано";
$MESS["SOA_NOT_SELECTED_ALT"] = "При необхідності уточнити місце розташування";
$MESS["SOA_NOT_SPECIFIED"] = "не вказано";
$MESS["SOA_NO_JS"] = "Для оформлення замовлення необхідно включити JavaScript. Мабуть, JavaScript або не підтримується браузером, або відключений. Змініть настройки браузера і потім <a href=\"\"> повторіть спробу </a>.";
$MESS["SOA_NUM_STEP"] = "не відповідає кроку";
$MESS["SOA_ORDER_COMPLETE"] = "Замовлення сформовано";
$MESS["SOA_ORDER_PROPS"] = "властивості замовлення";
$MESS["SOA_ORDER_PS_ERROR"] = "Помилка обраного способу оплати. Зверніться до Адміністрації сайту, або виберіть інший спосіб оплати.";
$MESS["SOA_ORDER_SUC"] = "Номер замовлення: <b>№#ORDER_ID#</ b>";
$MESS["SOA_ORDER_SUC1"] = "Ви можете слідкувати за виконанням свого замовлення в <a href=\"#LINK#\"> Персональному розділі сайту </a>. <br/> Зверніть увагу, що для входу в цей розділ вам необхідно буде ввести логін і пароль користувача сайту.";
$MESS["SOA_ORDER_SUC_TITLE"] = "Ваше замовлення успішно створене!";
$MESS["SOA_OTHER_LOCATION"] = "Інша розташування";
$MESS["SOA_PAY"] = "Оплата замовлення:";
$MESS["SOA_PAYMENT_SUC"] = "Номер вашої оплати: <b>№#PAYMENT_ID#</ b>";
$MESS["SOA_PAYSYSTEM_PRICE"] = "Додатково накладений платіж:";
$MESS["SOA_PAY_ACCOUNT3"] = "Коштів достатньо для повної оплати замовлення.";
$MESS["SOA_PAY_LINK"] = "Якщо вікно з платіжною інформацією не відкрилося автоматично, натисніть на посилання <a href=\"#LINK#\" target=\"_blank\">Оплатити замовлення</a>.";
$MESS["SOA_PAY_PDF"] = "Для того, щоб завантажити рахунок в форматі pdf, натисніть на посилання <a href=\"#LINK#\" target=\"_blank\"> Завантажити рахунок </a>.";
$MESS["SOA_PAY_SYSTEM"] = "Платіжна система";
$MESS["SOA_PICKUP_ADDRESS"] = "Адреса";
$MESS["SOA_PICKUP_DESC"] = "опис";
$MESS["SOA_PICKUP_EMAIL"] = "Електронна пошта";
$MESS["SOA_PICKUP_PHONE"] = "Телефон";
$MESS["SOA_PICKUP_STORE"] = "склад";
$MESS["SOA_PICKUP_WORK"] = "Режим роботи";
$MESS["SOA_PROP_NEW_PROFILE"] = "новий профіль";
$MESS["SOA_PS_SELECT_ERROR"] = "Платіжна система не обрана";
$MESS["SOA_REQUIRED"] = "обов'язково для заповнення";
$MESS["SOA_SUM_DELIVERY"] = "Доставка:";
$MESS["SOA_SUM_DISCOUNT"] = "знижка";
$MESS["SOA_SUM_IT"] = "Разом:";
$MESS["SOA_SUM_LEFT_TO_PAY"] = "Залишилося сплатити:";
$MESS["SOA_SUM_NAME"] = "Найменування";
$MESS["SOA_SUM_PAYED"] = "Сплачено:";
$MESS["SOA_SUM_PRICE"] = "Ціна";
$MESS["SOA_SUM_QUANTITY"] = "Кількість";
$MESS["SOA_SUM_SUMMARY"] = "Товарів на:";
$MESS["SOA_SUM_VAT"] = "ПДВ:";
$MESS["SOA_SUM_WEIGHT"] = "вага";
$MESS["SOA_SUM_WEIGHT_SUM"] = "Загальна вага:";
$MESS["SOA_SYMBOLS"] = "символів";
$MESS["SOA_YES"] = "да";
$MESS["STOF_AUTH_REQUEST"] = "Будь ласка, увійдіть";
$MESS["STOF_DO_AUTHORIZE"] = "Авторизуватись";
$MESS["STOF_DO_REGISTER"] = "Зареєструватися";
$MESS["STOF_EMAIL"] = "E-mail";
$MESS["STOF_ENTER"] = "Увійти";
$MESS["STOF_FORGET_PASSWORD"] = "Забули пароль?";
$MESS["STOF_LASTNAME"] = "Прізвище";
$MESS["STOF_LOGIN"] = "Логін";
$MESS["STOF_MY_PASSWORD"] = "Задати логін і пароль";
$MESS["STOF_NAME"] = "Ім'я";
$MESS["STOF_NEXT_STEP"] = "Продовжити оформлення замовлення";
$MESS["STOF_PASSWORD"] = "Пароль";
$MESS["STOF_REGISTER"] = "Реєстрація";
$MESS["STOF_REG_HINT"] = "Якщо ви вперше на сайті, і хочете, щоб ми вас пам'ятали, і всі ваші замовлення зберігалися, заповніть реєстраційну форму.";
$MESS["STOF_REG_REQUEST"] = "Будь ласка, зареєструйтесь";
$MESS["STOF_REMEMBER"] = "Запам'ятати мене";
$MESS["STOF_RE_PASSWORD"] = "Повтор пароля";
$MESS["STOF_SYS_PASSWORD"] = "Згенерувати логін і пароль";
$MESS["SUCCESS_PRELOAD_TEXT_DEFAULT"] = "Ви замовляли в нашому інтернет-магазині, тому ми заповнили всі дані автоматично. <br /> Якщо все заповнено вірно, натисніть кнопку \"#ORDER_BUTTON#\".";
$MESS["USE_COUPON_DEFAULT"] = "застосувати купон";
$MESS["ACCEPT_USER_CONSENT"] = "Приймаю";
$MESS['DELIVERY_NOT_FREE_TITLE'] = 'За тарифами';

$MESS['WS_CITY_ALTERNATIVE_NAME'] = 'Місто';
?>