<?
$MESS["IBLOCK_CACHE_FILTER"] = "Кешировать при установленном фильтре";

$MESS["T_IBLOCK_DESC_LIST_ID"] = "Код информационного блока";
$MESS["T_IBLOCK_DESC_LIST_TYPE"] = "Тип информационного блока (используется только для проверки)";
$MESS["T_IBLOCK_DESC_CAT_ID"] = "Код информационного блока каталога";
$MESS["T_IBLOCK_DESC_CAT_TYPE"] = "Тип информационного блока каталога (используется только для проверки)";
$MESS["SHOW_ALL"] = "Показать кнопку \"Все брэнды\"";

$MESS["CP_BNL_CACHE_GROUPS"] = "Учитывать права доступа";

?>