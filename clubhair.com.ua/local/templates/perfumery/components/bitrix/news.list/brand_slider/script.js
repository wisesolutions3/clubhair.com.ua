$(function () {

	$('.brands_wrapper .owl-carousel').owlCarousel({
        items: 5,
        dots: false,
        nav: true,
        navSpeed: 1000,
        lazyContent: true,
        lazyLoad: true,
        responsive: {
            0: {
                dots: false,
                items: 2
            },
            401: {
                dots: false,
                items: 3
            },
            769: {
                dots: false,
                items: 4
            },
            1281: {
                dots: false,
                items: 5
            }
        }
	});

})