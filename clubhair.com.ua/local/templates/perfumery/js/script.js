// region function header fixed -----------------------------------
function topLinePosition() {

    var scrollTop = $(document).scrollTop(),
        headerbottomTop = $(".header-bottom").offset().top;

    if(scrollTop >= headerbottomTop) {
        if(!$(".header").hasClass('sticky'))
        {

            $(".header").addClass("sticky");
            $(".hb-content").addClass('animated fadeInDown');
            $('body').addClass('sticky-in');
            $('#header-top-line .top-icon-box').appendTo($('#top-icon-wrapper'));

            navigationresize();
        }
    }
    else {
        $(".header").removeClass("sticky");
        $(".hb-content").removeClass('animated fadeInDown');
        $('body').removeClass('sticky-in');
        $('#top-icon-wrapper .top-icon-box').appendTo($('#header-top-line'));
        navigationresize();
    }

}
// endregion


//region resize menu -----------------------------------
function navigationresize() {
    $('.bx-nav-list-1-lvl li.more').before($('#overflow > li'));

    var navItemMore = $('.bx-nav-list-1-lvl > li.more'),
        navItems = $('.bx-nav-list-1-lvl > li:not(.more)'),
        navItemWidth = navItemMore.width(),
        windowWidth = $('.bx-nav-list-1-lvl').parent().width(),
        navOverflowWidth;

    navItems.each(function () {
        navItemWidth += $(this).width();
    });
    navItemWidth > windowWidth ? navItemMore.show() : navItemMore.hide();

    while (navItemWidth > windowWidth) {
        navItemWidth -= navItems.last().width();
        navItems.last().prependTo('#overflow');
        navItems.splice(-1, 1);
    }
    navOverflowWidth = $('#overflow').width();
}
// endregion


// region open fast view window -----------------------------------

var fastView = {

    url: "",
    self:{},
    _init: function () {

        $(document).on('click',".fast_view",this.loadView);
    },
    loadView: function () {

        var self = fastView;
        self.url = $(this).data('href');
        $('.product_card_fast').addClass('fast-in');
        $('.popup_mask').fadeIn(100);
        $('body').addClass('noscroll');
        $.get(self.url+"?ajax_view=y&"+Date.now(),function (data) {

            $(".add_el").html(data).promise().done(function () {
                $('.popup_mask').fadeIn(100);
                InitUlDropdown();
            });

        });
        return false;
    }

};
$(function () {
    fastView._init();
});
// endregion


$(document).ready(function () {

// region main banner -----------------------------------

    $('.main_banner').slick({
        slidesToShow: 1,
        slidesToScroll: 1,
        arrows: false,
        dots: true,
        infinite: true,
        autoplay: true,
        speed: 500,
        fade: true
    });
// endregion


// region all sliders -----------------------------------

     $('.slider .owl-carousel').owlCarousel({
        items: 5,
        margin: -1,
        dots: true,
        dotsSpeed: 1000,
        nav: true,
        navSpeed: 1000,
        lazyContent: true,
        responsive: {
            0: {
                dots: false,
                items: 2
            },
            992: {
                dots: false,
                items: 3
            },
            1025: { 
                items: 4
            },
			1400: {
				items: 5
			}
        }
    });
// endregion


// region top basket slider -----------------------------------

    $('.top_slider .owl-carousel').owlCarousel({
        items: 6,
        margin: -1,
        dots: false,
        nav: true,
        navSpeed: 500,
        lazyContent: true
    });

// endregion
    if ($(window).width() > 1024) {

        // header fixed -----------------------------------
        topLinePosition();
        navigationresize();
        $(window).scroll(function() {
            topLinePosition();
        });


        // close fast view window -----------------------------------
        $('.close-btn, .popup_mask').on('click', function () {
            $('body').removeClass('noscroll');
            $('.product_card_fast').removeClass('fast-in');
            $('.popup_mask').fadeOut(100);
            $('.add_el').html("");
        });


        // open top user menu -----------------------------------
        $(document).on('click', '.user-link', function (e) {
            e.preventDefault();
            $('#user').toggleClass('open-in');
        });


        // open top basket
        $(document).on('click', '.basket-link', function (e) {
            e.preventDefault();
            $('#basket_in').toggleClass('open-in');
        });

// region scroll-bar
        $(".brand-menu .brand-tab .brand-tab-wrp").niceScroll({
            cursorcolor: "#d8d8d8",
            cursorborder: 0,
            cursorborderradius: 2,
            zindex: 999,
            cursoropacitymax: 1
        });
// endregion


    } else {

        // open mobile menu catalog
        $(document).on('click', '.catalog__filter .catalog__section-list .title > span, .catalog__filter .catalog__section-filter .title > span', function () {
            var button = $(this).parent().find('.open_list');
            var id = $(button).attr('data-id');
            $(button).toggleClass('active');
            $('#' + id).slideToggle();
        })

        $(document).on('click', '.open_list', function () {
            var id = $(this).attr('data-id');
            $(this).toggleClass('active');
            $('#' + id).slideToggle();
        });

        $(window).scroll(function () {
            var scroll = $(window).scrollTop();
            if (scroll > 10) {
                $('.header').addClass('fixed');
            } else {
                $(".header").removeClass('fixed');
            }
        });


        $('.header-brand-line')
            .appendTo($('#brand_mobile'))
            .css({'display': 'block'});

    }


// region open mobile menu

    var Closed = false;

    $('.hamburger').click(function () {
        if (Closed == true) {
            $('.hamburger').removeClass('is-active');
            $('body').removeClass('noscroll menu-in');
            Closed = false;
        } else {
            $('.hamburger').addClass('is-active');
            $('body').addClass('noscroll menu-in');
            Closed = true;
        }
    });
// endregion


// region close top basket, top search, top user menu -----------------------------------

    $(document).click(function(event) {
        $target = $(event.target);
        if(!$target.closest('#basket_in').length) {
            $('#basket_in').removeClass('open-in');
        }
        if(!$target.closest('#search_in').length) {
            $('#search_in').removeClass('open-in');
            $('#title-search-input').val('');
            $('.title-search-result').html('');
        }
        if(!$target.closest('#user').length) {
            $('#user').removeClass('open-in');
        }
    });
// endregion


// region open search title -----------------------------------

    $(document).on('click', '.search-link', function (e) {
        e.preventDefault();
        $('#search_in').toggleClass('open-in');
        $('#title-search-input').focus();
        $('.hamburger').removeClass('is-active');
        $('body').removeClass('noscroll menu-in');
        Closed = false;
    });
// endregion


// region add to favourites  -----------------------------------
    $(document).on('click','.to_favorites',function() { // работа с закладками
        var name = 'FOREVER';
        var cookie_zac = BX.getCookie(name);

        if($(this).hasClass('active')){
            $(this).removeClass('active');
            var znach = $(this).attr('data-cookieid')+"|";
            var new_zac = cookie_zac.replace(znach,"");

            $('.goods_icon-counter').html(
                +($('.goods_icon-counter').html())-1
            );
            if($(this).data('remove')) {
                $(this).closest('.favour-item').remove();
            }
            BX.setCookie(name, new_zac, {expires: 86400,path:'/'});
        }else{
            $(this).addClass('active');

            if(cookie_zac == undefined){
                var znach = "|"+$(this).attr('data-cookieid')+"|";
                BX.setCookie(name, znach, {expires: 86400,path:'/'});
            }else{
                var znach = cookie_zac+$(this).attr('data-cookieid')+"|";
                BX.setCookie(name, znach, {expires: 86400,path:'/'});
            }
        }
        activZacladca();
        return false;
    });

    activZacladca(); // вызов функции проверки закладок

// endregion


// region animation fade in content

    $(".animsition").animsition({
        inClass: 'fade-in-up-sm',
        outClass: '',
        inDuration: 800,
        outDuration: 300,
        loading: true,
        loadingParentElement: 'body', //animsition wrapper element
        loadingClass: 'animsition-loading',
        loadingInner: '', // e.g '<img src="loading.svg" />'
        timeout: 100,
        timeoutCountdown: 5000,
        onLoadEvent: true,
        browser: [ 'animation-duration', '-webkit-animation-duration'],
        // "browser" option allows you to disable the "animsition" in case the css property in the array is not supported by your browser.
        // The default setting is to disable the "animsition" in a browser that does not support "animation-duration".
        overlay : false,
        overlayClass : 'animsition-overlay-slide',
        overlayParentElement : 'body',
        transition: function(url){ window.location.href = url; }
    });
// endregion


// region scroll-bar
    $(".stores-list").niceScroll({
        cursorcolor: "#d8d8d8",
        cursorborder: 0,
        cursorborderradius: 2,
        zindex: 999,
        cursoropacitymax: 1
    });
// endregion

});


function activZacladca() { // проверка есть ли товар в закладках
    var name = 'FOREVER';

    var cookie_zac = BX.getCookie(name);
    if (cookie_zac !== undefined) {
        var mas = cookie_zac.split('|');

        mas.forEach(function (item, i, mas) {
            if (item != "" ) {

                $('#favour_in .goods_icon-counter').html(
                    i
                );
                $(".to_favorites[data-cookieid='"+item+"']").addClass('active');
            }
        });
    }

}


// section list for brands
$(document).ready(function () {
    var arr = $('[data-role="prop_angle_brand"]');
    arr.each(function(i,elem) {
        if ($(elem).siblings().length < 2)
            $(elem).css('display','none');
    });
    $(".brend-active").parents(".bsel").addClass('selected');
    $(".brend-active").parents(".bsel").siblings().addClass('selected');
});

$('[data-role="prop_angle_brand"]').on("click", function() {
    var elt = $(this).siblings();

    if (elt.hasClass('selected')){
        $(this).removeClass('selected')
        elt.removeClass('selected');
    }
    else{
        $(this).addClass('selected');
        elt.addClass('selected');
    }
});
