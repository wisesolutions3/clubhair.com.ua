<?
$MESS["CT_CPV_TPL_ELEMENT_DELETE_CONFIRM"] = "Damit werden alle Informationen gelГ¶scht, die mit diesem Eintrag zusammenhГ¤ngen. Fortfahren?";
$MESS["CT_CPV_TPL_MESS_BTN_BUY"] = "Kaufen";
$MESS["CT_CPV_TPL_MESS_BTN_ADD_TO_BASKET"] = "In den Warenkorb";
$MESS["CT_CPV_TPL_MESS_PRODUCT_NOT_AVAILABLE"] = "Nicht am Lager";
$MESS["CT_CPV_TPL_MESS_BTN_DETAIL"] = "Details";
$MESS["CT_CPV_TPL_MESS_BTN_SUBSCRIBE"] = "Abonnieren";
$MESS["CT_CPV_CATALOG_BTN_MESSAGE_BASKET_REDIRECT"] = "Warenkorb anzeigen";
$MESS["ADD_TO_BASKET_OK"] = "Zu Ihrem Warenkorb hinzugefГјgt";
$MESS["CT_CPV_TPL_MESS_BTN_COMPARE"] = "Vergleichen";
$MESS["CT_CPV_CATALOG_TITLE_ERROR"] = "Fehler";
$MESS["CT_CPV_CATALOG_TITLE_BASKET_PROPS"] = "Produkteigenschaften, die in den Warenkorb Гјbermittelt werden sollen";
$MESS["CT_CPV_CATALOG_BASKET_UNKNOWN_ERROR"] = "Unbekannter Fehler beim HinzufГјgen eines Elements zum Warenkorb";
$MESS["CT_CPV_CATALOG_BTN_MESSAGE_SEND_PROPS"] = "AuswГ¤hlen";
$MESS["CT_CPV_CATALOG_BTN_MESSAGE_CLOSE"] = "SchlieГџen";
$MESS["CT_CPV_CATALOG_BTN_MESSAGE_CLOSE_POPUP"] = "Weiter einkaufen";
$MESS["CT_CPV_CATALOG_MESS_COMPARE_OK"] = "Produkt wurde zur Vergleichstabelle hinzugefГјgt";
$MESS["CT_CPV_CATALOG_MESS_COMPARE_TITLE"] = "Produktvergleich";
$MESS["CT_CPV_CATALOG_PRICE_TOTAL_PREFIX"] = "Gesamt";
$MESS["CT_CPV_CATALOG_MESS_COMPARE_UNKNOWN_ERROR"] = "Fehler beim HinzufГјgen des Produktes zur Vergleichstabelle";
$MESS["CT_CPV_CATALOG_BTN_MESSAGE_COMPARE_REDIRECT"] = "Produkte vergleichen";
$MESS["CT_CPV_CATALOG_BTN_MESSAGE_LAZY_LOAD_WAITER"] = "Geladen";
$MESS["CT_CPV_CATALOG_SHOW_MAX_QUANTITY"] = "Am Lager";
$MESS["CT_CPV_CATALOG_RELATIVE_QUANTITY_MANY"] = "Am Lager verfГјgbar";
$MESS["CT_CPV_CATALOG_RELATIVE_QUANTITY_FEW"] = "nur noch wenige";
?>