<?
$MESS["FORUM_BUTTON_CANCEL"] = "Скасувати";
$MESS["FORUM_BUTTON_OK"] = "вставити";
$MESS["FORUM_ERROR_NO_PATH_TO_VIDEO"] = "Вкажіть шлях до відео.";
$MESS["FORUM_ERROR_NO_TITLE"] = "Ведіть назву.";
$MESS["FORUM_ERROR_NO_URL"] = "Ви повинні ввести адресу (URL)";
$MESS["FORUM_HEIGHT"] = "Висота";
$MESS["FORUM_LIST_PROMPT"] = "Введіть пункт списку. Натисніть 'Скасувати' або залиште пробіл для завершення списку";
$MESS["FORUM_PATH"] = "Шлях (http: //)";
$MESS["FORUM_PREVIEW"] = "Малюнок (http: //)";
$MESS["FORUM_TEXT_ENTER_IMAGE"] = "Введіть повну адресу (URL) зображення";
$MESS["FORUM_TEXT_ENTER_URL"] = "Введіть повну адресу (URL)";
$MESS["FORUM_TEXT_ENTER_URL_NAME"] = "Введіть назву сайту";
$MESS["FORUM_VIDEO"] = "Відео";
$MESS["FORUM_WIDTH"] = "Ширина";
$MESS["F_ATTACH_FILES"] = "Прикріплені файли";
$MESS["F_CAPTCHA_PROMT"] = "Символи на картинці";
$MESS["F_CAPTCHA_TITLE"] = "Захист від автоматичних повідомлень";
$MESS["F_C_GOTO_FORUM"] = "&gt";
$MESS["F_DELETE"] = "видалити";
$MESS["F_DELETE_CONFIRM"] = "Повідомлення буде видалено без можливості відновлення. Видалити?";
$MESS["F_DOWNLOAD"] = "завантажити";
$MESS["F_FILE_EXTENSION"] = "Дозволені розширення файлів: # EXTENSION #.";
$MESS["F_FILE_SIZE"] = "Розмір файлу не повинен перевищувати # SIZE #.";
$MESS["F_HIDE"] = "приховати";
$MESS["F_HIDE_SMILE"] = "приховати";
$MESS["F_LOAD_FILE"] = "завантажити файл";
$MESS["F_LOAD_IMAGE"] = "Завантажити зображення";
$MESS["F_NAME"] = "ім'я";
$MESS["F_PREVIEW"] = "попередній повідомлення";
$MESS["F_QUOTE_FULL"] = "Цитувати";
$MESS["F_QUOTE_HINT"] = "Для вставки цитати в форму відповіді виділіть її та натисніть сюди";
$MESS["F_SHOW"] = "Показати";
$MESS["F_WAIT"] = "Зачекайте ...";
$MESS["F_WANT_ALLOW_SMILES"] = "Дозволити смайлики в цьому повідомленні";
$MESS["F_WANT_SUBSCRIBE_FORUM"] = "Підписатися на нові повідомлення цього форуму";
$MESS["F_WANT_SUBSCRIBE_TOPIC"] = "Підписатися на нові повідомлення цієї теми";
$MESS["JERROR_MAX_LEN"] = "Максимальна довжина повідомлення # MAX_LENGTH # символів. Всього символів: # LENGTH #.";
$MESS["JERROR_NO_MESSAGE"] = "Ви повинні ввести повідомлення.";
$MESS["JERROR_NO_TOPIC_NAME"] = "Ви повинні ввести назву теми.";
$MESS["JQOUTE_AUTHOR_WRITES"] = "пише";
$MESS["OPINIONS_EMAIL"] = "Ваш E-Mail";
$MESS["OPINIONS_NAME"] = "Ваше ім'я";
$MESS["OPINIONS_PREVIEW"] = "переглянути";
$MESS["OPINIONS_SEND"] = "Надіслати";
?>