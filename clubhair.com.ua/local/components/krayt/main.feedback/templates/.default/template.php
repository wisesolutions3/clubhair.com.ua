<?
if(!defined("B_PROLOG_INCLUDED")||B_PROLOG_INCLUDED!==true)die();
/**
 * Bitrix vars
 *
 * @var array $arParams
 * @var array $arResult
 * @var CBitrixComponentTemplate $this
 * @global CMain $APPLICATION
 * @global CUser $USER
 */
?>

    <div class="title">
            <?$APPLICATION->IncludeFile(
                SITE_DIR."include/title_feedback.php",
                Array(),
                Array("MODE"=>"html")
            );?>
        </div>

<div class="row justify-content-center justify-content-lg-start">
    <form action="<?=POST_FORM_ACTION_URI?>" method="POST" class="col-sm-8 col-12 message-form form-feedback <?if(strlen($arResult["OK_MESSAGE"]) > 0){?>form-done<?}?>">
        <?=bitrix_sessid_post()?>

        <div class="message-form__wrapper row">

            <div class="form__widget-content mb-3 col-12">

                <div class="ajax-preloader-wrap" id="ajax-preloader-wrap" style="display:none;">
                    <div class="ajax-preloader">
                        <div class="fa fa-spinner fa-pulse fa-3x fa-fw"></div>
                    </div>
                </div>

                <div class="message-form__widget <?if(!empty($arResult["ERROR_MESSAGE"]["NO_NAME"])):?>red-brd<?endif;?>">
                    <input type="text"
                           name="user_name"
                           value="<?=$arResult["AUTHOR_NAME"]?>"
                           id="user_name"
                           class="text-field <?if(!empty($arResult["AUTHOR_NAME"])){?>done<?}?>"
                           placeholder="<?=GetMessage("MFT_NAME")?><?if(empty($arParams["REQUIRED_FIELDS"]) || in_array("NAME", $arParams["REQUIRED_FIELDS"])):?>*<?endif?>"
                    >
                    <?if (!empty($arResult["ERROR_MESSAGE"]["NO_NAME"])) {?>
                        <span class="mf-error"><?=$arResult["ERROR_MESSAGE"]["NO_NAME"]?></span>
                    <?}?>
                </div>

                <div class="message-form__widget <?if(!empty($arResult["ERROR_MESSAGE"]["NO_EMAIL"])):?>red-brd<?endif;?>">
                    <input type="text"
                           name="user_email"
                           value="<?=$arResult["AUTHOR_EMAIL"]?>"
                           id="user_email"
                           class="text-field <?if(!empty($arResult["AUTHOR_EMAIL"])){?>done<?}?>"
                           placeholder="<?=GetMessage("MFT_EMAIL")?><?if(empty($arParams["REQUIRED_FIELDS"]) || in_array("EMAIL", $arParams["REQUIRED_FIELDS"])):?>*<?endif;?>"
                    >
                    <?if (!empty($arResult["ERROR_MESSAGE"]["NO_EMAIL"])) {?>
                        <span class="mf-error"><?=$arResult["ERROR_MESSAGE"]["NO_EMAIL"]?></span>
                    <?}?>
                </div>

                <div class="message-form__widget <?if(!empty($arResult["ERROR_MESSAGE"]["NO_PHONE"])):?>red-brd<?endif;?>">
                    <input type="text"
                           name="user_phone"
                           value="<?=$arResult["AUTHOR_PHONE"]?>"
                           id="user_phone"
                           class="text-field masked <?if(!empty($arResult["AUTHOR_PHONE"])){?>done<?}?>"
                           placeholder="<?=GetMessage("MFT_PHONE")?><?if(empty($arParams["REQUIRED_FIELDS"]) || in_array("PHONE", $arParams["REQUIRED_FIELDS"])):?>*<?endif;?>"
                    >
                    <?if (!empty($arResult["ERROR_MESSAGE"]["NO_PHONE"])) {?>
                        <span class="mf-error"><?=$arResult["ERROR_MESSAGE"]["NO_PHONE"]?></span>
                    <?}?>
                </div>

                <div class="message-form__widget <?if(!empty($arResult["ERROR_MESSAGE"]["NO_MESSAGE"])):?>red-brd<?endif;?>">
                        <textarea name="MESSAGE"
                                  rows="30"
                                  cols="10"
                                  placeholder="<?=GetMessage("MFT_MESSAGE")?><?if(empty($arParams["REQUIRED_FIELDS"]) || in_array("MESSAGE", $arParams["REQUIRED_FIELDS"])):?>*<?endif?>"
                        ><?=$arResult["MESSAGE"]?></textarea>
                    <?if (!empty($arResult["ERROR_MESSAGE"]["NO_MESSAGE"])) {?>
                        <span class="mf-error"><?=$arResult["ERROR_MESSAGE"]["NO_MESSAGE"]?></span>
                    <?}?>
                </div>

                <?if($arParams["USE_CAPTCHA"] == "Y"):
                    $captcha_code = $_POST["captcha_sid"];
                    $captcha_word = $_POST["captcha_word"];
                    $cpt = new CCaptcha();
                    $captchaPass = COption::GetOptionString("main", "captcha_password", "");
                    ?>
                    <div class="message-form__widget mf-captcha <?if(!empty($arResult["ERROR_MESSAGE"][0])):?>red-brd<?endif;?>">
                        <input type="hidden" name="captcha_sid" value="<?=$arResult["capCode"]?>">
                        <img src="/bitrix/tools/captcha.php?captcha_sid=<?=$arResult["capCode"]?>"
                             width="180" height="40" alt="CAPTCHA"
                             class="mb-1"
                        >
                        <input type="text" name="captcha_word" size="30" maxlength="50" value=""
                               placeholder="<?=GetMessage("MFT_CAPTCHA_CODE")?>*"
                        >
                        <?
                        if(!empty($arResult["ERROR_MESSAGE"][0])) {
                            if (strlen($captcha_word) > 0 && strlen($captcha_code) > 0) {
                                if (!$cpt->CheckCodeCrypt($captcha_word, $captcha_code, $captchaPass)) { ?>
                                    <span class="mf-error"><?= $arResult["ERROR_MESSAGE"][0] = GetMessage("MF_CAPTCHA_WRONG"); ?></span>
                                    <?
                                }
                            }
                            else {?>
                                <span class="mf-error"><?=$arResult["ERROR_MESSAGE"][0] = GetMessage("MF_CAPTHCA_EMPTY");?></span>
                            <?}
                        }


                        ?>
                    </div>
                <?endif;?>
            </div>
            <input type="hidden" name="PARAMS_HASH" value="<?=$arResult["PARAMS_HASH"]?>">

            <div class="message-form__controls col-12">
                <div class="btn-green-gradient">
                    <input type="submit" name="submit"
                           value="<?=GetMessage("MFT_SUBMIT")?>"
                           class="message-submit btn-form__control"
                    >
                </div>

                <?
                if(strlen($arResult["OK_MESSAGE"]) > 0)
                {
                    ?><div class="mf-ok-text"><?=$arResult["OK_MESSAGE"]?></div><?
                }
                ?>
            </div>
        </div>
    </form>
</div>
<script>
    $(function () {

        $(".masked").mask("<?=$arParams['PHONE_MASK']?>");

        BX.ready(function(){
            loader = BX('ajax-preloader-wrap');
            BX.showWait = function(node, msg) {
                BX.style(loader, 'display', 'block');
                BX.addClass(loader, 'animated fadeIn');
            };
            BX.closeWait = function(node, obMsg) {
                BX.style(loader, 'display', 'none');
                BX.removeClass(loader, 'animated fadeIn');
            };
        });


        (function() {
            // trim polyfill : https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/String/Trim
            if (!String.prototype.trim) {
                (function() {
                    // Make sure we trim BOM and NBSP
                    var rtrim = /^[\s\uFEFF\xA0]+|[\s\uFEFF\xA0]+$/g;
                    String.prototype.trim = function() {
                        return this.replace(rtrim, '');
                    };
                })();
            }

            [].slice.call( document.querySelectorAll( 'input.input__field' ) ).forEach( function( inputEl ) {
                // in case the input is already filled..
                if( inputEl.value.trim() !== '' ) {
                    classie.add( inputEl.parentNode, 'input--filled' );
                }

                // events:
                inputEl.addEventListener( 'focus', onInputFocus );
                inputEl.addEventListener( 'blur', onInputBlur );
            } );

            function onInputFocus( ev ) {
                classie.add( ev.target.parentNode, 'input--filled' );
            }

            function onInputBlur( ev ) {
                if( ev.target.value.trim() === '' ) {
                    classie.remove( ev.target.parentNode, 'input--filled' );
                }
            }
        })();

    })
</script>