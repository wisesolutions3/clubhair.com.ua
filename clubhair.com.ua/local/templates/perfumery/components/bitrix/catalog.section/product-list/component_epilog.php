<? if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();

/**
 * @var array $arParams
 * @var array $templateData
 * @var string $templateFolder
 * @var CatalogSectionComponent $component
 */

global $APPLICATION;

if (!empty($templateData['TEMPLATE_LIBRARY']))
{
	$loadCurrency = false;
	if (!empty($templateData['CURRENCIES']))
	{
		$loadCurrency = \Bitrix\Main\Loader::includeModule('currency');
	}
	CJSCore::Init($templateData['TEMPLATE_LIBRARY']);

	if ($loadCurrency)
	{
		?>
		<script>
			BX.Currency.setCurrencies(<?=$templateData['CURRENCIES']?>);
		</script>
		<?
	}
}

//	lazy load and big data json answers
$request = \Bitrix\Main\Context::getCurrent()->getRequest();
if ($request->isAjaxRequest() && ($request->get('action') === 'showMore' || $request->get('action') === 'deferredLoad'))
{
	$content = ob_get_contents();
	ob_end_clean();

	list(, $itemsContainer) = explode('<!-- items-container -->', $content);
	list(, $paginationContainer) = explode('<!-- pagination-container -->', $content);

	if ($arParams['AJAX_MODE'] === 'Y')
	{
		$component->prepareLinks($paginationContainer);
	}

	$component::sendJsonAnswer(array(
		'items' => $itemsContainer,
		'pagination' => $paginationContainer
	));
}

?>
<script>
    /*GOOGLE ANALYTICS*/
    var items = [];
    
    <? foreach ($arResult['ANALITICS'] as $key => $item) { ?>
    items.push({
        "id": "<?=$item['ID']?>",
        "name": "<?=$item['NAME']?>",
        "list_name": "Catalog List Results",
        "list_position": <?=$key + 1?>,
    });
    <?}?>

    $( document ).ready(function() {
        gtag('event', 'view_item_list', {
            "items": items
        });

        $(document).on('click', '.product_item a', function(){
            var $parentBlock = $(this).parents('.item-ws');
            gtag('event', 'select_content', {
                "content_type": "product",
                "items": [
                    {
                        "id": $($parentBlock).data('id'),
                        "name": $($parentBlock).data('name'),
                        "list_name": "Catalog List Results",
                        "list_position": $($parentBlock).data('position'),
                    }
                ]
            });
        });
    });

</script>