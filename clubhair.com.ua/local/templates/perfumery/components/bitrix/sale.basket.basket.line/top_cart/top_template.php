<?if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED!==true) die();
/**
 * @global array $arParams
 * @global CUser $USER
 * @global CMain $APPLICATION
 * @global string $cartId
 */
$compositeStub = (isset($arResult['COMPOSITE_STUB']) && $arResult['COMPOSITE_STUB'] == 'Y');
?>
<!--<pre>--><?//=print_r($arParams);?><!--</pre>-->
<a href="<?=$arParams['PATH_TO_BASKET'];?>" class="icon-box-link <?=($arResult['NUM_PRODUCTS'] >= 1 ? 'basket-link' : 'animsition-link')?>" data-id="basket_in">
    <span class="icon-item basket-icon">
        <span class="goods_icon-counter"><?=$arResult['NUM_PRODUCTS']?></span>
    </span>
    <span class="icon-txt"><?=GetMessage('TSB1_CART')?></span>
</a>