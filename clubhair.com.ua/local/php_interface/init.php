<?
define('PREFIX_RU','RU');
define("MAIN_LANGUAGE","UA");
define("CATALOG_IBLOCK_ID","4");
define("BANNERS_IBLOK_ID","2");
include('WiseLang.php');
WsLanguage::UpdateBasketOfSiteID();

if (isset($_GET['wise_noinit']) && !empty($_GET['wise_noinit'])) {
    $strNoInit = strval($_GET['wise_noinit']);
    if ($strNoInit == 'N') {
        if (isset($_SESSION['wise_noinit']))
            unset($_SESSION['wise_noinit']);
    } elseif ($strNoInit == 'Y') {
        $_SESSION['wise_noinit'] = 'Y';
    }
}
if (!(isset($_SESSION['wise_noinit']) && $_SESSION['wise_noinit'] == 'Y')) {
    $dir = scandir(__DIR__ . "/wise_bitrix_class/");
    foreach ($dir as $value) {
        if ($value != "." && $value != "..") {
            require_once(__DIR__ . "/wise_bitrix_class/" . $value);
        }
    }
    $dir = scandir(__DIR__ . "/wise_custom_class/");
    foreach ($dir as $value) {
        if ($value != "." && $value != "..") {
            $arClasses[str_replace(".php", "", $value)] = "/local/php_interface/wise_custom_class/" . $value;
        }
    }
    $dir = scandir(__DIR__ . "/wise_events_class/");

    foreach ($dir as $value) {
        if ($value != "." && $value != "..") {
            $arClasses[str_replace(".php", "", $value)] = "/local/php_interface/wise_events_class/" . $value;
        }
    }

    if ($arClasses) {
        CModule::AddAutoloadClasses(
            '',
            $arClasses
        );
    }

    if (file_exists(__DIR__ . "/ws_events.php")) {
        require_once(__DIR__ . "/ws_events.php");
    }

    if (file_exists(__DIR__."/newPostUpdateElements.php")) {
        require_once(__DIR__."/newPostUpdateElements.php");
    }

    if (file_exists(__DIR__."/TurboSMSHandler.php")) {
        @require_once(__DIR__."/TurboSMSHandler.php");
    }
}



if(strpos($_SERVER['REQUEST_URI'], 'bitrix/admin/') !== false) {
    $arCustomAssetsDescr = array(
        'js' => array(
            '/local/templates/perfumery/js/admin/html-editor.js'
        ),
        'use' => CJSCore::USE_ADMIN
    );
    CJSCore::RegisterExt('custom_assets', $arCustomAssetsDescr);

    CJSCore::Init(array("custom_assets", "jquery2"));
}

