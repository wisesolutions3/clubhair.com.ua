<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();                        
if(\Bitrix\Main\Loader::includeSharewareModule("krayt.perfumery") == \Bitrix\Main\Loader::MODULE_DEMO_EXPIRED || 
   \Bitrix\Main\Loader::includeSharewareModule("krayt.perfumery") ==  \Bitrix\Main\Loader::MODULE_NOT_FOUND
    )
{ return false;}
use Bitrix\Main\Loader;
use Bitrix\Main\ModuleManager;
use Bitrix\Main\Localization\Loc;
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);

?>
<div class="brand_box">
<?$ElementID = $APPLICATION->IncludeComponent(
	"bitrix:news.detail",
	"flat",
	Array(
		"DISPLAY_DATE" => $arParams["DISPLAY_DATE"],
		"DISPLAY_NAME" => $arParams["DISPLAY_NAME"],
		"DISPLAY_PICTURE" => $arParams["DISPLAY_PICTURE"],
		"DISPLAY_PREVIEW_TEXT" => $arParams["DISPLAY_PREVIEW_TEXT"],
		"IBLOCK_TYPE" => $arParams["IBLOCK_TYPE"],
		"IBLOCK_ID" => $arParams["IBLOCK_ID"],
		"FIELD_CODE" => $arParams["DETAIL_FIELD_CODE"],
		"PROPERTY_CODE" => $arParams["DETAIL_PROPERTY_CODE"],
		"DETAIL_URL" => $arResult["FOLDER"].$arResult["URL_TEMPLATES"]["detail"],
		"SECTION_URL" => $arResult["FOLDER"].$arResult["URL_TEMPLATES"]["section"],
		"META_KEYWORDS" => $arParams["META_KEYWORDS"],
		"META_DESCRIPTION" => $arParams["META_DESCRIPTION"],
		"BROWSER_TITLE" => $arParams["BROWSER_TITLE"],
		"SET_CANONICAL_URL" => $arParams["DETAIL_SET_CANONICAL_URL"],
		"DISPLAY_PANEL" => $arParams["DISPLAY_PANEL"],
		"SET_LAST_MODIFIED" => $arParams["SET_LAST_MODIFIED"],
		"SET_TITLE" => "Y",
		"MESSAGE_404" => $arParams["MESSAGE_404"],
		"SET_STATUS_404" => $arParams["SET_STATUS_404"],
		"SHOW_404" => $arParams["SHOW_404"],
		"FILE_404" => $arParams["FILE_404"],
		"INCLUDE_IBLOCK_INTO_CHAIN" => $arParams["INCLUDE_IBLOCK_INTO_CHAIN"],
		"ADD_SECTIONS_CHAIN" => $arParams["ADD_SECTIONS_CHAIN"],
		"ACTIVE_DATE_FORMAT" => $arParams["DETAIL_ACTIVE_DATE_FORMAT"],
		"CACHE_TYPE" => $arParams["CACHE_TYPE"],
		"CACHE_TIME" => $arParams["CACHE_TIME"],
		"CACHE_GROUPS" => $arParams["CACHE_GROUPS"],
		"USE_PERMISSIONS" => $arParams["USE_PERMISSIONS"],
		"GROUP_PERMISSIONS" => $arParams["GROUP_PERMISSIONS"],
		"DISPLAY_TOP_PAGER" => $arParams["DETAIL_DISPLAY_TOP_PAGER"],
		"DISPLAY_BOTTOM_PAGER" => $arParams["DETAIL_DISPLAY_BOTTOM_PAGER"],
		"PAGER_TITLE" => $arParams["DETAIL_PAGER_TITLE"],
		"PAGER_SHOW_ALWAYS" => "N",
		"PAGER_TEMPLATE" => $arParams["DETAIL_PAGER_TEMPLATE"],
		"PAGER_SHOW_ALL" => $arParams["DETAIL_PAGER_SHOW_ALL"],
		"CHECK_DATES" => $arParams["CHECK_DATES"],
		"ELEMENT_ID" => $arResult["VARIABLES"]["ELEMENT_ID"],
		"ELEMENT_CODE" => $arResult["VARIABLES"]["ELEMENT_CODE"],
		"SECTION_ID" => $arResult["VARIABLES"]["SECTION_ID"],
		"SECTION_CODE" => $arResult["VARIABLES"]["SECTION_CODE"],
		"IBLOCK_URL" => $arResult["FOLDER"].$arResult["URL_TEMPLATES"]["news"],
		"SEARCH_PAGE" => $arResult["FOLDER"].$arResult["URL_TEMPLATES"]["search"],
		"USE_SHARE" => $arParams["USE_SHARE"],
		"SHARE_HIDE" => $arParams["SHARE_HIDE"],
		"SHARE_TEMPLATE" => $arParams["SHARE_TEMPLATE"],
		"SHARE_HANDLERS" => $arParams["SHARE_HANDLERS"],
		"SHARE_SHORTEN_URL_LOGIN" => $arParams["SHARE_SHORTEN_URL_LOGIN"],
		"SHARE_SHORTEN_URL_KEY" => $arParams["SHARE_SHORTEN_URL_KEY"],
		"ADD_ELEMENT_CHAIN" => (isset($arParams["ADD_ELEMENT_CHAIN"]) ? $arParams["ADD_ELEMENT_CHAIN"] : ''),
		"USE_RATING" => $arParams["USE_RATING"],
		"MAX_VOTE" => $arParams["MAX_VOTE"],
		"VOTE_NAMES" => $arParams["VOTE_NAMES"],
		"MEDIA_PROPERTY" => $arParams["MEDIA_PROPERTY"],
		"SLIDER_PROPERTY" => $arParams["SLIDER_PROPERTY"],
		"TEMPLATE_THEME" => $arParams["TEMPLATE_THEME"],
		"STRICT_SECTION_CHECK" => $arParams["STRICT_SECTION_CHECK"],
	),
	$component
);?>
<div class="">

    <?
    //свойство по которому идет фильтрация
    $PROP_BRAND = "BRAND";
    if($arParams['CATALOG_PROPPERTY_BRAND'])
    {
        $PROP_BRAND = $arParams['CATALOG_PROPPERTY_BRAND'];
    }

    if ($_GET['sect']):
        $sectionID = $_GET['sect'];
    else:
        //Получение разделов где есть товары с таким свойством
        $arSelect = Array("ID","SECTION_PAGE_URL","NAME");
        $arFilter = Array(
            "IBLOCK_ID"=>IntVal($arParams['CATALOG_IBLOCK_ID']),
            "GLOBAL_ACTIVE"=>"Y",
            "ACTIVE"=>"Y",
            "PROPERTY"=> [$PROP_BRAND => $ElementID]
        );
        $res = CIBlockSection::GetList(Array(), $arFilter, true, $arSelect,Array("nPageSize"=>1));
        $arFields = $res->GetNext();
        $sectionID = $arFields['ID'];
    endif;
    $GLOBALS['BRANDSECTID'] = $sectionID;
    //echo $sectionID;
    ?>

    <?
    $arSelect = Array("ID","SECTION_PAGE_URL","NAME","IBLOCK_SECTION_ID");
    $arFilter = Array(
        "IBLOCK_ID"=>IntVal($arParams['CATALOG_IBLOCK_ID']),
        "GLOBAL_ACTIVE"=>"Y",
        "ACTIVE"=>"Y",
        "PROPERTY"=> [$PROP_BRAND => $ElementID],
        "CNT_ACTIVE"=>"Y"
    );
    $maxLevel = 0;
    $res = CIBlockSection::GetList(Array(), $arFilter, true, $arSelect, Array("nPageSize"=>500));
    while($arFields = $res->GetNext())
    {
        $arFields['LEVEL_MENU'] = substr_count($arFields['SECTION_PAGE_URL'], '/') - 3;
        $arFields['URL'] = $APPLICATION->GetCurPageParam("sect=".$arFields['ID'], array("sect"));
        $arSectionBrand[$arFields['ID']] = $arFields;
    }



    function display_children ($arrSections, $parent = 0){
        foreach ($arrSections as &$item) {
            if ($item['IBLOCK_SECTION_ID'] == $parent):?>
                <ul class="bsel brand-list<?echo ($parent)? '' : ' parent'?><? echo ($GLOBALS['BRANDSECTID'] == $item['IBLOCK_SECTION_ID']) ? ' selected' :''?>">
                    <li class="brand-li">
                        <a href="<?=$item['URL']?>" class="bsel <? echo ($GLOBALS['BRANDSECTID'] == $item['ID']) ? 'brend-active' :''?>"><?=$item['NAME']?> (<?=$item['ELEMENT_CNT']?>)</a>
                        <i data-role="prop_angle_brand" class="bsel fa fa-angle-down<? echo ($GLOBALS['BRANDSECTID'] == $item['ID']) ? ' selected' :''?>"></i>
                        <?$temp_par = $item['ID'];
                        unset($item);
                        display_children($arrSections, $temp_par);?>
                    </li>
                </ul>
            <?endif;
        }
    }
?>
    <div class="catalog__content">
        <div class="catalog__filter">
            <div class="catalog__section-list brand">
                <div class="title">
                    <span><?=Loc::getMessage('SECTIONS')?></span>
                    <button class="open_list visible-xs" data-id="categories"></button>
                </div>
                <div class="section__list-wrapper hidden_list" id="categories">
                    <?display_children($arSectionBrand);?>
                </div>
            </div>

            <div class="catalog__section-filter">
                <?
                $GLOBALS['arrBrandName'] = array(
                    "PROPERTY_".$PROP_BRAND=>$ElementID,
                );
                $GLOBALS['mySmartFilter'] = array(
                    "PROPERTY_".$PROP_BRAND=>$ElementID,
                );
                ?>
                <?
                $APPLICATION->IncludeComponent(
                    "bitrix:catalog.smart.filter",
                    "brand",
                    array(
                        "IBLOCK_TYPE" => '',
                        "IBLOCK_ID" => $arParams['CATALOG_IBLOCK_ID'],
                        "SECTION_ID" => $sectionID,
                        "FILTER_NAME" => "arrBrandName",
                        "PREFILTER_NAME" => "mySmartFilter",
                        "PRICE_CODE" => $arParams['PRICE_CODE'],
                        "CACHE_TYPE" => $arParams["CACHE_TYPE"],
                        "CACHE_TIME" => $arParams["CACHE_TIME"],
                        "CACHE_GROUPS" => $arParams["CACHE_GROUPS"],
                        "SAVE_IN_SESSION" => "N",
                        "FILTER_VIEW_MODE" => $arParams["FILTER_VIEW_MODE"],
                        "XML_EXPORT" => "Y",
                        "SECTION_TITLE" => "NAME",
                        "SECTION_DESCRIPTION" => "DESCRIPTION",
                        'HIDE_NOT_AVAILABLE' => "Y",
                        "TEMPLATE_THEME" => $arParams["TEMPLATE_THEME"],
                        'CONVERT_CURRENCY' => $arParams['CONVERT_CURRENCY'],
                        'CURRENCY_ID' => $arParams['CURRENCY_ID'],
                        "SEF_MODE" => $arParams["SEF_MODE"],
                        "SEF_RULE" => $arResult["FOLDER"].$arResult["URL_TEMPLATES"]["smart_filter"],
                        "SMART_FILTER_PATH" => $arResult["VARIABLES"]["SMART_FILTER_PATH"],
                        "PAGER_PARAMS_NAME" => $arParams["PAGER_PARAMS_NAME"],
                        "INSTANT_RELOAD" => $arParams["INSTANT_RELOAD"],
                        "DISPLAY_ELEMENT_COUNT" => "Y",
                        "SHOW_ALL_WO_SECTION" => "Y",
                    ),
                    $component,
                    array('HIDE_ICONS' => 'Y')
                );
                ?>
            </div>
        </div>

        <div id="ajax_block" class="catalog__list">
            <?php
            if(!empty($_REQUEST['ajax']) && $_REQUEST['ajax'] == 'Y')
                $APPLICATION->RestartBuffer();
            ?>
            <?php
            $section = $sectionID;
            $arSection = CIBlockSection::GetByID($section);
            if ($arSection) {
                if ($ar_res = $arSection->GetNext()) {
                    $sectionName = $ar_res['NAME'];
                }
            }
            ?>
            <div class="title_box">
                <h1><?=$sectionName;?></h1>
                <div class="sorting_box">
                    <?php
                    $dbPriceType = CCatalogGroup::GetList(
                        array("SORT" => "ASC"),
                        array("NAME" => $arParams["PRICE_CODE"][0])
                    )->Fetch();
                    $ID_PRICE = $dbPriceType['ID'];

                    if(empty($_REQUEST['sort']))
                        $sort = 1;
                    else
                        $sort = $_REQUEST['sort'];

                    $sortFieldP = 1;
                    $sortFieldM = 2;
                    $sortFieldR = 3;
                    switch($sort)
                    {
                        case 1:
                            $arParams["ELEMENT_SORT_FIELD"] = 'shows';
                            $arParams["ELEMENT_SORT_ORDER"] = 'desc';
                            $sortFieldP = 4;
                            break;
                        case 2:
                            $arParams["ELEMENT_SORT_FIELD"] = 'CATALOG_PRICE_'.$ID_PRICE;
                            $arParams["ELEMENT_SORT_ORDER"] = 'asc';
                            $sortFieldM = 5;
                            break;
                        case 3:
                            $arParams["ELEMENT_SORT_FIELD"] = 'PROPERTY_name';
                            $arParams["ELEMENT_SORT_ORDER"] = 'desc';
                            $sortFieldR = 6;
                            break;
                        case 4:
                            $arParams["ELEMENT_SORT_FIELD"] = 'shows';
                            $arParams["ELEMENT_SORT_ORDER"] = 'asc';
                            $sortFieldP = 1;
                            break;
                        case 5:
                            $arParams["ELEMENT_SORT_FIELD"] = 'CATALOG_PRICE_'.$ID_PRICE;
                            $arParams["ELEMENT_SORT_ORDER"] = 'desc';
                            $sortFieldM = 2;
                            break;
                        case 6:
                            $arParams["ELEMENT_SORT_FIELD"] = 'PROPERTY_name';
                            $arParams["ELEMENT_SORT_ORDER"] = 'asc';
                            $sortFieldR = 3;
                            break;
                    }
                    ?>
                    <div class="sorting">
                        <div class="sorting_item <?if($sort == 1 || $sort == 4) echo 'active';?>">
                            <a href="<?=$APPLICATION->GetCurPageParam("sort=".$sortFieldP, array("sort","ajax"))?>">
                                <span><?=Loc::getMessage('K_TITLE_SORT_POPULAR')?></span>
                            </a>
                        </div>
                        <div class="sorting_item <?if($sort == 2 || $sort == 5) echo 'active';?>">
                            <a href="<?=$APPLICATION->GetCurPageParam("sort=".$sortFieldM, array("sort","ajax"))?>">
                                <span><?=Loc::getMessage('K_TITLE_SORT_PRICE')?></span> <span class="fa fa-long-arrow-<?if($sort == 2){?>up<?}elseif($sort == 5){?>down<?}?>"></span>
                            </a>
                        </div>
                        <div class="sorting_item <?if($sort == 3 || $sort == 6) echo 'active';?>">
                            <a href="<?=$APPLICATION->GetCurPageParam("sort=".$sortFieldR, array("sort","ajax"))?>">
                                <span><?=Loc::getMessage('K_TITLE_SORT_ALFA')?></span> <span class="fa fa-long-arrow-<?if($sort == 3){?>up<?}elseif($sort == 6){?>down<?}?>">
                            </a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="catalog__list-content">

                <?php
                $intSectionID = $APPLICATION->IncludeComponent(
                    "bitrix:catalog.section",
                    "product-list",
                    array(
                        "SHOW_ALL_WO_SECTION" => "Y",
                        "ACTION_VARIABLE" => "action",
                        "ADD_PICT_PROP" => "-",
                        "ADD_PROPERTIES_TO_BASKET" => "Y",
                        "ADD_SECTIONS_CHAIN" => "N",
                        "ADD_TO_BASKET_ACTION" => "ADD",
                        "AJAX_MODE" => "N",
                        "AJAX_OPTION_ADDITIONAL" => "",
                        "AJAX_OPTION_HISTORY" => "N",
                        "AJAX_OPTION_JUMP" => "N",
                        "AJAX_OPTION_STYLE" => "Y",
                        "BACKGROUND_IMAGE" => "",
                        "BASKET_URL" => SITE_DIR."basket/",
                        "BRAND_PROPERTY" => "-",
                        "BROWSER_TITLE" => "-",
                        "CACHE_FILTER" => "N",
                        "CACHE_GROUPS" => "Y",
                        "CACHE_TIME" => "36000000",
                        "CACHE_TYPE" => "A",
                        "COMPATIBLE_MODE" => "",
                        "CONVERT_CURRENCY" => "",
                        "CURRENCY_ID" => "",
                        //"CUSTOM_FILTER" => "{\"CLASS_ID\":\"CondGroup\",\"DATA\":{\"All\":\"OR\",\"True\":\"True\"},\"CHILDREN\":[]}",
                        "DATA_LAYER_NAME" => "",
                        "DETAIL_URL" => SITE_DIR."catalog/#SECTION_CODE_PATH#/#ELEMENT_CODE#/",
                        "DISABLE_INIT_JS_IN_COMPONENT" => "N",
                        "DISCOUNT_PERCENT_POSITION" => "top-left",
                        "DISPLAY_BOTTOM_PAGER" => "Y",
                        "DISPLAY_TOP_PAGER" => "N",
                        "ELEMENT_SORT_FIELD" => $arParams["ELEMENT_SORT_FIELD"],
                        "ELEMENT_SORT_ORDER" => $arParams["ELEMENT_SORT_ORDER"],
                        "ELEMENT_SORT_FIELD2" => "shows",
                        "ELEMENT_SORT_ORDER2" => "asc",
                        "ENLARGE_PRODUCT" => "STRICT",
                        "ENLARGE_PROP" => "-",
                        "FILTER_NAME" => "arrBrandName",
                        "HIDE_NOT_AVAILABLE" => "Y",
                        "HIDE_NOT_AVAILABLE_OFFERS" => "Y",
                        "IBLOCK_ID" => $arParams['CATALOG_IBLOCK_ID'],
                        "IBLOCK_TYPE" => "",
                        "INCLUDE_SUBSECTIONS" => "Y",
                        "LABEL_PROP" => array(
                            0 => "PRODUCT_HIT",
                            1 => "NEW",
                        ),
                        "LABEL_PROP_MOBILE" => "",
                        "LABEL_PROP_POSITION" => "top-left",
                        "LAZY_LOAD" => "N",
                        "LINE_ELEMENT_COUNT" => "3",
                        "LOAD_ON_SCROLL" => "N",
                        "MESSAGE_404" => "",
                        //"MESS_BTN_ADD_TO_BASKET" => "В корзину",
                        //	"MESS_BTN_BUY" => "Купить",
                        //	"MESS_BTN_DETAIL" => "Подробнее",
                        //	"MESS_BTN_LAZY_LOAD" => "Показать ещё",
                        //	"MESS_BTN_SUBSCRIBE" => "Подписаться",
                        //	"MESS_NOT_AVAILABLE" => "Нет в наличии",
                        //	"META_DESCRIPTION" => "-",
                        "META_KEYWORDS" => "-",
                        "OFFERS_CART_PROPERTIES" => array(
                        ),
                        "OFFERS_FIELD_CODE" => array(
                            0 => "",
                            1 => "",
                        ),
                        "OFFERS_LIMIT" => "0",
                        "OFFERS_PROPERTY_CODE" => $arParams['OFFERS_PROPERTY_CODE'],
                        "OFFERS_SORT_FIELD" => "shows",
                        "OFFERS_SORT_FIELD2" => "shows",
                        "OFFERS_SORT_ORDER" => "asc",
                        "OFFERS_SORT_ORDER2" => "asc",
                        "OFFER_ADD_PICT_PROP" => "-",
                        "OFFER_TREE_PROPS" => "",
                        "PAGER_BASE_LINK_ENABLE" => "N",
                        "PAGER_DESC_NUMBERING" => "N",
                        "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
                        "PAGER_SHOW_ALL" => "N",
                        "PAGER_SHOW_ALWAYS" => "N",
                        "PAGER_TEMPLATE" => "visual_1",
                        "PAGER_TITLE" => "Товары",
                        "PAGE_ELEMENT_COUNT" => 24,
                        "PARTIAL_PRODUCT_PROPERTIES" => "Y",
                        "PRICE_CODE" => $arParams['PRICE_CODE'],
                        "PRICE_VAT_INCLUDE" => "Y",
                        "PRODUCT_BLOCKS_ORDER" => "price,props,sku,quantityLimit,quantity,buttons,compare",
                        "PRODUCT_DISPLAY_MODE" => "N",
                        "PRODUCT_ID_VARIABLE" => "id",
                        "PRODUCT_PROPERTIES" => array(
                        ),
                        "PRODUCT_PROPS_VARIABLE" => "prop",
                        "PRODUCT_QUANTITY_VARIABLE" => "",
                        "PRODUCT_ROW_VARIANTS" => "[{'VARIANT':'3','BIG_DATA':false},{'VARIANT':'3','BIG_DATA':false},{'VARIANT':'3','BIG_DATA':false},{'VARIANT':'3','BIG_DATA':false},{'VARIANT':'3','BIG_DATA':false}]",
                        "PRODUCT_SUBSCRIPTION" => "Y",
                        "PROPERTY_CODE" => $arParams['CATALOG_PROPPERTY'],
                        "PROPERTY_CODE_MOBILE" => array(
                        ),
                        "RCM_PROD_ID" => '',
                        "RCM_TYPE" => "personal",
                        "SECTION_CODE" => "",
                        "SECTION_ID" => $sectionID,
                        //"SECTION_ID_VARIABLE" => "SECTION_ID",
                        "SECTION_URL" => SITE_DIR."catalog/#SECTION_CODE_PATH#/",
                        "SECTION_USER_FIELDS" => array(
                            0 => "",
                            1 => "",
                        ),
                        "SEF_MODE" => "Y",
                        "SET_BROWSER_TITLE" => "N",
                        "SET_LAST_MODIFIED" => "N",
                        "SET_META_DESCRIPTION" => "N",
                        "SET_META_KEYWORDS" => "Y",
                        "SET_STATUS_404" => "N",
                        "SET_TITLE" => "N",
                        "SHOW_404" => "N",
                        "SHOW_CLOSE_POPUP" => "Y",
                        "SHOW_DISCOUNT_PERCENT" => "Y",
                        "SHOW_FROM_SECTION" => "N",
                        "SHOW_MAX_QUANTITY" => "N",
                        "SHOW_OLD_PRICE" => "Y",
                        "SHOW_PRICE_COUNT" => "1",
                        "SHOW_SLIDER" => "N",
                        "SLIDER_INTERVAL" => "3000",
                        "SLIDER_PROGRESS" => "N",
                        "TEMPLATE_THEME" => "blue",
                        "USE_ENHANCED_ECOMMERCE" => "Y",
                        "USE_MAIN_ELEMENT_SECTION" => "N",
                        "USE_PRICE_COUNT" => "N",
                        "USE_PRODUCT_QUANTITY" => "N",
                        "DISPLAY_COMPARE" => "N",
                        "SEF_RULE" => "",
                        "SECTION_CODE_PATH" => "",
                        "COMPONENT_TEMPLATE" => "product-list",
                        "SECTION_DATA" => $section
                    ),
                    $component
                );
                $GLOBALS['CATALOG_CURRENT_SECTION_ID'] = $intSectionID;
                ?>
            </div>
            <?php

            if(!empty($_REQUEST['ajax']) && $_REQUEST['ajax'] == 'Y')
                die();
            ?>
        </div>
    </div>
</div>

</div>


<?php


if ($ElementID) {
    $ipropValues = new Bitrix\Iblock\InheritedProperty\ElementValues($arParams['IBLOCK_ID'], $ElementID);
    $ipropValues->clearValues();
    $path['IPROPERTY_VALUES'] = $ipropValues->getValues();


    if ($path['IPROPERTY_VALUES']['ELEMENT_META_TITLE']) {
        $APPLICATION->SetPageProperty('title', $path['IPROPERTY_VALUES']['ELEMENT_META_TITLE']);
    }

    if ($path['IPROPERTY_VALUES']['ELEMENT_META_KEYWORDS']) {
        $APPLICATION->SetPageProperty('keywords', $path['IPROPERTY_VALUES']['ELEMENT_META_KEYWORDS']);
    }

    if ($path['IPROPERTY_VALUES']['ELEMENT_META_DESCRIPTION']) {
        $APPLICATION->SetPageProperty('description', $path['IPROPERTY_VALUES']['ELEMENT_META_DESCRIPTION']);
    }
}
