<?php

class CMainHandlers
{
    public static function OnProlog()
    {
        self::FixAuthorizeAfterPayment();
    }

    private static function FixAuthorizeAfterPayment()
    {
        global $USER;
        if (!$USER->IsAuthorized() && $_REQUEST['pay_order']) {
            CModule::IncludeModule('sale');
            $userId = CSaleOrder::GetById($_REQUEST['pay_order'])['USER_ID'];
            $arUserGroup = CUser::GetUserGroup($userId);

            if (!in_array(1,$arUserGroup)) {
                $USER->Authorize($userId);
            }
        }
    }
}