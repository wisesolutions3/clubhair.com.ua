<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();                        
if(\Bitrix\Main\Loader::includeSharewareModule("krayt.perfumery") == \Bitrix\Main\Loader::MODULE_DEMO_EXPIRED || 
   \Bitrix\Main\Loader::includeSharewareModule("krayt.perfumery") ==  \Bitrix\Main\Loader::MODULE_NOT_FOUND
    )
{ return false;}

CJSCore::Init();

$cur = $APPLICATION->GetCurDir();

?>

<?if($arResult["FORM_TYPE"] == "login"):?>
    <a href="<?=SITE_DIR?>auth/" class="icon-box-link">
<!--        <span class="fa fa-user-o"></span>-->
        <span class="icon-item user-icon"></span>
        <span class="icon-txt"><?=GetMessage("AUTH_LOGIN_BUTTON")?></span>
    </a>
        <?if($arResult["NEW_USER_REGISTRATION"] == "Y"):?>
            <span class="visible-xs-inline-block mx-1">/</span>
            <a class="visible-xs-inline-block<?if(($cur == SITE_DIR.'auth/') && ($_GET['register'] == 'yes')):?> selected<?endif;?>" href="<?=SITE_DIR?>auth/?register=yes"><?=GetMessage("AUTH_REGISTER")?></a>
        <?endif;?>
    <?
else:
    ?>
    <a href="<?=$arResult["PROFILE_URL"]?>" class="icon-box-link user-link" title="<?=GetMessage("AUTH_PROFILE")?>">
<!--        <span class="fa fa-user-o"></span>-->
        <span class="icon-item user-icon"></span>
        <span class="icon-txt hidden-xs"><?=GetMessage('AUTH_PROFILE')?></span>
        <span class="icon-txt visible-xs-inline-block"><?=GetMessage('AUTH_CABINET')?></span>
    </a>
    <span class="visible-xs-inline-block mx-1">/</span>
    <a class="logout visible-xs-inline-block" href="<?=$APPLICATION->GetCurPageParam("logout=yes", Array("logout"))?>"><?=GetMessage("AUTH_LOGOUT_BUTTON")?></a>
<?endif?>