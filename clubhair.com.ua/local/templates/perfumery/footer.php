<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die(); ?>
<?php if ($APPLICATION->GetCurDir() != SITE_DIR) { ?>
    </div>
<?php } ?>
</div>

<div class="product_card_fast">
    <div class="fast_wrapper">
        <div class="preloader-wrapper">
            <div class="loader">
                <span></span>
                <span></span>
                <span></span>
            </div>
        </div>
        <div class="add_el_wrp">
            <button class="close-btn" data-dismiss="modal"></button>
            <div class="add_el"></div>
        </div>
    </div>
</div>

<div class="popup_mask"></div>

<footer class="footer">
    <div class="wrapper-inner">
        <div class="footer-content">
            <div class="footer-item footer-item-logo">
                <a href="<?= SITE_DIR; ?>" class="logo bottom-logo">
                    <?php $APPLICATION->IncludeFile(SITE_DIR . 'include/logo-inner.php') ?>
                </a>
                <div class="footer-phone">
                    <?php $APPLICATION->IncludeFile(SITE_DIR . 'include/top-phone.php') ?>
                </div>

                <div class="footer-item-copy">
                    <div class="footer-item-copy-item"><a href="https://www.wise-solutions.com.ua/" class="color-orange"
                                               target="_blank" rel="nofollow"><?= \Bitrix\Main\Localization\Loc::getMessage('K_COPY_DEV_WEB') ?></a>
                    </div>
                </div>

            </div>
            <div class="footer-item footer-item-menu footer-item-menu-catalog">
                <div class="footer-item-title">
                    <?php $APPLICATION->IncludeFile(SITE_DIR . 'include/footer-title_1.php') ?>
                </div>
                <div class="navigation-box">
                    <? $APPLICATION->IncludeComponent(
                        "bitrix:menu",
                        "bottom",
                        array(
                            "ALLOW_MULTI_SELECT" => "N",
                            "CHILD_MENU_TYPE" => "bottom_cat",
                            "DELAY" => "N",
                            "MAX_LEVEL" => "1",
                            "MENU_CACHE_GET_VARS" => array(),
                            "MENU_CACHE_TIME" => "3600",
                            "MENU_CACHE_TYPE" => "N",
                            "MENU_CACHE_USE_GROUPS" => "Y",
                            "ROOT_MENU_TYPE" => "bottom_cat",
                            "USE_EXT" => "Y",
                            "COMPONENT_TEMPLATE" => "bottom"
                        ),
                        false
                    ); ?>
                </div>
            </div>
            <div class="footer-item footer-item-menu">
                <div class="footer-item-title">
                    <?php $APPLICATION->IncludeFile(SITE_DIR . 'include/footer-title_5.php') ?>
                </div>
                <div class="navigation-box">
                    <? $APPLICATION->IncludeComponent(
                        "bitrix:menu",
                        "bottom",
                        array(
                            "ALLOW_MULTI_SELECT" => "N",
                            "CHILD_MENU_TYPE" => "bottom_center",
                            "DELAY" => "N",
                            "MAX_LEVEL" => "1",
                            "MENU_CACHE_GET_VARS" => array(),
                            "MENU_CACHE_TIME" => "3600",
                            "MENU_CACHE_TYPE" => "N",
                            "MENU_CACHE_USE_GROUPS" => "Y",
                            "ROOT_MENU_TYPE" => "bottom_center",
                            "USE_EXT" => "Y",
                            "COMPONENT_TEMPLATE" => "bottom"
                        ),
                        false
                    ); ?>
                </div>
            </div>
            <div class="footer-item footer-item-menu">
                <div class="footer-item-title">
                    <?php $APPLICATION->IncludeFile(SITE_DIR . 'include/footer-title_2.php') ?>
                </div>
                <div class="navigation-box">
                    <? $APPLICATION->IncludeComponent(
                        "bitrix:menu",
                        "bottom",
                        array(
                            "ALLOW_MULTI_SELECT" => "N",
                            "CHILD_MENU_TYPE" => "bottom",
                            "DELAY" => "N",
                            "MAX_LEVEL" => "1",
                            "MENU_CACHE_GET_VARS" => array(),
                            "MENU_CACHE_TIME" => "3600",
                            "MENU_CACHE_TYPE" => "N",
                            "MENU_CACHE_USE_GROUPS" => "Y",
                            "ROOT_MENU_TYPE" => "bottom",
                            "USE_EXT" => "N",
                            "COMPONENT_TEMPLATE" => "bottom"
                        ),
                        false
                    ); ?>
                </div>
            </div>
            <div class="footer-item footer-item-pays">
                <div class="footer-item-title">
                    <?php $APPLICATION->IncludeFile(SITE_DIR . 'include/footer-title_3.php') ?>
                </div>
                <div class="pays-box">
                    <?php $APPLICATION->IncludeFile(SITE_DIR . 'include/footer_pay_icon.php') ?>
                </div>
                <div class="social-box">
                    <div class="footer-item-title">
                        <?php $APPLICATION->IncludeFile(SITE_DIR . 'include/footer-title_4.php') ?>
                    </div>
                    <div class="social">
                        <?php $APPLICATION->IncludeFile(SITE_DIR . 'include/footer_soc_icon.php') ?>
                    </div>
                </div>
                <div id="bx-composite-banner" style=" margin-top: 10px;"></div>
            </div>
        </div>
    </div>
</footer>
<?php $APPLICATION->IncludeFile(SITE_DIR . 'include/metrica.php') ?>
</body>
</html>

<?
for ( $i = 0; $i <= 10; $i++) {
    if($_REQUEST["PAGEN_".$i] > 0) {
        $page_nav = $_REQUEST["PAGEN_" . $i];
    } elseif ($_REQUEST["SIZEN_".$i] > 0) {
        $page_size = $_REQUEST["SIZEN_".$i];
    } elseif (strpos($_SERVER['REQUEST_URI'], '/catalog/') !== false) {
        $is_catalog = true;
    }
}

if($page_nav > 0 || $page_size > 0 || $is_catalog) {
    $canonical = 'https://' . $_SERVER['SERVER_NAME'] . str_replace('?'.$_SERVER['QUERY_STRING'], '', $_SERVER['REQUEST_URI']);
    $APPLICATION->SetPageProperty('canonical', $canonical);
}