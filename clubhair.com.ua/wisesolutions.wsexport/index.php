<?
$content = false;
$documentRoot = $_SERVER["DOCUMENT_ROOT"];
require($_SERVER['DOCUMENT_ROOT'] . '/bitrix/modules/main/include/prolog_before.php');
$patchFilePath = $_SERVER["REQUEST_URI"];
if( stripos( $patchFilePath, "xml" ) !== false ){
    if( stripos( $documentRoot.$patchFilePath, "?" ) !== false ){
        $arRequest = explode( "?", $patchFilePath );
        $patchFilePath = $arRequest[0];
    }
}
$document = false;
if( file_exists( $documentRoot.$patchFilePath ) ){
    $document = $documentRoot.$patchFilePath;
}
elseif( file_exists( $documentRoot."/upload/wisesolutions.wsexport/".$patchFilePath ) ){
    $document = $documentRoot."/upload/wisesolutions.wsexport/".$patchFilePath;
}
elseif( file_exists( $documentRoot."/upload/".$patchFilePath ) ){
    $document = $documentRoot."/upload/".$patchFilePath;
}elseif(is_dir($documentRoot . "/upload/ws_export_cache/dynamic/".$patchFilePath)){
    $content = file_get_contents($documentRoot . "/upload/ws_export_cache/dynamic/".$patchFilePath."/header.txt");
    if ($handle = opendir($documentRoot . "/upload/ws_export_cache/dynamic/".$patchFilePath)) {
        while (false !== ($entry = readdir($handle))) {
            if ($entry != "." && $entry != ".." && $entry != "footer.txt" && $entry != "header.txt") {
                $content .= file_get_contents($documentRoot . "/upload/ws_export_cache/dynamic/".$patchFilePath."/".$entry);
            }
        }
        closedir($handle);
    }
    
    $content .= file_get_contents($documentRoot . "/upload/ws_export_cache/dynamic/".$patchFilePath."/footer.txt");
}


$inputEncoding = $_REQUEST["encoding"];
if(SITE_CHARSET=="windows-1251" && $inputEncoding=='utf-8'){
    $content= iconv("windows-1251", "UTF-8",$content);
}

if( $document || $content){
    if( stripos( $patchFilePath, "xml" ) !== false ){
        header("Expires: Thu, 19 Feb 1998 13:24:18 GMT");
        header("Last-Modified: ".gmdate("D, d M Y H:i:s")." GMT");
        header("Cache-Control: no-cache, must-revalidate");
        header("Cache-Control: post-check=0,pre-check=0");
        header("Pragma: no-cache");
        header('Content-Type: application/xml; charset='.$inputEncoding);
        if($content!=""){
            echo $content;
        }else{
            echo file_get_contents( $document );
        }
        
    }
    elseif( stripos( $patchFilePath, "zip" ) !== false ){
        header("Pragma: public");
        header("Expires: 0");
        header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
        header("Cache-Control: public");
        header("Content-Type: application/zip");
        header("Content-Transfer-Encoding: Binary");
        header("Content-Length: ".filesize($document));
        header("Content-Disposition: attachment; filename=\"".basename($document)."\"");
        readfile($document);
        exit;
    }
    else{
        header('Pragma: public');
        header('Expires: 0');
        header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
        header('Cache-Control: private', false);
        header('Content-Type: text/csv');
        header('Content-Disposition: attachment;filename=' . $patchFilePath);
        
        if($content!=""){
            echo $content;
        }else{
            echo file_get_contents( $document );
        }
    }
    die();
}
?>