<?
$MESS["CATALOG_CNT_PRODUCT_SECTION"] = "Кількість товарів в розділі";
$MESS["CATALOG_PROPPERTY_BRAND"] = "Властивість 'Бренд'";
$MESS["CP_BCS_OFFERS_PROPERTY_CODE"] = "Властивості торгових пропозицій";
$MESS["IBLOCK_PRICE_CODE"] = "Тип ціни";
$MESS["TP_BN_LIST_USE_SHARE"] = "Відображати панель соц. закладок в списку";
$MESS["TP_BN_MEDIA_PROPERTY"] = "Властивість для відображення медіа";
$MESS["TP_BN_SLIDER_PROPERTY"] = "Властивість із зображеннями для слайдера";
$MESS["TP_BN_TEMPLATE_THEME"] = "Колірна тема";
$MESS["TP_BN_THEME_BLACK"] = "темна";
$MESS["TP_BN_THEME_BLUE"] = "синя (тема за замовчуванням)";
$MESS["TP_BN_THEME_GREEN"] = "зелена";
$MESS["TP_BN_THEME_RED"] = "червона";
$MESS["TP_BN_THEME_SITE"] = "Брати тему з налаштувань сайту (для вирішення bitrix.eshop)";
$MESS["TP_BN_THEME_WOOD"] = "дерево";
$MESS["TP_BN_THEME_YELLOW"] = "жовта";
$MESS["TP_CATALOG_IBLOCK_ID"] = "Інформаційний блок каталогу";
$MESS["TP_CATALOG_PROPPERTY"] = "властивості каталогу";
$MESS["T_IBLOCK_DESC_NEWS_DATE"] = "Виводити дату елемента";
$MESS["T_IBLOCK_DESC_NEWS_PICTURE"] = "Виводити зображення для анонса";
$MESS["T_IBLOCK_DESC_NEWS_SHARE_SHORTEN_URL_KEY"] = "Ключ для для bit.ly";
$MESS["T_IBLOCK_DESC_NEWS_SHARE_SHORTEN_URL_LOGIN"] = "Логін для bit.ly";
$MESS["T_IBLOCK_DESC_NEWS_SHARE_SYSTEM"] = "Використовувані соц. закладки та мережі";
$MESS["T_IBLOCK_DESC_NEWS_SHARE_TEMPLATE"] = "Шаблон компонента панелі соц. закладок";
$MESS["T_IBLOCK_DESC_NEWS_TEXT"] = "Виводити текст анонса";
$MESS["T_IBLOCK_DESC_NEWS_USE_SHARE"] = "Відображати панель соц. закладок";
?>