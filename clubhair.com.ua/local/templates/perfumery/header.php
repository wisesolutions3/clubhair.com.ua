<?php
    if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)   die();
use Bitrix\Main\Localization\Loc;
use Bitrix\Main\Loader;
Loc::loadLanguageFile(__FILE__);
if(\Bitrix\Main\Loader::includeSharewareModule("krayt.perfumery") == \Bitrix\Main\Loader::MODULE_DEMO_EXPIRED ||
    \Bitrix\Main\Loader::includeSharewareModule("krayt.perfumery") ==  \Bitrix\Main\Loader::MODULE_NOT_FOUND
)
{
    $APPLICATION->IncludeFile(SITE_DIR.'include/alert_install.php');
    die();
}
global  $USER;
?><!DOCTYPE html>
<html lang="<?=LANGUAGE_ID?>">
    <head>
<meta name="facebook-domain-verification" content="vzhpm7ap2icn22pqe5isva3xaetvpb" />
        <meta name="viewport"
              content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="ie=edge">

        <link rel="icon" href="<?=SITE_DIR?>favicon.ico" >
        <?php
        // JS libraries
        $APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH."/js/jquery-3.2.1_min.js");
        $APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH."/js/maskinput.js");
        $APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH."/js/owl.carousel.js");
        $APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH."/js/jquery.cookie.js");
        $APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH."/js/bootstrap.min.js");
        $APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH."/js/jquery.fancybox.js");
        $APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH."/js/slick.js");
        $APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH."/js/animsition.min.js");
        $APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH."/js/jquery.nicescroll.js");
        $APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH."/js/swiper.min.js");
		$APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH."/js/jquery.formstyler.min.js");
        $APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH."/js/script.js");
        $APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH."/js/wise.js?ver=2");

        // CSS libraries
        $APPLICATION->SetAdditionalCSS(SITE_TEMPLATE_PATH."/css/owl.carousel.css");
        $APPLICATION->SetAdditionalCSS(SITE_TEMPLATE_PATH."/css/swiper.min.css");
        $APPLICATION->SetAdditionalCSS(SITE_TEMPLATE_PATH."/css/bootstrap_v4.css");
        $APPLICATION->SetAdditionalCSS(SITE_TEMPLATE_PATH."/css/slick.css");
        $APPLICATION->SetAdditionalCSS(SITE_TEMPLATE_PATH."/css/font-awesome.css");
        $APPLICATION->SetAdditionalCSS(SITE_TEMPLATE_PATH."/css/animsition.min.css");
		$APPLICATION->SetAdditionalCSS(SITE_TEMPLATE_PATH."/css/jquery.formstyler.css");
        $APPLICATION->SetAdditionalCSS(SITE_TEMPLATE_PATH."/css/jquery.formstyler.theme.css");

        $APPLICATION->ShowHead();
        ?>
        <title><?php $APPLICATION->ShowTitle();?></title>
<!-- Google Tag Manager -->
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-T87JXJ8');</script>
<!-- End Google Tag Manager -->
    </head>
    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-170971023-1"></script>
    <script>
        window.dataLayer = window.dataLayer || [];
        function gtag(){dataLayer.push(arguments);}
        gtag('js', new Date());

        gtag('config', 'UA-170971023-1');
    </script>



    <body class="">
<!-- Google Tag Manager (noscript) -->
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-T87JXJ8"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager (noscript) -->
        <?$APPLICATION->ShowPanel();?>
        <header class="header">
            <div class="header_rel">
                <div class="header-top">
                    <div class="ht-content">
                        <div class="wrapper-inner">
                            <div class="header-top-line" id="header-top-line">
                                <div class="hamburger visible-xs">
                                    <span class="line"></span>
                                </div>
                                <div class="header-phone">
                                    <?php $APPLICATION->IncludeFile(SITE_DIR.'include/top-phone.php') ?>
                                </div>
                                <a href="<?=SITE_DIR;?>" class="logo top-logo">
									<?php $APPLICATION->IncludeFile(SITE_DIR.'include/logo-main.php'); ?>
                                </a>
                                <div class="top-icon-box">
                                    <div class="icon-box_item" id="favour_in">
                                        <a href="<?=SITE_DIR;?>favorite/" class=" icon-box-link">
                                            <span class="icon-item favour-icon">
                                                <span class="goods_icon-counter">0</span>
                                            </span>
                                            <span class="icon-txt"><?=Loc::getMessage('K_TITLE_PERRFORMER')?></span>
                                        </a>
                                    </div>
                                    <div class="icon-box_item" id="basket_in">
                                        <?$APPLICATION->IncludeComponent(
                                            "bitrix:sale.basket.basket.line",
                                            "top_cart",
                                            array(
                                                "HIDE_ON_BASKET_PAGES" => "Y",
                                                "PATH_TO_BASKET" => SITE_DIR."basket/",
                                                "PATH_TO_ORDER" => SITE_DIR."personal/order/",
                                                "PATH_TO_PERSONAL" => SITE_DIR."personal/",
                                                "PATH_TO_PROFILE" => SITE_DIR."personal/",
                                                "PATH_TO_REGISTER" => SITE_DIR."login/",
                                                "POSITION_FIXED" => "N",
                                                "POSITION_HORIZONTAL" => "right",
                                                "POSITION_VERTICAL" => "top",
                                                "SHOW_AUTHOR" => "Y",
                                                "SHOW_DELAY" => "N",
                                                "SHOW_EMPTY_VALUES" => "Y",
                                                "SHOW_IMAGE" => "Y",
                                                "SHOW_NOTAVAIL" => "N",
                                                "SHOW_NUM_PRODUCTS" => "Y",
                                                "SHOW_PERSONAL_LINK" => "N",
                                                "SHOW_PRICE" => "Y",
                                                "SHOW_PRODUCTS" => "Y",
                                                "SHOW_SUMMARY" => "Y",
                                                "SHOW_TOTAL_PRICE" => "N",
                                                "COMPONENT_TEMPLATE" => "top_cart",
                                                "PATH_TO_AUTHORIZE" => SITE_DIR."login/",
                                                "SHOW_REGISTRATION" => "Y",
                                                "MAX_IMAGE_SIZE" => "70"
                                            ),
                                            false
                                        );?>

                                    </div>

                                    <div class="icon-box_item" id="user">
                                    <? $APPLICATION->IncludeComponent("bitrix:system.auth.form", "top_auth", Array(
                                            "FORGOT_PASSWORD_URL" => "",
                                            "PROFILE_URL" => SITE_DIR."personal",
                                            "REGISTER_URL" => SITE_DIR."personal/",
                                            "SHOW_ERRORS" => "N",
                                            "COMPONENT_TEMPLATE" => ""
                                        ),
                                        false
                                    ); ?>

                                    <?
                                    if(!empty($_GET['logout']) && $_GET['logout'] == 'Y')
                                    {
                                        $USER->Logout();
                                    }
                                    global $USER;

                                    if ($USER->IsAuthorized()) {
                                        ?>
                                        <div class="user-navigation">
                                            <span class="arrow-top"></span>

                                            <a href="<?=SITE_DIR?>personal/" class="user-name" title="<?=GetMessage("AUTH_PROFILE")?>">
<!--                                                <span class="fa fa-user"></span>-->
                                                <span class="icon-item user-icon"></span>
                                                <span class="icon-txt"><?=($USER->GetFullName())?$USER->GetFullName():$USER->GetLogin();?></span>
                                            </a>

                                            <?$APPLICATION->IncludeComponent(
                                                "bitrix:menu",
                                                "personal",
                                                Array(
                                                    "ALLOW_MULTI_SELECT" => "N",
                                                    "CHILD_MENU_TYPE" => "",
                                                    "DELAY" => "N",
                                                    "MAX_LEVEL" => "3",
                                                    "MENU_CACHE_GET_VARS" => array(""),
                                                    "MENU_CACHE_TIME" => "3600",
                                                    "MENU_CACHE_TYPE" => "N",
                                                    "MENU_CACHE_USE_GROUPS" => "Y",
                                                    "ROOT_MENU_TYPE" => "personal",
                                                    "USE_EXT" => "Y"
                                                )
                                            );?>

                                            <a class="logout btn-green-gradient" href="<?=$APPLICATION->GetCurPageParam("logout=yes", Array("logout"))?>">
                                                <span><?=GetMessage("AUTH_LOGOUT_BUTTON")?></span>
                                            </a>
                                        </div>
                                        <?
                                    }
                                    ?>
                                </div>
                            <?
                            $APPLICATION->IncludeComponent(
                            'wisesolutions:language.dropdown',
                            '.default',
                                array()
                            );
                            ?>
                            </div>
                        </div>
                    </div>
                </div>
                </div>
                <div class="header-bottom">
                    <div class="hb-content">
                        <div class="wrapper-inner">
                            <div class="header-bottom-line">
                                <a href="<?=SITE_DIR;?>" class="logo top-logo">
                                    <?php $APPLICATION->IncludeFile(SITE_DIR.'include/logo-inner.php') ?>
                                </a>
                                <div class="top-nav-wrapper">
                                    <div class="top-nav-wrapper-overflow">

                                        <div class="icon-box_item visible-xs">
                                            <? $APPLICATION->IncludeComponent("bitrix:system.auth.form", "top_auth", Array(
                                                "FORGOT_PASSWORD_URL" => "",
                                                "PROFILE_URL" => SITE_DIR."personal",
                                                "REGISTER_URL" => SITE_DIR."personal/",
                                                "SHOW_ERRORS" => "N",
                                                "COMPONENT_TEMPLATE" => "",
                                            ),
                                                false
                                            ); ?>
                                        </div>

                                        <div class="top-catalog-menu">
                                            <div class="top-catalog-menu-title visible-xs">
                                                <button class="open_list" data-id="cat_mobile"></button>
                                                <a href="<?=SITE_DIR?>catalog/" class=""><?=Loc::getMessage('K_LINK_CATALOG')?></a>
                                            </div>
                                            <div class="bx-top-nav-wrapper hidden_list" id="cat_mobile">
                                                <?php $APPLICATION->IncludeFile(SITE_DIR.'include/header_top_menu.php') ?>
                                            </div>
                                        </div>

                                        <div class="top-brands-menu">
                                            <div class="top-catalog-menu-title visible-xs">
                                                <button class="open_list" data-id="brand_mobile"></button>
                                                <a href="<?=SITE_DIR?>brands/" class=""><?=Loc::getMessage('K_LINK_BRAND')?></a>
                                            </div>
                                            <div class="bx-top-nav-wrapper hidden_list" id="brand_mobile"></div>
                                        </div>

                                        <div class="visible-xs">
                                            <?$APPLICATION->IncludeComponent(
                                                "bitrix:menu",
                                                "bottom",
                                                array(
                                                    "ALLOW_MULTI_SELECT" => "N",
                                                    "CHILD_MENU_TYPE" => "bottom",
                                                    "DELAY" => "N",
                                                    "MAX_LEVEL" => "1",
                                                    "MENU_CACHE_GET_VARS" => array(
                                                    ),
                                                    "MENU_CACHE_TIME" => "3600",
                                                    "MENU_CACHE_TYPE" => "N",
                                                    "MENU_CACHE_USE_GROUPS" => "Y",
                                                    "ROOT_MENU_TYPE" => "bottom",
                                                    "USE_EXT" => "N",
                                                    "COMPONENT_TEMPLATE" => "bottom"
                                                ),
                                                false
                                            );?>
                                        </div>

										<div class="mobile-language">
                                            <?
                                            $APPLICATION->IncludeComponent(
                                                'wisesolutions:language.dropdown',
                                                'mobile',
                                                array()
                                            );
                                            ?>
										</div>

                                    </div>
                                </div>
                                <div id="top-icon-wrapper"></div>
                                <div class="icon-box_item" id="search_in">
                                    <a href="" class="icon-box-link search-link">
                                        <span class="icon-item search-icon"></span>
                                        <span class="icon-txt"><?=Loc::getMessage('K_TITLE_SEARCH')?></span>
                                    </a>
                                    <div class="search_wrapper">
                                        <div class="wrapper-inner">
                                            <div class="title"><?=Loc::getMessage('K_ALERT_SEARCH')?></div>
                                            <div class="search__input">
                                                <?$APPLICATION->IncludeComponent(
                                                    "bitrix:search.title",
                                                    "main",
                                                    array(
                                                        "CATEGORY_0" => array(
                                                            0 => "iblock_offers",
                                                            1 => "iblock_catalog",
                                                        ),
                                                        "CATEGORY_0_TITLE" => "main",
                                                        "CATEGORY_0_iblock_catalog" => array(
                                                            0 => "all",
                                                        ),
                                                        "CATEGORY_0_iblock_offers" => array(
                                                            0 => "all",
                                                        ),
                                                        "CHECK_DATES" => "N",
                                                        "CONTAINER_ID" => "title-search",
                                                        "INPUT_ID" => "title-search-input",
                                                        "NUM_CATEGORIES" => "1",
                                                        "ORDER" => "rank",
                                                        "PAGE" => "#SITE_DIR#catalog/search.php",
                                                        "SHOW_INPUT" => "Y",
                                                        "SHOW_OTHERS" => "N",
                                                        "TOP_COUNT" => "5",
                                                        "USE_LANGUAGE_GUESS" => "Y",
                                                        "COMPONENT_TEMPLATE" => "main",
                                                        "TEMPLATE_THEME" => "blue",
                                                        "PRICE_CODE" => array(
                                                            0 => "ru",
                                                        ),
                                                        "PRICE_VAT_INCLUDE" => "Y",
                                                        "PREVIEW_TRUNCATE_LEN" => "",
                                                        "SHOW_PREVIEW" => "Y",
                                                        "CONVERT_CURRENCY" => "N",
                                                        "PREVIEW_WIDTH" => "75",
                                                        "PREVIEW_HEIGHT" => "75"
                                                    ),
                                                    false
                                                );?>

                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="header-brand-line">
                <div class="header_rel">
                    <div class="wrapper-inner">
                        <?php $APPLICATION->IncludeFile(SITE_DIR.'include/krayt.brands.menu.php') ?>
                    </div>
                </div>
            </div>
        </header>
        <div class="main_content" <?=($APPLICATION->GetCurDir() == SITE_DIR.'contacts/' ? 'style="margin-bottom:0;"' : '');?>>

            <?php if($APPLICATION->GetCurDir() != SITE_DIR){?>
                <div class="breadcrumbs">
                    <div class="wrapper-inner">
                        <?$APPLICATION->IncludeComponent("bitrix:breadcrumb", "main", Array(
                            "PATH" => "",
                                "SITE_ID" => SITE_ID,
                                "START_FROM" => "0",
                            ),
                            false
                        );?>
                    </div>
                </div>
                <div class="wrapper-inner animsition">
            <?php }?>