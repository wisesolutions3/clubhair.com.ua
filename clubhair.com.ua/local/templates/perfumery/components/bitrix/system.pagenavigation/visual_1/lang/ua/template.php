<?
$MESS["nav_all"] = "Усе";
$MESS["nav_all_descr"] = "Увесь список";
$MESS["nav_next_title"] = "Наступна сторінка";
$MESS["nav_of"] = "з";
$MESS["nav_page_current_title"] = "Поточна сторінка";
$MESS["nav_page_num_title"] = "Сторінка #NUM#";
$MESS["nav_pages"] = "сторінки:";
$MESS["nav_prev_title"] = "Попередня сторінка";
$MESS["nav_show_pages"] = "За стор.";
$MESS["nav_size_descr"] = "за:";
$MESS["next_title"] = "Наступна";
$MESS["prev_title"] = "Попередня";
$MESS["more"] = "Показати наступні товари";
?>