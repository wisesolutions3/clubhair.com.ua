<? if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();

use \Bitrix\Main\Localization\Loc;

/**
 * @global CMain $APPLICATION
 * @var array $arParams
 * @var array $item
 * @var array $actualItem
 * @var array $minOffer
 * @var array $itemIds
 * @var array $price
 * @var array $measureRatio
 * @var bool $haveOffers
 * @var bool $showSubscribe
 * @var array $morePhoto
 * @var bool $showSlider
 * @var string $imgTitle
 * @var string $productTitle
 * @var string $buttonSizeClass
 * @var CatalogSectionComponent $component
 */
$file = CFile::ResizeImageGet($item['PREVIEW_PICTURE']['ID'], array('width' => '288', 'height' => '250'), BX_RESIZE_IMAGE_PROPORTIONAL, true);
?>
<div class="product_item">
    <div class="product_item_img">
        <a href="<?= $item['DETAIL_PAGE_URL'] ?>" title="<?= $imgTitle ?>" data-entity="image-wrapper">
            <div class="product_item_wrapper_img">
                <img class="product_item_img-bg" src="<?= $file['src'] ?>"
                     width="<?= $file["width"] ?>" height="<?= $file["height"] ?>"
                     alt="<?= $productTitle ?>" title="<?= $productTitle ?>">
            </div>
        </a>

        <div class="product_card-flags">
            <? if ($item['LABEL']) {
                ?>
                <div id="<?= $itemIds['STICKER_ID'] ?>">
                    <?
                    if (!empty($item['LABEL_ARRAY_VALUE'])) {
                        foreach ($item['LABEL_ARRAY_VALUE'] as $code => $value) {
                            ?>
                            <div class="flag-item <?= strtolower($code); ?>" title="<?= $value ?>"><?= $value ?></div>
                            <?
                        }
                    }
                    ?>
                </div>
                <?
            } ?>
            <?
            if ($arParams['SHOW_DISCOUNT_PERCENT'] === 'Y') {
                ?>
                <div class="flag-item stock" id="<?= $itemIds['DSC_PERC'] ?>"
                     style="display: <?= ($price['PERCENT'] > 0 ? '' : 'none') ?>;"><?= -$price['PERCENT'] ?>%
                </div>
                <?
            }
            ?>
        </div>

        <div class="fast_view btn-green-gradient" data-href="<?= $item['DETAIL_PAGE_URL'] ?>">
            <span class="title"><?= GetMessage('FAST_VIEW_TITLE'); ?></span>
        </div>
    </div>


    <div class="product_item_name_box">
        <div class="product_item_title">
            <div class="name"><a href="<?= $item['DETAIL_PAGE_URL'] ?>" title="<?= $productTitle ?>">
                    <?= $productTitle ?>
                </a>
            </div>

        </div>
<!--        <div class="list-preview-text">-->
<!--            --><?//
//
//            $text = TruncateText(
//                strip_tags(html_entity_decode($item['PREVIEW_TEXT'] ? $item['PREVIEW_TEXT'] : $item['DETAIL_TEXT'])),
//                100
//            );
//
//            if (strlen($text) > 1) {
//                echo $text;
//            }
//            ?>
<!--        </div>-->
    </div>


    <div class="product_item__price" data-entity="price-block">
        <div class="price" id="<?= $itemIds['PRICE'] ?>">
            <?
            if (!empty($price)) {
                if ($arParams['PRODUCT_DISPLAY_MODE'] === 'N' && $haveOffers) {
                    ?>
                    <span><? echo Loc::getMessage(
                            'CT_BCI_TPL_MESS_PRICE_SIMPLE_MODE',
                            array(
                                '#PRICE#' => $price['PRINT_RATIO_PRICE'],
                                '#VALUE#' => $measureRatio,
                                '#UNIT#' => $minOffer['ITEM_MEASURE']['TITLE']
                            )
                        ); ?></span>
                    <?php
                } else {
                    ?>
                    <span><?= $price['PRINT_RATIO_PRICE']; ?></span>
                    <?php
                }
            }
            ?>
        </div>
        <?
        if ($arParams['SHOW_OLD_PRICE'] === 'Y') {
            ?>
            <div class="old_price"
                 id="<?= $itemIds['PRICE_OLD'] ?>" <?= ($price['RATIO_PRICE'] >= $price['RATIO_BASE_PRICE'] ? 'style="display: none;"' : '') ?>>
                <span><?= $price['PRINT_RATIO_BASE_PRICE'] ?></span>
            </div>
            <?
        }
        ?>
        <div class="product-item-info-container" data-entity="buttons-block">
            <?
            if (!$haveOffers) {
                if ($actualItem['CAN_BUY']) {
                    ?>
                    <div class="product_item__ico basket_icon product-item-button-container"
                         id="<?= $itemIds['BASKET_ACTIONS'] ?>">
                        <a id="<?= $itemIds['BUY_LINK'] ?>"
                           href="javascript:void(0)" rel="nofollow">
                            <?= ($arParams['ADD_TO_BASKET_ACTION'] === 'BUY' ? $arParams['MESS_BTN_BUY'] : $arParams['MESS_BTN_ADD_TO_BASKET']) ?>
                        </a>
                    </div>
                    <?
                } else {
                    ?>
                    <div class="product_item__ico basket_icon product-item-button-container" style="display: none;">
                        <?
                        if ($showSubscribe) {
                            $APPLICATION->IncludeComponent(
                                'bitrix:catalog.product.subscribe',
                                '',
                                array(
                                    'PRODUCT_ID' => $actualItem['ID'],
                                    'BUTTON_ID' => $itemIds['SUBSCRIBE_LINK'],
                                    'BUTTON_CLASS' => 'btn btn-default ' . $buttonSizeClass,
                                    'DEFAULT_DISPLAY' => true,
                                    'MESS_BTN_SUBSCRIBE' => $arParams['~MESS_BTN_SUBSCRIBE'],
                                ),
                                $component,
                                array('HIDE_ICONS' => 'Y')
                            );
                        }
                        ?>
                        <div id="<?= $itemIds['NOT_AVAILABLE_MESS'] ?>" href="javascript:void(0)" rel="nofollow">
                            <?= $arParams['MESS_NOT_AVAILABLE'] ?>
                        </div>
                    </div>
                    <?
                }
            } else {
                if ($arParams['PRODUCT_DISPLAY_MODE'] === 'Y') {
                    ?>
                    <div class="product_item__ico basket_icon product-item-button-container">
                        <?
                        if ($showSubscribe) {
                            $APPLICATION->IncludeComponent(
                                'bitrix:catalog.product.subscribe',
                                '',
                                array(
                                    'PRODUCT_ID' => $item['ID'],
                                    'BUTTON_ID' => $itemIds['SUBSCRIBE_LINK'],
                                    'BUTTON_CLASS' => 'btn btn-default ' . $buttonSizeClass,
                                    'DEFAULT_DISPLAY' => !$actualItem['CAN_BUY'],
                                    'MESS_BTN_SUBSCRIBE' => $arParams['~MESS_BTN_SUBSCRIBE'],
                                ),
                                $component,
                                array('HIDE_ICONS' => 'Y')
                            );
                        }
                        ?>
                        <a id="<?= $itemIds['NOT_AVAILABLE_MESS'] ?>" href="javascript:void(0)" rel="nofollow"
                            <?= ($actualItem['CAN_BUY'] ? 'style="display: none;"' : '') ?>>
                            <?= $arParams['MESS_NOT_AVAILABLE'] ?>
                        </a>
                        <div id="<?= $itemIds['BASKET_ACTIONS'] ?>" <?= ($actualItem['CAN_BUY'] ? '' : 'style="display: none;"') ?>>
                            <a class="btn btn-default <?= $buttonSizeClass ?>" id="<?= $itemIds['BUY_LINK'] ?>"
                               href="javascript:void(0)" rel="nofollow">
                                <?= ($arParams['ADD_TO_BASKET_ACTION'] === 'BUY' ? $arParams['MESS_BTN_BUY'] : $arParams['MESS_BTN_ADD_TO_BASKET']) ?>
                            </a>
                        </div>
                    </div>
                    <?
                } else {
                    ?>
                    <!--                    <div class="product-item-button-container">-->
                    <!--                        <a class="btn btn-default --><?//=$buttonSizeClass
                    ?><!--" href="--><?//=$item['DETAIL_PAGE_URL']
                    ?><!--">-->
                    <!--                            --><?//=$arParams['MESS_BTN_DETAIL']
                    ?>
                    <!--                        </a>-->
                    <!--                    </div>-->
                    <?
                }
            }
            ?>
            <div class="product_item__ico favour-in to_favorites" data-cookieid="<?= $item['ID']; ?>">
                <svg class="heart" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink"
                     version="1.1" id="Capa_1" x="0px" y="0px" viewBox="0 0 471.701 471.701"
                     style="enable-background:new 0 0 471.701 471.701;" xml:space="preserve" width="512px"
                     height="512px">
                            <g>
                                <path d="M433.601,67.001c-24.7-24.7-57.4-38.2-92.3-38.2s-67.7,13.6-92.4,38.3l-12.9,12.9l-13.1-13.1   c-24.7-24.7-57.6-38.4-92.5-38.4c-34.8,0-67.6,13.6-92.2,38.2c-24.7,24.7-38.3,57.5-38.2,92.4c0,34.9,13.7,67.6,38.4,92.3   l187.8,187.8c2.6,2.6,6.1,4,9.5,4c3.4,0,6.9-1.3,9.5-3.9l188.2-187.5c24.7-24.7,38.3-57.5,38.3-92.4   C471.801,124.501,458.301,91.701,433.601,67.001z M414.401,232.701l-178.7,178l-178.3-178.3c-19.6-19.6-30.4-45.6-30.4-73.3   s10.7-53.7,30.3-73.2c19.5-19.5,45.5-30.3,73.1-30.3c27.7,0,53.8,10.8,73.4,30.4l22.6,22.6c5.3,5.3,13.8,5.3,19.1,0l22.4-22.4   c19.6-19.6,45.7-30.4,73.3-30.4c27.6,0,53.6,10.8,73.2,30.3c19.6,19.6,30.3,45.6,30.3,73.3   C444.801,187.101,434.001,213.101,414.401,232.701z"/>
                            </g>
                    <g>
                    </g>
                    <g>
                    </g>
                    <g>
                    </g>
                    <g>
                    </g>
                    <g>
                    </g>
                    <g>
                    </g>
                    <g>
                    </g>
                    <g>
                    </g>
                    <g>
                    </g>
                    <g>
                    </g>
                    <g>
                    </g>
                    <g>
                    </g>
                    <g>
                    </g>
                    <g>
                    </g>
                    <g>
                    </g>
</svg>
                <svg class="heart active" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink"
                     version="1.1" id="Capa_1" x="0px" y="0px" width="512px" height="512px" viewBox="0 0 32 32"
                     style="enable-background:new 0 0 32 32;" xml:space="preserve">
                            <g>
                                <g>
                                    <path d="M14.708,15.847C14.252,14.864,14,13.742,14,12.5s0.252-2.489,0.708-3.659c0.455-1.171,1.114-2.266,1.929-3.205    c0.814-0.938,1.784-1.723,2.86-2.271C20.574,2.814,21.758,2.5,23,2.5s2.426,0.252,3.503,0.707c1.077,0.456,2.046,1.115,2.86,1.929    c0.813,0.814,1.474,1.784,1.929,2.861C31.749,9.073,32,10.258,32,11.5s-0.252,2.427-0.708,3.503    c-0.455,1.077-1.114,2.047-1.929,2.861C28.55,18.678,17.077,29.044,16,29.5l0,0l0,0C14.923,29.044,3.45,18.678,2.636,17.864    c-0.814-0.814-1.473-1.784-1.929-2.861C0.252,13.927,0,12.742,0,11.5s0.252-2.427,0.707-3.503C1.163,6.92,1.821,5.95,2.636,5.136    C3.45,4.322,4.42,3.663,5.497,3.207C6.573,2.752,7.757,2.5,9,2.5s2.427,0.314,3.503,0.863c1.077,0.55,2.046,1.334,2.861,2.272    c0.814,0.939,1.473,2.034,1.929,3.205C17.748,10.011,18,11.258,18,12.5s-0.252,2.364-0.707,3.347    c-0.456,0.983-1.113,1.828-1.929,2.518"
                                          fill="#ea88b0"/>
                                </g>
                            </g>
                    <g>
                    </g>
                    <g>
                    </g>
                    <g>
                    </g>
                    <g>
                    </g>
                    <g>
                    </g>
                    <g>
                    </g>
                    <g>
                    </g>
                    <g>
                    </g>
                    <g>
                    </g>
                    <g>
                    </g>
                    <g>
                    </g>
                    <g>
                    </g>
                    <g>
                    </g>
                    <g>
                    </g>
                    <g>
                    </g>
                        </svg>
            </div>
        </div>

    </div>

</div>