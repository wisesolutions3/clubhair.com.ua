<?
$MESS["ACTIVE"] = "Активний:";
$MESS["EMAIL"] = "Email:";
$MESS["LAST_LOGIN"] = "Остання авторизація:";
$MESS["LAST_NAME"] = "Прізвище:";
$MESS["LAST_UPDATE"] = "Дата оновлення:";
$MESS["LOGIN"] = "Логін (мін. 3 символи):";
$MESS["MAIN_RESET"] = "Скасувати";
$MESS["NAME"] = "ім'я:";
$MESS["NEW_PASSWORD"] = "Новий пароль (хв. 6 символів):";
$MESS["NEW_PASSWORD_CONFIRM"] = "Підтвердження нового пароля:";
$MESS["NEW_PASSWORD_REQ"] = "Новий пароль:";
$MESS["PERSONAL_BIRTHDAY"] = "Дата народження:";
$MESS["PROFILE_DATA_SAVED"] = "зміни збережені";
$MESS["RESET"] = "Скинути";
$MESS["SAVE"] = "Зберегти зміни";
$MESS["SECOND_NAME"] = "По батькові:";
$MESS["SPS_CHAIN_PRIVATE"] = "Профіль";
$MESS["SPS_TITLE_PRIVATE"] = "Профіль";
$MESS['SEX'] = 'Стать:';
$MESS['MEN'] = 'Чоловік';
$MESS['WOMAN'] = 'Жінка';
?>