<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();

/**
 * @var array $arParams
 * @var array $arResult
 * @var SaleOrderAjax $component
 */

$iblockId = 7;

$cityOptions[] = GetMessage('CHOOSE_CITY_TITLE');
$cityOptionsSort[] = 0;
$departmentOptions[] = GetMessage('CHOOSE_DEPARTMENT_TITLE');
$departmentOptionsSort[] = 0;

$active_delivery_id = 0;
foreach ($arResult['DELIVERY'] as $delivery_id => $ar_delivery) {
    if($ar_delivery['CHECKED'] == 'Y') {
        $active_delivery_id = $delivery_id;
        break;
    }
}

foreach ($arResult['JS_DATA']['ORDER_PROP']['properties'] as $key => $property) {
    if ($property['CODE'] == 'CITY_WS') {
        if($active_delivery_id == 7 || $active_delivery_id == 8) {
            $arResult['JS_DATA']['ORDER_PROP']['properties'][$key]['NAME'] = GetMessage('WS_CITY_ALTERNATIVE_NAME');
        }

        unset($arResult['JS_DATA']['ORDER_PROP']['properties'][$key]['RELATION']);
        unset($arResult['JS_DATA']['ORDER_PROP']['properties'][$key]['VALUE']);
        $arResult['JS_DATA']['ORDER_PROP']['properties'][$key]['TYPE'] = 'ENUM';
        $arResult['JS_DATA']['ORDER_PROP']['properties'][$key]['OPTIONS'] = $cityOptions;
        $arResult['JS_DATA']['ORDER_PROP']['properties'][$key]['OPTIONS_SORT'] = $cityOptionsSort;
    }
    if ($property['CODE'] == 'DEPARTMENT_WS') {
        unset($arResult['JS_DATA']['ORDER_PROP']['properties'][$key]['RELATION']);
        unset($arResult['JS_DATA']['ORDER_PROP']['properties'][$key]['VALUE']);
        $arResult['JS_DATA']['ORDER_PROP']['properties'][$key]['TYPE'] = 'ENUM';
        $arResult['JS_DATA']['ORDER_PROP']['properties'][$key]['OPTIONS'] = $departmentOptions;
        $arResult['JS_DATA']['ORDER_PROP']['properties'][$key]['OPTIONS_SORT'] = $departmentOptionsSort;
    }
}



if ($_REQUEST['search'] != '') {
    if (LANGUAGE_ID == 'ru') {
        $filter = ['IBLOCK_ID' => $iblockId, 'ACTIVE' => 'Y', '%UF_NAME_RU' => $_REQUEST['search']];
    } else {
        $filter = ['IBLOCK_ID' => $iblockId, 'ACTIVE' => 'Y', '%NAME' => $_REQUEST['search']];
    }
    $db_list = CIBlockSection::GetList(['NAME' => 'ASC'], $filter, false, ['IBLOCK_ID', 'ID','NAME', 'UF_NAME_RU']);
    while ($ar_result = $db_list->GetNext()) {
        if (LANGUAGE_ID == 'ru') {
            $arResultSerch[] = ['id' => $ar_result['ID'], 'text' => $ar_result['UF_NAME_RU']];
        } else {
            $arResultSerch[] = ['id' => $ar_result['ID'], 'text' => $ar_result['NAME']];
        }
    }

    Bitrix\Main\Type\Collection::sortByColumn($arResultSerch, 'text', ['text' => 'strlen']);

    global $APPLICATION;
    $APPLICATION->RestartBuffer();
    echo json_encode(['results' => $arResultSerch], JSON_UNESCAPED_UNICODE);
    die();
}



if (!empty($_REQUEST['delivery_city'])) {
    $GLOBALS['APPLICATION']->RestartBuffer();

    $rs = CIBlockElement::GetList (['NAME' => 'ASC'], ['IBLOCK_ID' => $iblockId, 'ACTIVE' => 'Y', 'IBLOCK_SECTION_ID' => $_REQUEST['delivery_city']], false, false, ['NAME', 'ID', 'IBLOCK_SECTION_ID', 'PROPERTY_NAME_RU']);
    while ($ar_result = $rs->GetNext()) {
        if (LANGUAGE_ID == 'ru') {
            $departmentOptions[$ar_result['ID']] = html_entity_decode($ar_result['PROPERTY_NAME_RU_VALUE']);
        } else {
            $departmentOptions[$ar_result['ID']] = html_entity_decode($ar_result['NAME']);
        }
    }

    echo json_encode($departmentOptions, JSON_UNESCAPED_UNICODE);
    die();
}

foreach ($arResult['JS_DATA']['GRID']['ROWS'] as $idInBasket => &$arItem) {
    $arItem['data']['PROPS'] = WsLanguage::OrderElementProperties($arItem['data']['PROPS'], $arItem['data']);
}



$component = $this->__component;
$component::scaleImages($arResult['JS_DATA'], $arParams['SERVICES_IMAGES_SCALING']);